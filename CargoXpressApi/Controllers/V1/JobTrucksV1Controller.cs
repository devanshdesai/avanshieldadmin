﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;


namespace CargoXpressApi.Controllers.V1
{
    public class JobTrucksV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobTrucksServices  abstractJobTrucksServices;

        #endregion

        #region Cnstr
        public JobTrucksV1Controller(AbstractJobTrucksServices abstractJobTrucksServices)
        {
            this.abstractJobTrucksServices = abstractJobTrucksServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTrucks_Upsert")]
        public async Task<IHttpActionResult> JobTrucks_Upsert(JobTrucks jobTrucks)
        {
            var quote = abstractJobTrucksServices.JobTrucks_Upsert(jobTrucks);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTrucks_ById")]
        public async Task<IHttpActionResult> JobTrucks_ById(int Id)
        {
            var quote = abstractJobTrucksServices.JobTrucks_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTrucks_ByJobId")]
        public async Task<IHttpActionResult> JobTrucks_ByJobId(PageParam pageParam,int JobId = 0)
        {
            var quote = abstractJobTrucksServices.JobTrucks_ByJobId(pageParam, JobId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTrucks_Delete")]
        public async Task<IHttpActionResult> JobTrucks_Delete(int JobId)
        {
            var quote = abstractJobTrucksServices.JobTrucks_Delete(JobId);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobTrucks_ActInact")]
        public async Task<IHttpActionResult> JobTrucks_IsJobCompleted(int Id)
        {
            var quote = abstractJobTrucksServices.JobTrucks_IsJobCompleted(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
