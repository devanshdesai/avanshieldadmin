﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class JobBillOfLoadingV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices;

        #endregion

        #region Cnstr
        public JobBillOfLoadingV1Controller(AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices)
        {
            this.abstractJobBillOfLoadingServices = abstractJobBillOfLoadingServices;
        }
        #endregion

       


        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBillOfLoading_Upsert")]
        public async Task<IHttpActionResult> JobBillOfLoading_Upsert()
        {
            var httpRequest = HttpContext.Current.Request;
            var jobBillOfLoading = new JobBillOfLoading();
            jobBillOfLoading.JobId = Convert.ToInt32(httpRequest.Params["JobId"]);
            jobBillOfLoading.BillOfLoading = Convert.ToString(httpRequest.Params["BillOfLoading"]);
            
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "BillOfLoading/" + jobBillOfLoading.JobId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                jobBillOfLoading.FileUrl = basePath + fileName;
            }

            var quote = abstractJobBillOfLoadingServices.JobBillOfLoading_Upsert(jobBillOfLoading);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBillOfLoading_All")]
        public async Task<IHttpActionResult> JobBillOfLoading_All(PageParam pageParam, string Search = "", int JobId = 0, string BillOfLoading = "")
        {
            var quote = abstractJobBillOfLoadingServices.JobBillOfLoading_All(pageParam, Search, JobId, BillOfLoading);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBillOfLoading_Delete")]
        public async Task<IHttpActionResult> JobBillOfLoading_Delete(int Id)
        {
            var quote = abstractJobBillOfLoadingServices.JobBillOfLoading_Delete(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }





    }

    }

