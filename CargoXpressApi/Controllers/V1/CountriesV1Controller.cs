﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class CountriesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCountriesService abstractCountriesServices;
        #endregion

        #region Cnstr
        public CountriesV1Controller(AbstractCountriesService abstractCountriesServices)
        {
            this.abstractCountriesServices = abstractCountriesServices;
        }
        #endregion


        //[System.Web.Http.HttpPost]
        //[InheritedRoute("Countries_All")]
        //public async Task<IHttpActionResult> Countries_All(PageParam pageParam,string search="")
        //{
        //    var quote = abstractCountriesServices.Countries_All(pageParam,search);
        //    return this.Content((HttpStatusCode)200, quote);
        //}


        [System.Web.Http.HttpPost]
        [InheritedRoute("Countries_All")]
        public async Task<IHttpActionResult> Countries_All(PageParam pageParam, string search = "")
        {
            var quote = abstractCountriesServices.Countries_All(pageParam, search);
            return this.Content((HttpStatusCode)200,quote);
        }
    }
}
