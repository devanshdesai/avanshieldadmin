﻿
using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class MasterPricingPlansV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterPricingPlansServices abstractMasterPricingPlansServices;
        #endregion

        #region Cnstr
        public MasterPricingPlansV1Controller(AbstractMasterPricingPlansServices abstractMasterPricingPlansServices)
        {
            this.abstractMasterPricingPlansServices = abstractMasterPricingPlansServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPricingPlans_All")]
        public async Task<IHttpActionResult> MasterPricingPlans_All(PageParam pageParam, int UserType)
        {
            var quote = abstractMasterPricingPlansServices.MasterPricingPlans_All(pageParam,"", UserType);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
