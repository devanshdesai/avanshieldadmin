﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;


namespace CargoXpressApi.Controllers.V1
{
    public class AdminUserV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdminUserServices  abstractAdminUserServices;

        #endregion

        #region Cnstr
        public AdminUserV1Controller(AbstractAdminUserServices abstractAdminUserServices)
        {
            this.abstractAdminUserServices = abstractAdminUserServices;
        }
        #endregion

       

        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminUser_ById")]
        public async Task<IHttpActionResult> AdminUser_ById(int Id)
        {
            var quote = abstractAdminUserServices.AdminUser_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminUser_ChangePassword")]
        public async Task<IHttpActionResult> AdminUser_ChangePassword(AdminUser adminUser)
        {
            var quote = abstractAdminUserServices.AdminUser_ChangePassword(adminUser);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminUser_Login")]
        public async Task<IHttpActionResult> AdminUser_Login(AdminUser adminUser)
        {
            var quote = abstractAdminUserServices.AdminUser_LogIn(adminUser);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("AdminUser_Logout")]
        public async Task<IHttpActionResult> AdminUser_LogOut(int Id)
        {
            var quote = abstractAdminUserServices.AdminUser_LogOut(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
