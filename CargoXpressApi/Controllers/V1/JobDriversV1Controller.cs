﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;


namespace CargoXpressApi.Controllers.V1
{
    public class JobDriversV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobDriversServices  abstractJobDriversServices;

        #endregion

        #region Cnstr
        public JobDriversV1Controller(AbstractJobDriversServices abstractJobDriversServices)
        {
            this.abstractJobDriversServices = abstractJobDriversServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDrivers_Upsert")]
        public async Task<IHttpActionResult> JobDrivers_Upsert(JobDrivers jobDrivers)
        {
            var quote = abstractJobDriversServices.JobDrivers_Upsert(jobDrivers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDrivers_ById")]
        public async Task<IHttpActionResult> JobDrivers_ById(int Id)
        {
            var quote = abstractJobDriversServices.JobDrivers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDrivers_ByJobId")]
        public async Task<IHttpActionResult> JobDrivers_ByJobId(PageParam pageParam,int JobId = 0)
        {
            var quote = abstractJobDriversServices.JobDrivers_ByJobId(pageParam, JobId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDrivers_Delete")]
        public async Task<IHttpActionResult> JobDrivers_Delete(int JobId)
        {
            var quote = abstractJobDriversServices.JobDrivers_Delete(JobId);
            return this.Content((HttpStatusCode)200, quote);
        }
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDrivers_ActInact")]
        public async Task<IHttpActionResult> JobDrivers_IsJobCompleted(int Id)
        {
            var quote = abstractJobDriversServices.JobDrivers_IsJobCompleted(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
