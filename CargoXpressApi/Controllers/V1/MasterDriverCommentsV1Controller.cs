﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class MasterDriverCommentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterDriverCommentsServices  abstractMasterDriverCommentsServices;

        #endregion

        #region Cnstr
        public MasterDriverCommentsV1Controller(AbstractMasterDriverCommentsServices abstractMasterDriverCommentsServices)
        {
            this.abstractMasterDriverCommentsServices = abstractMasterDriverCommentsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterDriverComments_All")]
        public async Task<IHttpActionResult> MasterDriverComments_All(PageParam pageParam)
        {
            var quote = abstractMasterDriverCommentsServices.MasterDriverComments_All(pageParam);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
