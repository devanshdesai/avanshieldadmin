﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class TruckDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTruckDocumentsService abstractTruckDocumentsServices;

        #endregion

        #region Cnstr
        public TruckDocumentsV1Controller(AbstractTruckDocumentsService abstractTruckDocumentsServices)
        {
            this.abstractTruckDocumentsServices = abstractTruckDocumentsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckDocuments_Upsert")]
        public async Task<IHttpActionResult> TruckDocuments_Upsert()
        {
            TruckDocuments truckDocuments = new TruckDocuments();
            var httpRequest = HttpContext.Current.Request;
            truckDocuments.TruckId = Convert.ToInt32(httpRequest.Params["TruckId"]);
            truckDocuments.DocumentType = Convert.ToInt32(httpRequest.Params["DocumentType"]);
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "TruckDocuments/" + truckDocuments.TruckId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                truckDocuments.Url = basePath + fileName;
            }

            var quote = abstractTruckDocumentsServices.TruckDocuments_Upsert(truckDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckDocuments_ById")]
        public async Task<IHttpActionResult> TruckDocuments_ById(int Id)
        {
            var quote = abstractTruckDocumentsServices.TruckDocuments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckDocuments_All")]
        public async Task<IHttpActionResult> TruckDocuments_All(PageParam pageParam,int CarrierId = 0)
        {
            var quote = abstractTruckDocumentsServices.TruckDocuments_All(pageParam,CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
