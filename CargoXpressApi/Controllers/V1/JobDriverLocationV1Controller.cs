﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.Data.SqlClient;
using System.Data;
using System.Device.Location;

namespace CargoXpressApi.Controllers.V1
{
    public class JobDriverLocationV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobDriverLocationServices  abstractJobDriverLocationServices;
        private readonly AbstractJobServices abstractJobServices;
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractNotificationServices abstractNotificationServices;
        #endregion

        #region Cnstr
        public JobDriverLocationV1Controller(AbstractJobDriverLocationServices abstractJobDriverLocationServices, AbstractJobServices abstractJobServices, 
            AbstractShippersServices abstractShippersServices, AbstractCarriersServices abstractCarriersServices, AbstractNotificationServices abstractNotificationServices)
        {
            this.abstractJobDriverLocationServices = abstractJobDriverLocationServices;
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractJobServices = abstractJobServices;
            this.abstractNotificationServices = abstractNotificationServices;
        }
        #endregion
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDriverLocationUpsert")]
        public async Task<IHttpActionResult> JobDriverLocation_Upsert(List<JobDriverLocation> jobdriverlocation)
        {
            int JobId = 0; 
            foreach (var item in jobdriverlocation)
            {
                AbstractJobDriverLocation abstractJobDriverLocation = new JobDriverLocation();
                abstractJobDriverLocation.JobId = item.JobId;
                abstractJobDriverLocation.DriverId = item.DriverId;
                abstractJobDriverLocation.Latitude = item.Latitude;
                abstractJobDriverLocation.Longitude = item.Longitude;
                abstractJobDriverLocation.CapturedDate = item.CapturedDate;
                var quote = abstractJobDriverLocationServices.JobDriverLocation_Upsert(abstractJobDriverLocation);
                JobId = item.JobId;
            }

            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql = "select top 1 Id from RouteMaster where JobId=" + JobId;
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            sda.Fill(dt);

            if(dt.Rows.Count > 0)
            {
                DataTable dtRouteChild = new DataTable();
                var_sql = "select Id,Lat,Long,LocationName from RouteChild where RouteMasterId=" + Convert.ToString(dt.Rows[0]["Id"])+" and IsPassed=0";
                sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dtRouteChild);
                for(int i=0; i < dtRouteChild.Rows.Count; i++)
                {
                    var sCoord = new GeoCoordinate(ConvertTo.Double(jobdriverlocation[jobdriverlocation.Count -1].Latitude), ConvertTo.Double(jobdriverlocation[jobdriverlocation.Count - 1].Longitude));
                    var eCoord = new GeoCoordinate(Convert.ToDouble(dtRouteChild.Rows[i]["Lat"]), Convert.ToDouble(dtRouteChild.Rows[i]["Long"]));
                    double distance = sCoord.GetDistanceTo(eCoord);
                    if(distance <= 3000)
                    {
                        var_sql = "update RouteChild set IsPassed=1 where Id=" + ConvertTo.Integer(dtRouteChild.Rows[i]["Id"]);
                        con.Open();
                        SqlCommand cmd = new SqlCommand(var_sql, con);
                        cmd.ExecuteNonQuery();
                        con.Close();

                        try
                        {
                            var_sql = "select LocationName from RouteChild where Id=" + ConvertTo.Integer(dtRouteChild.Rows[i]["Id"]);
                            DataTable dtRc = new DataTable();
                            SqlDataAdapter sdaRc = new SqlDataAdapter(var_sql, con);
                            sdaRc.Fill(dtRc);

                            if (dtRc.Rows.Count > 0)
                            {
                                var jobResult = abstractJobServices.Job_ById(JobId);
                                if (jobResult.Item != null)
                                {
                                    var shipperResult = abstractShippersServices.Shippers_ById(jobResult.Item.ShipperId);
                                    var carrierResult = abstractCarriersServices.Carriers_ById(jobResult.Item.CarrierId);
                                    string body = "The driver has passed the checkpoint : " + Convert.ToString(dtRc.Rows[0]["LocationName"]);
                                    string title = "CargoXpress - Checkpoint Passed";
                                    EmailHelper.SendPushNotification(shipperResult.Item.DeviceToken, body, title);
                                    EmailHelper.SendPushNotification(carrierResult.Item.DeviceToken, body, title);

                                    AbstractNotification abstractNotification = new Notification();
                                    abstractNotification.UserId = jobResult.Item.ShipperId;
                                    abstractNotification.UserType = 3;
                                    abstractNotification.Text = body;
                                    abstractNotificationServices.Notification_Upsert(abstractNotification);

                                    abstractNotification = new Notification();
                                    abstractNotification.UserId = jobResult.Item.CarrierId;
                                    abstractNotification.UserType = 2;
                                    abstractNotification.Text = body;
                                    abstractNotificationServices.Notification_Upsert(abstractNotification);
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            return this.Content((HttpStatusCode)200, "Captured Successfully");
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDriverLocation_ById")]
        public async Task<IHttpActionResult> JobDriverLocations_ById(int Id)
        {
            var quote = abstractJobDriverLocationServices.JobDriverLocations_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDriverLocation_ByJobId")]
        public async Task<IHttpActionResult> JobDriverLocation_ByJobId(PageParam pageParam, int JobId, int DriverId)
        {
            var quote = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId , DriverId);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
