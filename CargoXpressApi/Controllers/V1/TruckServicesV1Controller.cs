﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class TruckServicesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTruckServicesServices  abstractTruckServicesServices;

        #endregion

        #region Cnstr
        public TruckServicesV1Controller(AbstractTruckServicesServices abstractTruckServicesServices)
        {
            this.abstractTruckServicesServices = abstractTruckServicesServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckServices_Upsert")]
        public async Task<IHttpActionResult> TruckServices_Upsert(TruckServices carriers)
        {
            var quote = abstractTruckServicesServices.TruckServices_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckServices_ById")]
        public async Task<IHttpActionResult> TruckServices_ById(int Id)
        {
            var quote = abstractTruckServicesServices.TruckServices_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckServices_All")]
        public async Task<IHttpActionResult> TruckServices_All(PageParam pageParam,string Search="", int ParentId = 0)
        {
            var quote = abstractTruckServicesServices.TruckServices_All(pageParam,Search,ParentId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckServices_ByParentId")]
        public async Task<IHttpActionResult> TruckServices_ByParentId(PageParam pageParam, string search = "")
        {
            var quote = abstractTruckServicesServices.TruckServices_ByParentId(pageParam, search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
