﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class JobDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobDocumentsServices  abstractJobDocumentsServices;

        #endregion

        #region Cnstr
        public JobDocumentsV1Controller(AbstractJobDocumentsServices abstractJobDocumentsServices)
        {
            this.abstractJobDocumentsServices = abstractJobDocumentsServices;
        }
        #endregion
        //Update by Krinal
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDocuments_Upsert")]
        public async Task<IHttpActionResult> JobDocuments_Upsert()
        {
            JobDocuments jobDocuments = new JobDocuments();
            var httpRequest = HttpContext.Current.Request;
            jobDocuments.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            jobDocuments.JobId = Convert.ToInt32(httpRequest.Params["JobId"]);
            jobDocuments.UserType = Convert.ToInt32(httpRequest.Params["UserType"]);
            jobDocuments.DocumentTypeId = Convert.ToInt32(httpRequest.Params["DocumentTypeId"]);
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                if (jobDocuments.UserType == 2)
                {
                    string basePath = "JobDocuments/Carriers/" + jobDocuments.JobId + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                    if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                    }
                    myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                    jobDocuments.Url = basePath + fileName;
                }
                else if (jobDocuments.UserType == 3)
                {
                    string basePath = "JobDocuments/Shipper/" + jobDocuments.JobId + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                    if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                    }
                    myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                    jobDocuments.Url = basePath + fileName;
                }
                else if (jobDocuments.UserType == 4)
                {
                    string basePath = "JobDocuments/Driver/" + jobDocuments.JobId + "/";
                    string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                    if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                    }
                    myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                    jobDocuments.Url = basePath + fileName;
                }
                
            }

            var quote = abstractJobDocumentsServices.JobDocuments_Upsert(jobDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDocuments_ById")]
        public async Task<IHttpActionResult> JobDocuments_ById(int Id)
        {
            var quote = abstractJobDocumentsServices.JobDocuments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDocuments_ByJobId")]
        public async Task<IHttpActionResult> JobDocuments_ByJobId(PageParam pageParam, int JobId,int UserType)
        {
            var quote = abstractJobDocumentsServices.JobDocuments_ByJobId(pageParam, JobId, UserType);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
