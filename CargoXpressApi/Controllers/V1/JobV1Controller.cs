﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.Data.SqlClient;
using System.Data;

namespace CargoXpressApi.Controllers.V1
{
    public class JobV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobServices abstractJobServices;
        private readonly AbstractCarrierPreferedDestinationsServices abstractCarrierPreferedDestinationsServices;
        private readonly AbstractCarrierTruckServicesServices abstractCarrierTruckServices;
        private readonly AbstractTrucksService abstractTrucksService;
        private readonly AbstractCountriesService abstractCountriesService;
        private readonly AbstractJobActivityServices abstractJobActivityServices;
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractNotificationServices abstractNotificationServices;
        #endregion

        #region Cnstr
        public JobV1Controller(AbstractJobServices abstractJobServices, AbstractCarrierPreferedDestinationsServices abstractCarrierPreferedDestinationsServices
            , AbstractCarrierTruckServicesServices abstractCarrierTruckServices, AbstractTrucksService abstractTrucksService,
            AbstractCountriesService abstractCountriesService, AbstractJobActivityServices abstractJobActivityServices,
            AbstractShippersServices abstractShippersServices, AbstractCarriersServices abstractCarriersServices,
            AbstractNotificationServices abstractNotificationServices)
        {
            this.abstractJobServices = abstractJobServices;
            this.abstractCarrierTruckServices = abstractCarrierTruckServices;
            this.abstractCarrierPreferedDestinationsServices = abstractCarrierPreferedDestinationsServices;
            this.abstractTrucksService = abstractTrucksService;
            this.abstractCountriesService = abstractCountriesService;
            this.abstractJobActivityServices = abstractJobActivityServices;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractNotificationServices = abstractNotificationServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_Upsert")]
        public async Task<IHttpActionResult> Job_Upsert(Job job)
        {

            try
            {
                string[] pickCountry = job.PickUpAddress.Split(',');
                string[] dropCountry = job.DropOffAddress.Split(',');

                SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                string var_sql = "select Id from Countries where LOWER(Name) like '%" + pickCountry[pickCountry.Length - 1].Trim().ToLower() + "%'";

                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    job.PickUpShipperAddressId = Convert.ToInt32(dt.Rows[0]["Id"]);
                }

                var_sql = "select Id from Countries where LOWER(Name) like '%" + dropCountry[dropCountry.Length - 1].Trim().ToLower() + "%'";

                dt = new DataTable();
                sda = new SqlDataAdapter(var_sql, con);
                sda.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    job.DropOffShipperAddressId = Convert.ToInt32(dt.Rows[0]["Id"]);
                }
            }
            catch(Exception ex)
            {

            }

            var quote = abstractJobServices.Job_Upsert(job);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_ById")]
        public async Task<IHttpActionResult> Job_ById(int Id)
        {
            var quote = abstractJobServices.Job_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_All")]
        public async Task<IHttpActionResult> Job_All(PageParam pageParam, string JobStatus = "", string Search = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null)
        {
            int tempCarrierId = CarrierId;
            if (JobStatus == "NewJob" && CarrierId > 0)
            {
                CarrierId = 0;
            }

            var quote = abstractJobServices.Job_All(pageParam, Search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, JobStatus);

            if (JobStatus == "NewJob" && tempCarrierId > 0)
            {
                PageParam pageParam2 = new PageParam();
                var trucks = abstractTrucksService.Trucks_All(pageParam2, "", "", "", 0, 0, tempCarrierId);
                var preferredTruckService = abstractCarrierTruckServices.CarrierTruckServices_All(pageParam2, tempCarrierId, null);
                var preferredLocations = abstractCarrierPreferedDestinationsServices.CarrierPreferedDestinations_All(pageParam2, tempCarrierId);
                if (trucks.Values.Count > 0)
                {
                    //quote.Values = quote.Values.Where(x => (trucks.Values.Any(y => y.TruckTypeId == x.TruckTypeId))
                    //&& (preferredTruckService.Values.Any(z => z.TruckServiceId == x.TruckServiceId))
                    //&& (preferredLocations.Values.Any(a => a.CountryId == x.PickUpShipperAddressId))
                    //&& (preferredLocations.Values.Any(a => a.CountryId == x.DropOffShipperAddressId))).ToList();
                    quote.Values = quote.Values.Where(x => (trucks.Values.Any(y => y.TruckTypeId == x.TruckTypeId))).ToList();
                }
                if(preferredTruckService.Values.Count > 0)
                {
                    quote.Values = quote.Values.Where(x => (preferredTruckService.Values.Any(y => y.TruckServiceId == x.TruckServiceId))).ToList();
                }
                if(preferredLocations.Values.Count > 0)
                {
                    quote.Values = quote.Values.Where(x => preferredLocations.Values.Any(a => a.CountryId == x.PickUpShipperAddressId)
                    && (preferredLocations.Values.Any(a => a.CountryId == x.DropOffShipperAddressId))).ToList();
                }
            }

            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_AssignDriver")]
        public async Task<IHttpActionResult> Job_AssignDriver(Job job)
        {
            var quote = abstractJobServices.Job_AssignDriver(job);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_AssignTruck")]
        public async Task<IHttpActionResult> Job_AssignTruck(Job job)
        {
            var quote = abstractJobServices.Job_AssignTruck(job);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_CarrierJobEndDate")]
        public async Task<IHttpActionResult> Job_CarrierJobEndDate(Job job)
        {
            var quote = abstractJobServices.Job_CarrierJobEndDate(job);
            if (quote.Item != null)
            {
                string body = "UPDATE : Carrier has ended the job " + quote.Item.JobNo;
                string title = "CargoXpress - Job Status Changed";
                var shipper = abstractShippersServices.Shippers_ById(quote.Item.ShipperId);
                EmailHelper.SendPushNotification(shipper.Item.DeviceToken, body, title);
                var carrier = abstractCarriersServices.Carriers_ById(quote.Item.CarrierId);
                EmailHelper.SendPushNotification(carrier.Item.DeviceToken, body, title);

                AbstractNotification abstractNotification = new Notification();
                abstractNotification.UserId = quote.Item.ShipperId;
                abstractNotification.UserType = 3;
                abstractNotification.Text = body;
                abstractNotificationServices.Notification_Upsert(abstractNotification);

                abstractNotification = new Notification();
                abstractNotification.UserId = quote.Item.CarrierId;
                abstractNotification.UserType = 2;
                abstractNotification.Text = body;
                abstractNotificationServices.Notification_Upsert(abstractNotification);
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_CarrierJobStartDate")]
        public async Task<IHttpActionResult> Job_CarrierJobStartDate(Job job)
        {
            var quote = abstractJobServices.Job_CarrierJobStartDate(job);

            if(quote.Item != null)
            {
                string body = "UPDATE : Carrier has started the job "+ quote.Item.JobNo;
                string title = "CargoXpress - Job Status Changed";
                var shipper = abstractShippersServices.Shippers_ById(quote.Item.ShipperId);
                EmailHelper.SendPushNotification(shipper.Item.DeviceToken, body, title);
                var carrier = abstractCarriersServices.Carriers_ById(quote.Item.CarrierId);
                EmailHelper.SendPushNotification(carrier.Item.DeviceToken, body, title);

                AbstractNotification abstractNotification = new Notification();
                abstractNotification.UserId = quote.Item.ShipperId;
                abstractNotification.UserType = 3;
                abstractNotification.Text = body;
                abstractNotificationServices.Notification_Upsert(abstractNotification);

                abstractNotification = new Notification();
                abstractNotification.UserId = quote.Item.CarrierId;
                abstractNotification.UserType = 2;
                abstractNotification.Text = body;
                abstractNotificationServices.Notification_Upsert(abstractNotification);
            }
            
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_JobStatus")]
        public async Task<IHttpActionResult> Job_JobStatus(Job job)
        {
            var quote = abstractJobServices.Job_JobStatus(job);
            if (quote.Item != null)
            {
                string body = "UPDATE : Job's ("+quote.Item.JobNo+") status has changed to " + quote.Item.JobStatus;
                string title = "CargoXpress - Job Status Changed";
                var shipper = abstractShippersServices.Shippers_ById(quote.Item.ShipperId);
                if(shipper.Item != null)
                {
                    EmailHelper.SendPushNotification(shipper.Item.DeviceToken, body, title);
                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserId = quote.Item.ShipperId;
                    abstractNotification.UserType = 3;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);

                }

                var carrier = abstractCarriersServices.Carriers_ById(quote.Item.CarrierId);
                if(carrier.Item != null)
                {
                    EmailHelper.SendPushNotification(carrier.Item.DeviceToken, body, title);
                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserId = quote.Item.CarrierId;
                    abstractNotification.UserType = 2;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);
                }
                
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_AcceptBid")]
        public async Task<IHttpActionResult> Job_AcceptBid(Job job)
        {
            var quote = abstractJobServices.Job_AcceptBid(job);
            if (quote.Item != null)
            {
                var jobResult = abstractJobServices.Job_ById(job.Id);
                if (jobResult.Item != null)
                {
                    var shipperResult = abstractCarriersServices.Carriers_ById(jobResult.Item.CarrierId);
                    string body = "UPDATE : Your bid has been accepted for the Job (" + jobResult.Item.JobNo + ")";
                    string title = "CargoXpress - Bid Accepted";
                    EmailHelper.SendPushNotification(shipperResult.Item.DeviceToken, body, title);

                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserId = jobResult.Item.CarrierId;
                    abstractNotification.UserType = 2;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);
                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Job_Activity")]
        public async Task<IHttpActionResult> Job_Activity(int JobId)
        {
            PageParam pageParam = new PageParam();
            var quote = abstractJobActivityServices.JobActivity_ByJobId(pageParam, JobId, 0);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
