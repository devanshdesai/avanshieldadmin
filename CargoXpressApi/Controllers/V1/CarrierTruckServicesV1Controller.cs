﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class CarrierTruckServicesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCarrierTruckServicesServices  abstractCarrierTruckServicesServices;

        #endregion

        #region Cnstr
        public CarrierTruckServicesV1Controller(AbstractCarrierTruckServicesServices abstractCarrierTruckServicesServices)
        {
            this.abstractCarrierTruckServicesServices = abstractCarrierTruckServicesServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierTruckServices_Upsert")]
        public async Task<IHttpActionResult> CarrierTruckServices_Upsert(CarrierTruckServices carriers)
        {
            var quote = abstractCarrierTruckServicesServices.CarrierTruckServices_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierTruckServices_ById")]
        public async Task<IHttpActionResult> CarrierTruckServices_ById(int Id)
        {
            var quote = abstractCarrierTruckServicesServices.CarrierTruckServices_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierTruckServices_All")]
        public async Task<IHttpActionResult> CarrierTruckServices_All(PageParam pageParam,int CarrierId = 0)
        {
            var quote = abstractCarrierTruckServicesServices.CarrierTruckServices_All(pageParam,CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierTruckServices_Delete")]
        public async Task<IHttpActionResult> CarrierTruckServices_Delete(int CarrierId)
        {
            var quote = abstractCarrierTruckServicesServices.CarrierTruckServices_Delete(CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
