﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;
using System.Data;
using System.Text;
using System.Device.Location;

namespace CargoXpressApi.Controllers.V1
{
    public class RouteChildV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractRouteChildServices abstractRouteChildServices;
        private readonly AbstractRouteMasterServices abstractRouteMasterServices;
        #endregion

        #region Cnstr
        public RouteChildV1Controller(AbstractRouteChildServices abstractRouteChildServices, AbstractRouteMasterServices abstractRouteMasterServices)
        {
            this.abstractRouteChildServices = abstractRouteChildServices;
            this.abstractRouteMasterServices = abstractRouteMasterServices;
        }
        #endregion

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteChild_Upsert")]
        public async Task<IHttpActionResult> RouteChild_Upsert(RouteChild routeChild)
        {
            var routeMasterDetails = abstractRouteMasterServices.RouteMaster_ById(routeChild.RouteMasterId);
            DataTable dt = GetLatLong(routeChild.LocationName);
            if (dt.Rows.Count > 0)
            {
                routeChild.Lat = Convert.ToString(dt.Rows[0]["Latitude"]);
                routeChild.Long = Convert.ToString(dt.Rows[0]["Longitude"]);
                double distance = 0;

                if (routeMasterDetails.Item != null)
                {
                    var sCoord = new GeoCoordinate(Convert.ToDouble(routeMasterDetails.Item.PickUpLat), Convert.ToDouble(routeMasterDetails.Item.PickUpLong));
                    var eCoord = new GeoCoordinate(Convert.ToDouble(dt.Rows[0]["Latitude"]), Convert.ToDouble(dt.Rows[0]["Longitude"]));
                    distance = Convert.ToDouble((sCoord.GetDistanceTo(eCoord) / 1000).ToString("00.00"));
                }

                routeChild.Distance = Convert.ToString(distance);
            }
            else
            {
                routeChild.Distance = "0";
            }
            var quote = abstractRouteChildServices.RouteChild_Upsert(routeChild);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteChild_ById")]
        public async Task<IHttpActionResult> RouteChild_ById(int Id)
        {
            var quote = abstractRouteChildServices.RouteChild_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteChild_ByRouteMasterId")]
        public async Task<IHttpActionResult> RouteChild_ByRouteMasterId(PageParam pageParam,string search="", int RouteMasterId = 0)
        {
            var quote = abstractRouteChildServices.RouteChild_ByRouteMasterId(pageParam,search, RouteMasterId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("RouteChild_Delete")]
        public async Task<IHttpActionResult> RouteChild_Delete(int Id)
        {
            var quote = abstractRouteChildServices.RouteChild_Delete(Id);
            return this.Content((HttpStatusCode)200, quote);
        }



        private DataTable GetLatLong(string address)
        {
            //string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyCVVyH8o7D-2V_fdEiK2wtXUvxC8IKcVWk";
            string url = "https://maps.google.com/maps/api/geocode/xml?address=" + address + "&key=AIzaSyAJyahQ-oUd7nU22mj6EUl-aRoMeRLevfA";
            WebRequest request = WebRequest.Create(url);

            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                DataTable dtCoordinates = new DataTable();
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    DataSet dsResult = new DataSet();
                    dsResult.ReadXml(reader);
                    dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
                    new DataColumn("Address", typeof(string)),
                    new DataColumn("Latitude",typeof(string)),
                    new DataColumn("Longitude",typeof(string)) });
                    foreach (DataRow row in dsResult.Tables["result"].Rows)
                    {
                        string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                        DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                        dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
                    }
                }
                return dtCoordinates;
            }
        }


    }
}
