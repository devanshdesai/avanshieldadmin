﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;


namespace CargoXpressApi.Controllers.V1
{
    public class MasterPlansV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterPlansServices  abstractMasterPlansServices;

        #endregion

        #region Cnstr
        public MasterPlansV1Controller(AbstractMasterPlansServices abstractMasterPlansServices)
        {
            this.abstractMasterPlansServices = abstractMasterPlansServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPlans_Upsert")]
        public async Task<IHttpActionResult> MasterPlans_Upsert(MasterPlans masterPlans)
        {
            var quote = abstractMasterPlansServices.MasterPlans_Upsert(masterPlans);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPlans_ById")]
        public async Task<IHttpActionResult> MasterPlans_ById(int Id)
        {
            var quote = abstractMasterPlansServices.MasterPlans_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterPlans_All")]
        public async Task<IHttpActionResult> MasterPlans_All(PageParam pageParam, string Search)
        {
            var quote = abstractMasterPlansServices.MasterPlans_All(pageParam, Search);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
