﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class CarrierV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCarriersServices  abstractCarriersServices;
        private readonly AbstractCarrierPricingPlanServices abstractCarrierPricingPlanServices;
        private readonly AbstractMasterPricingPlansServices abstractMasterPricingPlansServices;

        #endregion

        #region Cnstr
        public CarrierV1Controller(AbstractCarriersServices abstractCarriersServices, AbstractCarrierPricingPlanServices abstractCarrierPricingPlanServices,
            AbstractMasterPricingPlansServices abstractMasterPricingPlansServices)
        {
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractCarrierPricingPlanServices = abstractCarrierPricingPlanServices;
            this.abstractMasterPricingPlansServices = abstractMasterPricingPlansServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_Login")]
        public async Task<IHttpActionResult> Carriers_Login(Carriers carriers)
        {
            var quote = abstractCarriersServices.Carriers_Login(carriers);
            if (carriers.LoginType == false && quote.Code == 200 && quote.Item != null)
            {
                try
                {
                    carriers.Otp = CommonHelper.GenerateRandomNo();
                    //EmailHelper.SendMsgViaTwilio(cleaners.Otp, quote.Item.Mobile);
                }
                catch (Exception ex)
                {

                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_Logout")]
        public async Task<IHttpActionResult> Carriers_Logout(int Id)
        {
            var quote = abstractCarriersServices.Carriers_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_Upsert")]
        public async Task<IHttpActionResult> Carriers_Upsert(Carriers carriers)
        {
            var quote = abstractCarriersServices.Carriers_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_ProfileUpdate")]
        public async Task<IHttpActionResult> Carriers_ProfileUpdate()
        {
            Carriers carriers = new Carriers();
            var httpRequest = HttpContext.Current.Request;
            carriers.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CarrierProfile/" + carriers.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                carriers.FileURL = basePath + fileName;
            }

            var quote = abstractCarriersServices.Carriers_ProfileUpdate(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_ById")]
        public async Task<IHttpActionResult> Carrier_ById(int Id)
        {
            var quote = abstractCarriersServices.Carriers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_All")]
        public async Task<IHttpActionResult> Carriers_All(PageParam pageParam,string search="", string Name="", string Email="", int CountryId =0, string PhoneNumber ="", bool? IsActive = null)
        {
            var quote = abstractCarriersServices.Carriers_All(pageParam,search, Name, Email, CountryId, PhoneNumber,IsActive);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_ActInact")]
        public async Task<IHttpActionResult> Carriers_ActInact(int Id)
        {
            var quote = abstractCarriersServices.Carriers_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_Approve")]
        public async Task<IHttpActionResult> Carriers_Approve(int Id)
        {
            var quote = abstractCarriersServices.Carriers_Approve(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_EmailVerify")]
        public async Task<IHttpActionResult> Carriers_EmailVerify(int Id)
        {
            var quote = abstractCarriersServices.Carriers_EmailVerify(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_MobileVerify")]
        public async Task<IHttpActionResult> Carriers_MobileVerify(int Id)
        {
            var quote = abstractCarriersServices.Carriers_MobileVerify(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Carriers_ChangePassword")]
        public async Task<IHttpActionResult> Carriers_ChangePassword(Carriers carriers)
        {
            var quote = abstractCarriersServices.Carriers_ChangePassword(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Carrier_GetOtp")]
        public async Task<IHttpActionResult> Carrier_GetOtp(string mobileNo)
        {
            try
            {
                string Otp = CommonHelper.GenerateRandomNo();
                //EmailHelper.SendMsgViaTwilio(Otp, mobileNo);
                return this.Content((HttpStatusCode)200, Otp);
            }
            catch (Exception ex)
            {
                return this.Content((HttpStatusCode)500, "An error occured in the twilio");
            }

        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierPricingPlan_Upsert")]
        public async Task<IHttpActionResult> CarrierPricingPlan_Upsert(CarrierPricingPlan abstractCarrierPricingPlan)
        {
            var result = abstractMasterPricingPlansServices.MasterPricingPlans_ById(abstractCarrierPricingPlan.MasterPricingPlanId);
            //TimeLimit
            abstractCarrierPricingPlan.StartDate = DateTime.Now.AddDays(1).ToString("dd-MMM-yyyy");
            abstractCarrierPricingPlan.EndDate = DateTime.Now.AddMonths(ConvertTo.Integer(result.Item.TimeLimit)).ToString("dd-MMM-yyyy");
            var quote = abstractCarrierPricingPlanServices.CarrierPricingPlan_Upsert(abstractCarrierPricingPlan);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierPricingPlan_ByCarrierId")]
        public async Task<IHttpActionResult> CarrierPricingPlan_ByCarrierId(PageParam pageParam,int CarrierId,int UserType)
        {
            var quote = abstractCarrierPricingPlanServices.CarrierPricingPlan_ByCarrierId(pageParam,"", CarrierId, UserType);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
