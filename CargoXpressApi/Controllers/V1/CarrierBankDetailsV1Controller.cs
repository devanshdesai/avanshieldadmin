﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class CarrierBankDetailsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCarrierBankDetailsServices  abstractCarrierBankDetailsServices;

        #endregion

        #region Cnstr
        public CarrierBankDetailsV1Controller(AbstractCarrierBankDetailsServices abstractCarrierBankDetailsServices)
        {
            this.abstractCarrierBankDetailsServices = abstractCarrierBankDetailsServices;
        }
        #endregion

        

        


        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierBankDetails_Upsert")]
        public async Task<IHttpActionResult> CarrierBankDetails_Upsert(CarrierBankDetails carrierBankDetails)
        {
            var quote = abstractCarrierBankDetailsServices.CarrierBankDetails_Upsert(carrierBankDetails);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierBankDetails_ById")]
        public async Task<IHttpActionResult> CarrierBankDetails_ById(int Id)
        {
            var quote = abstractCarrierBankDetailsServices.CarrierBankDetails_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierBankDetails_ByCarrierId")]
        public async Task<IHttpActionResult> CarrierBankDetails_ByCarrierId(PageParam pageParam,string search="", int CarrierId=0)
        {
            var quote = abstractCarrierBankDetailsServices.CarrierBankDetails_ByCarrierId(pageParam,search, CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierBankDetails_Delete")]
        public async Task<IHttpActionResult> CarrierBankDetails_Delete(CarrierBankDetails carrierBankDetails)
        {
            var quote = abstractCarrierBankDetailsServices.CarrierBankDetails_Delete(carrierBankDetails);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierBankDetails_IsDefault")]
        public async Task<IHttpActionResult> CarrierBankDetails_IsDefault(int Id)
        {
            var quote = abstractCarrierBankDetailsServices.CarrierBankDetails_IsDefault(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

    }
}
