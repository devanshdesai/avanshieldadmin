﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class JobDriverCommentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobDriverCommentsServices  abstractJobDriverCommentsServices;
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractJobServices abstractJobServices;
        private readonly AbstractDriversService abstractDriversService;
        private readonly AbstractNotificationServices abstractNotificationServices;
        #endregion

        #region Cnstr
        public JobDriverCommentsV1Controller(AbstractJobDriverCommentsServices abstractJobDriverCommentsServices, AbstractShippersServices abstractShippersServices,
            AbstractCarriersServices abstractCarriersServices, AbstractJobServices abstractJobServices, AbstractDriversService abstractDriversService, AbstractNotificationServices abstractNotificationServices)
        {
            this.abstractJobDriverCommentsServices = abstractJobDriverCommentsServices;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractJobServices = abstractJobServices;
            this.abstractDriversService = abstractDriversService;
            this.abstractNotificationServices = abstractNotificationServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDriverComments_Upsert")]
        public async Task<IHttpActionResult> JobDriverComments_Upsert(JobDriverComments jobdrivercomments)
        {
            var quote = abstractJobDriverCommentsServices.JobDriverComments_Upsert(jobdrivercomments);
            if (quote.Item != null)
            {
                var JobResult = abstractJobServices.Job_ById(quote.Item.JobId);
                var carrier = abstractCarriersServices.Carriers_ById(JobResult.Item.CarrierId);
                var shipper = abstractShippersServices.Shippers_ById(JobResult.Item.ShipperId);
                var sriver = abstractDriversService.Drivers_ById(JobResult.Item.DriverId);
                if(quote.Item.UserType == 2)
                {
                    string body = "New Message Recieved From Carrier";
                    string title = "CargoXpress - New Message";
                    EmailHelper.SendPushNotification(sriver.Item.DeviceToken, body, title);
                    EmailHelper.SendPushNotification(shipper.Item.DeviceToken, body, title);

                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserId = JobResult.Item.ShipperId;
                    abstractNotification.UserType = 3;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);

                    abstractNotification = new Notification();
                    abstractNotification.UserId = JobResult.Item.DriverId;
                    abstractNotification.UserType = 4;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);
                }
                else if (quote.Item.UserType == 4)
                {
                    string body = "New Message Recieved From Driver";
                    string title = "CargoXpress - New Message";
                    EmailHelper.SendPushNotification(carrier.Item.DeviceToken, body, title);
                    EmailHelper.SendPushNotification(shipper.Item.DeviceToken, body, title);

                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserId = JobResult.Item.ShipperId;
                    abstractNotification.UserType = 3;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);

                    abstractNotification = new Notification();
                    abstractNotification.UserId = JobResult.Item.CarrierId;
                    abstractNotification.UserType = 2;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);

                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDriverComments_ById")]
        public async Task<IHttpActionResult> JobDriverComments_ById(int Id)
        {
            var quote = abstractJobDriverCommentsServices.JobDriverComments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobDriverComments_All")]
        public async Task<IHttpActionResult> JobDriverComments_All(PageParam pageParam, int JobId, int DriverId)
        {
            var quote = abstractJobDriverCommentsServices.JobDriverComments_All(pageParam, JobId, DriverId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
