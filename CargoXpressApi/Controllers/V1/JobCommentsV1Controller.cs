﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class JobCommentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobCommentsServices  abstractJobCommentsServices;

        #endregion

        #region Cnstr
        public JobCommentsV1Controller(AbstractJobCommentsServices abstractJobCommentsServices)
        {
            this.abstractJobCommentsServices = abstractJobCommentsServices;
        }
        #endregion
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobCommentsUpsert")]
        public async Task<IHttpActionResult> JobComments_Upsert(JobComments jobcomments)
        {
            var quote = abstractJobCommentsServices.JobComments_Upsert(jobcomments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobComments_ById")]
        public async Task<IHttpActionResult> JobComments_ById(int Id)
        {
            var quote = abstractJobCommentsServices.JobComments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobComments_ByJobId")]
        public async Task<IHttpActionResult> JobComments_ByJobId(PageParam pageParam, int JobId)
        {
            var quote = abstractJobCommentsServices.JobComments_ByJobId(pageParam, JobId);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
