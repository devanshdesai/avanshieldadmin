﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class SupportReplyV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractSupportServices abstractSupportServices;
        private readonly AbstractSupportReplyServices abstractSupportReplyService;
        #endregion

        #region Cnstr
        public SupportReplyV1Controller(AbstractSupportServices abstractSupportServices, AbstractSupportReplyServices abstractSupportReplyService)
        {
            this.abstractSupportServices = abstractSupportServices;
            this.abstractSupportReplyService = abstractSupportReplyService;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("SupportReply_Upsert")]
        public async Task<IHttpActionResult> SupportReply_Upsert(SupportReply supportReply)
        {
            var quote = abstractSupportReplyService.SupportReply_Upsert(supportReply);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("SupportReply_BySupportId")]
        public async Task<IHttpActionResult> SupportReply_BySupportId(PageParam pageParam, int SupportId)
        {
            var quote = abstractSupportReplyService.SupportReply_BySupportId(pageParam,SupportId);
            return this.Content((HttpStatusCode)200, quote);
        }
    }
}
