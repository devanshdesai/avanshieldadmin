﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class StatesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractStatesServices  abstractStatesServices;

        #endregion

        #region Cnstr
        public StatesV1Controller(AbstractStatesServices abstractStatesServices)
        {
            this.abstractStatesServices = abstractStatesServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("States_ByCountryId")]
        public async Task<IHttpActionResult> States_ByCountryId(PageParam pageParam, String Search = "", int CountryId = 0)
        {
            var quote = abstractStatesServices.States_ByCountryId(pageParam, Search, CountryId);
            return this.Content((HttpStatusCode)200, quote);
        }


        

    }
}
