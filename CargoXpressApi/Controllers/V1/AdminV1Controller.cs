﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class AdminV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractAdminServices  abstractAdminServices;

        #endregion

        #region Cnstr
        public AdminV1Controller(AbstractAdminServices abstractAdminServices)
        {
            this.abstractAdminServices = abstractAdminServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Login")]
        public async Task<IHttpActionResult> Admin_Login(Admin admin)
        {
            var quote = abstractAdminServices.Admin_Login(admin);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_Logout")]
        public async Task<IHttpActionResult> Admin_Logout(int Id)
        {
            var quote = abstractAdminServices.Admin_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ById")]
        public async Task<IHttpActionResult> Admin_ById(int Id)
        {
            var quote = abstractAdminServices.Admin_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

        [System.Web.Http.HttpPost]
        [InheritedRoute("Admin_ChangePassword")]
        public async Task<IHttpActionResult> Admin_ChangePassword(Admin admin)
        {
            var quote = abstractAdminServices.Admin_ChangePassword(admin);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("SampleDeviceToken")]
        public async Task<IHttpActionResult> SampleDeviceToken(string deviceToken, string body)
        {
            EmailHelper.SendPushNotification(deviceToken, body, "test");
            return this.Content((HttpStatusCode)200, "");
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("DriverSampleDeviceToken")]
        public async Task<IHttpActionResult> DriverSampleDeviceToken(string deviceToken, string body)
        {
            EmailHelper.DriverSendPushNotification(deviceToken, body, "test");
            return this.Content((HttpStatusCode)200, "");
        }
    }
}
