﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class TruckTypesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractTruckTypesServices  abstractTruckTypesServices;

        #endregion

        #region Cnstr
        public TruckTypesV1Controller(AbstractTruckTypesServices abstractTruckTypesServices)
        {
            this.abstractTruckTypesServices = abstractTruckTypesServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckTypes_Upsert")]
        public async Task<IHttpActionResult> TruckTypes_Upsert(TruckTypes truckTypes)
        {
            var quote = abstractTruckTypesServices.TruckTypes_Upsert(truckTypes);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckTypes_ById")]
        public async Task<IHttpActionResult> TruckTypes_ById(int Id)
        {
            var quote = abstractTruckTypesServices.TruckTypes_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("TruckTypes_All")]
        public async Task<IHttpActionResult> TruckTypes_All(PageParam pageParam,string Search="")
        {
            var quote = abstractTruckTypesServices.TruckTypes_All(pageParam,Search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
