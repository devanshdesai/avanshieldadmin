﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class LocationTypesV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractLocationTypesServices  abstractLocationTypesServices;

        #endregion

        #region Cnstr
        public LocationTypesV1Controller(AbstractLocationTypesServices abstractLocationTypesServices)
        {
            this.abstractLocationTypesServices = abstractLocationTypesServices;
        }
        #endregion


        [System.Web.Http.HttpPost]
        [InheritedRoute("LocationTypes_Upsert")]
        public async Task<IHttpActionResult> LocationTypes_Upsert(LocationTypes carriers)
        {
            var quote = abstractLocationTypesServices.LocationTypes_Upsert(carriers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("LocationTypes_ById")]
        public async Task<IHttpActionResult> LocationTypes_ById(int Id)
        {
            var quote = abstractLocationTypesServices.LocationTypes_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("LocationTypes_All")]
        public async Task<IHttpActionResult> LocationTypes_All(PageParam pageParam,String Search="")
        {
            var quote = abstractLocationTypesServices.LocationTypes_All(pageParam,Search);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
