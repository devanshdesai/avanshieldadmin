﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;


namespace CargoXpressApi.Controllers.V1
{
    public class CustomersV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomersServices  abstractCustomersServices;

        #endregion

        #region Cnstr
        public CustomersV1Controller(AbstractCustomersServices abstractCustomersServices)
        {
            this.abstractCustomersServices = abstractCustomersServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_Upsert")]
        public async Task<IHttpActionResult> Customers_Upsert(Customers customers)
        {
            var quote = abstractCustomersServices.Customers_Upsert(customers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_ById")]
        public async Task<IHttpActionResult> Customers_ById(int Id)
        {
            var quote = abstractCustomersServices.Customers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_All")]
        public async Task<IHttpActionResult> Customers_All(PageParam pageParam, string Search)
        {
            var quote = abstractCustomersServices.Customers_All(pageParam, Search);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Customers_ActInAct")]
        public async Task<IHttpActionResult> Customers_ActInAct(int Id)
        {
            var quote = abstractCustomersServices.Customers_ActInAct(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
