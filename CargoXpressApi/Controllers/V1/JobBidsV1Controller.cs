﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class JobBidsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobBidsServices  abstractJobBidsServices;
        private readonly AbstractJobServices abstractJobServices;
        private readonly AbstractShippersServices abstractShippersServices;
        private readonly AbstractCarriersServices abstractCarriersServices;
        private readonly AbstractNotificationServices abstractNotificationServices;
        #endregion

        #region Cnstr
        public JobBidsV1Controller(AbstractJobBidsServices abstractJobBidsServices, AbstractJobServices abstractJobServices, AbstractShippersServices abstractShippersServices,
            AbstractCarriersServices abstractCarriersServices, AbstractNotificationServices abstractNotificationServices)
        {
            this.abstractJobBidsServices = abstractJobBidsServices;
            this.abstractJobServices = abstractJobServices;
            this.abstractShippersServices = abstractShippersServices;
            this.abstractCarriersServices = abstractCarriersServices;
            this.abstractNotificationServices = abstractNotificationServices;
        }
        #endregion
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBids_Upsert")]
        public async Task<IHttpActionResult> JobBids_Upsert(JobBids jobBids)
        {
            var quote = abstractJobBidsServices.JobBids_Upsert(jobBids);
            if(quote.Item != null)
            {
                var jobResult = abstractJobServices.Job_ById(jobBids.JobId);
                if (jobResult.Item != null)
                {
                    var shipperResult = abstractShippersServices.Shippers_ById(jobResult.Item.ShipperId);
                    string body = "UPDATE : New bid has been made for the Job ("+ jobResult.Item.JobNo+ ")";
                    string title = "CargoXpress - New Bid";
                    EmailHelper.SendPushNotification(shipperResult.Item.DeviceToken, body, title);

                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserId = jobResult.Item.ShipperId;
                    abstractNotification.UserType = 3;
                    abstractNotification.Text = body;
                    abstractNotificationServices.Notification_Upsert(abstractNotification);

                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBids_ById")]
        public async Task<IHttpActionResult> JobBids_ById(int Id)
        {
            var quote = abstractJobBidsServices.JobBids_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBids_ByJobId")]
        public async Task<IHttpActionResult> JobBids_ByJobId(PageParam pageParam, int JobId, int CarrierId=0)
        {
            var quote = abstractJobBidsServices.JobBids_ByJobId(pageParam, JobId, CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("JobBidStatus_Update")]
        public async Task<IHttpActionResult> JobBidStatus_Update(JobBids jobBids)
        {
            var quote = abstractJobBidsServices.JobBidStatus_Update(jobBids);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
