﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class DriversV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractDriversService abstractDriversServices;

        #endregion

        #region Cnstr
        public DriversV1Controller(AbstractDriversService abstractDriversServices)
        {
            this.abstractDriversServices = abstractDriversServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_Login")]
        public async Task<IHttpActionResult> Drivers_Login(Drivers drivers)
        {
            var quote = abstractDriversServices.Drivers_Login(drivers);
            if (drivers.LoginType == false && quote.Code == 200 && quote.Item != null)
            {
                try
                {
                    drivers.Otp = CommonHelper.GenerateRandomNo();
                    //EmailHelper.SendMsgViaTwilio(cleaners.Otp, quote.Item.Mobile);
                }
                catch (Exception ex)
                {

                }
            }
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_Logout")]
        public async Task<IHttpActionResult> Drivers_Logout(int Id)
        {
            var quote = abstractDriversServices.Drivers_Logout(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_Upsert")]
        public async Task<IHttpActionResult> Drivers_Upsert(Drivers drivers)
        {
            var quote = abstractDriversServices.Drivers_Upsert(drivers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_ById")]
        public async Task<IHttpActionResult> Drivers_ById(int Id)
        {
            var quote = abstractDriversServices.Drivers_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }
        // string Name, string Email , int CountryId, string PhoneNumber, bool? IsActive, string DriverLicense, string DriverID,
        //Name, Email, CountryId, PhoneNumber, IsActive, DriverLicense, DriverID, 
        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_All")]
        public async Task<IHttpActionResult> Drivers_All(PageParam pageParam, string search="", int CarrierId=0)
        {
            var quote = abstractDriversServices.Drivers_All(pageParam, search, CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_ActInact")]
        public async Task<IHttpActionResult> Drivers_ActInact(int Id)
        {
            var quote = abstractDriversServices.Drivers_ActInact(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_MobileVerify")]
        public async Task<IHttpActionResult> Drivers_MobileVerify(int Id)
        {
            var quote = abstractDriversServices.Drivers_MobileVerify(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_EmailVerified")]
        public async Task<IHttpActionResult> Drivers_EmailVerified(int Id)
        {
            var quote = abstractDriversServices.Drivers_EmailVerified(Id);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_ChangePassword")]
        public async Task<IHttpActionResult> Drivers_ChangePassword(Drivers drivers)
        {
            var quote = abstractDriversServices.Drivers_ChangePassword(drivers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Driver_GetOtp")]
        public async Task<IHttpActionResult> Driver_GetOtp(string mobileNo)
        {
            try
            {
                string Otp = CommonHelper.GenerateRandomNo();
                //EmailHelper.SendMsgViaTwilio(Otp, mobileNo);
                return this.Content((HttpStatusCode)200, Otp);
            }
            catch (Exception ex)
            {
                return this.Content((HttpStatusCode)500, "An error occured in the twilio");
            }

        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("Drivers_ProfileUpdate")]
        public async Task<IHttpActionResult> Drivers_ProfileUpdate()
        {
            Drivers drivers = new Drivers();
            var httpRequest = HttpContext.Current.Request;
            drivers.Id = Convert.ToInt32(httpRequest.Params["Id"]);
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "DriverProfile/" + drivers.Id + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                drivers.FileURL = basePath + fileName;
            }

            var quote = abstractDriversServices.Drivers_ProfileUpdate(drivers);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

    }
}
