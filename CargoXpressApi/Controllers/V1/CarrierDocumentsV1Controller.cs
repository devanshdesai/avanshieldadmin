﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;
using System.IO;

namespace CargoXpressApi.Controllers.V1
{
    public class CarrierDocumentsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCarrierDocumentsServices  abstractCarrierDocumentsServices;

        #endregion

        #region Cnstr
        public CarrierDocumentsV1Controller(AbstractCarrierDocumentsServices abstractCarrierDocumentsServices)
        {
            this.abstractCarrierDocumentsServices = abstractCarrierDocumentsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierDocuments_Upsert")]
        public async Task<IHttpActionResult> CarrierDocuments_Upsert()
        {
            CarrierDocuments carrierDocuments = new CarrierDocuments();
            var httpRequest = HttpContext.Current.Request;
            carrierDocuments.CarrierId = Convert.ToInt32(httpRequest.Params["CarrierId"]);
            carrierDocuments.DocumentType = Convert.ToInt32(httpRequest.Params["DocumentType"]);            
            if (httpRequest.Files.Count > 0)
            {
                var myFile = httpRequest.Files[0];
                string basePath = "CarrierDocuments/" + carrierDocuments.CarrierId + "/";
                string fileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + Path.GetFileName(myFile.FileName);
                if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/" + basePath))))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + basePath));
                }
                myFile.SaveAs(HttpContext.Current.Server.MapPath("~/" + basePath + fileName));
                carrierDocuments.Url = basePath + fileName;
            }

            var quote = abstractCarrierDocumentsServices.CarrierDocuments_Upsert(carrierDocuments);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierDocuments_ById")]
        public async Task<IHttpActionResult> CarrierDocuments_ById(int Id)
        {
            var quote = abstractCarrierDocumentsServices.CarrierDocuments_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CarrierDocuments_All")]
        public async Task<IHttpActionResult> CarrierDocuments_All(PageParam pageParam,int CarrierId = 0)
        {
            var quote = abstractCarrierDocumentsServices.CarrierDocuments_All(pageParam,CarrierId);
            return this.Content((HttpStatusCode)200, quote);
        }

    }
}
