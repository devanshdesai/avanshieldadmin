﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class ShipperCardsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractShipperCardsServices  abstractShipperCardsServices;

        #endregion

        #region Cnstr
        public ShipperCardsV1Controller(AbstractShipperCardsServices abstractShipperCardsServices)
        {
            this.abstractShipperCardsServices = abstractShipperCardsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperCards_Upsert")]
        public async Task<IHttpActionResult> ShipperCards_Upsert(ShipperCards carrierBankDetails)
        {
            var quote = abstractShipperCardsServices.ShipperCards_Upsert(carrierBankDetails);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperCards_ById")]
        public async Task<IHttpActionResult> ShipperCards_ById(int Id)
        {
            var quote = abstractShipperCardsServices.ShipperCards_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperCards_ByShipperId")]
        public async Task<IHttpActionResult> ShipperCards_ByShipperId(PageParam pageParam,string search="", int ShipperId= 0)
        {
            var quote = abstractShipperCardsServices.ShipperCards_ByCarrierId(pageParam,search, ShipperId);
            return this.Content((HttpStatusCode)200, quote);
        }


        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperCards_Delete")]
        public async Task<IHttpActionResult> ShipperCards_Delete(ShipperCards carrierBankDetails)
        {
            var quote = abstractShipperCardsServices.ShipperCards_Delete(carrierBankDetails);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("ShipperCards_IsDefault")]
        public async Task<IHttpActionResult> ShipperCards_IsDefault(int Id)
        {
            var quote = abstractShipperCardsServices.ShipperCards_IsDefault(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        

    }
}
