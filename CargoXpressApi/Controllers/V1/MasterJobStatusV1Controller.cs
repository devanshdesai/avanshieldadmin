﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class MasterJobStatusV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractMasterJobStatusServices  abstractMasterJobStatusServices;

        #endregion

        #region Cnstr
        public MasterJobStatusV1Controller(AbstractMasterJobStatusServices abstractMasterJobStatusServices)
        {
            this.abstractMasterJobStatusServices = abstractMasterJobStatusServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("MasterJobStatus_All")]
        public async Task<IHttpActionResult> MasterJobStatus_All(PageParam pageParam, String search, int JobStatusTypeId)
        {
            var quote = abstractMasterJobStatusServices.MasterJobStatus_All(pageParam, search, JobStatusTypeId);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
