﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;


namespace CargoXpressApi.Controllers.V1
{
    public class CustomerTransactionsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractCustomerTransactionsServices  abstractCustomerTransactionsServices;

        #endregion

        #region Cnstr
        public CustomerTransactionsV1Controller(AbstractCustomerTransactionsServices abstractCustomerTransactionsServices)
        {
            this.abstractCustomerTransactionsServices = abstractCustomerTransactionsServices;
        }
        #endregion

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerTransactions_Upsert")]
        public async Task<IHttpActionResult> CustomerTransactions_Upsert(CustomerTransactions customerTransactions)
        {
            var quote = abstractCustomerTransactionsServices.CustomerTransactions_Upsert(customerTransactions);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerTransactions_ById")]
        public async Task<IHttpActionResult> CustomerTransactions_ById(int Id)
        {
            var quote = abstractCustomerTransactionsServices.CustomerTransactions_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerTransactions_All")]
        public async Task<IHttpActionResult> CustomerTransactions_All(PageParam pageParam, string Search = null , int CustomerId = 0 , int PlanId = 0)
        {
            var quote = abstractCustomerTransactionsServices.CustomerTransactions_All(pageParam, Search, CustomerId, PlanId);
            return this.Content((HttpStatusCode)200, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("CustomerTransactions_IsReedemed")]
        public async Task<IHttpActionResult> CustomerTransactions_IsReedemed(int Id)
        {
            var quote = abstractCustomerTransactionsServices.CustomerTransactions_IsReedemed(Id);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
