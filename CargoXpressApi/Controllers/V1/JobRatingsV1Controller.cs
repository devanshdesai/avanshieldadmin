﻿using CargoXpress.APICommon;
using CargoXpressApi.Models;
using CargoXpress.Common;
using CargoXpressApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpressApi.Controllers.V1
{
    public class JobRatingsV1Controller : AbstractBaseController
    {
        #region Fields
        private readonly AbstractJobRatingsServices  abstractJobRatingsServices;

        #endregion

        #region Cnstr
        public JobRatingsV1Controller(AbstractJobRatingsServices abstractJobRatingsServices)
        {
            this.abstractJobRatingsServices = abstractJobRatingsServices;
        }
        #endregion
        
        [System.Web.Http.HttpPost]
        [InheritedRoute("JobRatingsUpsert")]
        public async Task<IHttpActionResult> JobRatings_Upsert(JobRatings jobratings)
        {
            var quote = abstractJobRatingsServices.JobRatings_Upsert(jobratings);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobRatings_ById")]
        public async Task<IHttpActionResult> JobRatings_ById(int Id)
        {
            var quote = abstractJobRatingsServices.JobRatings_ById(Id);
            return this.Content((HttpStatusCode)quote.Code, quote);
        }

        [System.Web.Http.HttpPost]
        [InheritedRoute("JobRatings_ByJobId")]
        public async Task<IHttpActionResult> JobRatings_ByJobId(PageParam pageParam, int JobId, int ShipperId = 0, int CarrierId = 0, int UserType=0)
        {
            var quote = abstractJobRatingsServices.JobRatings_ByJobId(pageParam, JobId, ShipperId, CarrierId, UserType);
            return this.Content((HttpStatusCode)200, quote);
        }


    }
}
