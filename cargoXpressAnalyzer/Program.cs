﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cargoXpressAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
            DataTable dt = new DataTable();
            string todayDate = DateTime.Now.ToString("dd-MMM-yyyy");
            string var_sql = "select * from CarrierPricingPlan where ExpiryDate='" + todayDate + "'";
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            sda.Fill(dt);

            for (int i=0; i < dt.Rows.Count; i++)
            {
                var_sql = "update CarrierPricingPlan set IsActive=0 where Id="+Convert.ToInt32(dt.Rows[i]["Id"]);
                con.Open();
                SqlCommand cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();

                if(Convert.ToInt32(dt.Rows[i]["UserType"]) == 2)
                {
                    var_sql = "update Carriers set IsPlanActive=0 where Id=" + Convert.ToInt32(dt.Rows[i]["CarrierId"]);
                }

                if (Convert.ToInt32(dt.Rows[i]["UserType"]) == 3)
                {
                    var_sql = "update Shippers set IsPlanActive=0 where Id=" + Convert.ToInt32(dt.Rows[i]["CarrierId"]);
                }

                con.Open();
                cmd = new SqlCommand(var_sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}
