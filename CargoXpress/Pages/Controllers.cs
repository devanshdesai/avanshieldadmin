﻿namespace CargoXpress.Pages
{
    public class Controllers
    {
        public const string Home = "Home";
        public const string Authentication = "Authentication";
        public const string AdminUser = "AdminUser";
        public const string Dashboard = "Dashboard";
        public const string Account = "Account";
        public const string Shippers = "Shippers";
        public const string Carriers = "Carriers";
        public const string Drivers = "Drivers";
        public const string Trucks = "Trucks";
        public const string Jobs = "Jobs";
        public const string RouteMaster = "RouteMaster";
        public const string MasterPricingPlans = "MasterPricingPlans";
        public const string Support = "Support";
        public const string PaymentMaster = "PaymentMaster";
        public const string QuickTracking = "QuickTracking";
        public const string JobDrivers = "JobDrivers";
        public const string Customers = "Customers";
        public const string MasterPlans = "MasterPlans";
    }
}