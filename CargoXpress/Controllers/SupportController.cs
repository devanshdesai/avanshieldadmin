﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DataTables.Mvc;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;

namespace CargoXpress.Controllers
{
    public class SupportController : Controller
    {
        #region Fields
        private readonly AbstractSupportServices abstractSupportServices;
        private readonly AbstractSupportReplyServices abstractSupportReplyService;

        #endregion

        #region Ctor
        public SupportController(AbstractSupportServices abstractSupportServices, AbstractSupportReplyServices abstractSupportReplyService)
        {
            this.abstractSupportServices = abstractSupportServices;
            this.abstractSupportReplyService = abstractSupportReplyService;


        }
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                
                string search = Convert.ToString(requestModel.Search.Value);


                var model = abstractSupportServices.Support_All(pageParam, 0, 0, 0, 0);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult ActInact(int ri = 0)
        {
            abstractSupportServices.Support_ActInact(ri);
            return Json("Support's status updated successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SupportDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            SuccessResult<AbstractSupport> successResult = abstractSupportServices.Support_ById(Id);
            ViewBag.SupportNo = successResult.Item.SupportNo;
            return View(successResult.Item);
        }

        [HttpPost]
        public JsonResult GetById(int Id)
        {
            // int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            SuccessResult<AbstractSupport> successResult = abstractSupportServices.Support_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult BindSupportReply(int ri)
        {
            PageParam pageParam = new PageParam();
            var model = abstractSupportReplyService.SupportReply_BySupportId(pageParam, ri);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Upsert(int SupportId,string ReplyText)
        {
            AbstractSupportReply abstractSupportReply = new SupportReply();
            abstractSupportReply.Id = 0;
            abstractSupportReply.ReplyText = ReplyText;
            abstractSupportReply.UserTypeId = 1;
            abstractSupportReply.SupportId = SupportId;
            if(abstractSupportReply.ReplyText != "" && abstractSupportReply.ReplyText != null && abstractSupportReply.ReplyText != string.Empty)
            {
                abstractSupportReplyService.SupportReply_Upsert(abstractSupportReply);
            }
            return Json(200, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}