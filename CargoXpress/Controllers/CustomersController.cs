﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class CustomersController : Controller
    {
        #region Fields
        private readonly AbstractCustomersServices abstractCustomersServices;
        private readonly AbstractCustomerTransactionsServices abstractCustomerTransactionsServices;
        private readonly AbstractCustomerPlansServices abstractCustomerPlansServices;
        private readonly AbstractCustomerActivityServices abstractCustomerActivityServices;
        #endregion

        #region Ctor
        public CustomersController(AbstractCustomersServices abstractCustomersServices,
            AbstractCustomerTransactionsServices abstractCustomerTransactionsServices, 
            AbstractCustomerPlansServices abstractCustomerPlansServices,
            AbstractCustomerActivityServices abstractCustomerActivityServices) //AbstractJobServices abstractJobServices
        {
            this.abstractCustomersServices = abstractCustomersServices;
            this.abstractCustomerTransactionsServices = abstractCustomerTransactionsServices;
            this.abstractCustomerPlansServices = abstractCustomerPlansServices;
            this.abstractCustomerActivityServices = abstractCustomerActivityServices;
        }
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index(int Id = 0)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractCustomersServices.Customers_All(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult ActInact(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            abstractCustomersServices.Customers_ActInAct(Id);
            return Json("Latest Announcement status updated successfully", JsonRequestBehavior.AllowGet);
        }
        public ActionResult CustomersDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            var result = abstractCustomersServices.Customers_ById(Id);
            ViewBag.CustomerName = result.Item.FirstName + " " + result.Item.LastName;
            return View();
        }

        public ActionResult CustomersActivities(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            var result = abstractCustomersServices.Customers_ById(Id);
            ViewBag.CustomerName = result.Item.FirstName + " " + result.Item.LastName;
            return View();
        }


        [HttpPost]
        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractCustomers> successResult = abstractCustomersServices.Customers_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Upsert(int Id, string Firstname, string Lastname, string Email, string Mobile, string CustomerKey)
        {
            AbstractCustomers abstractCustomers = new Customers();

            abstractCustomers.Id = Id;
            abstractCustomers.FirstName = Firstname;
            abstractCustomers.LastName = Lastname;
            abstractCustomers.Email = Email;
            abstractCustomers.Mobile = Mobile;
            abstractCustomers.CustomerKey = CustomerKey; 




            SuccessResult<AbstractCustomers> result = abstractCustomersServices.Customers_Upsert(abstractCustomers);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCustomerTransactionsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CustomerId = 0 , int PlanId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
               
                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractCustomerPlansServices.CustomerPlans_ByCustomerId(pageParam, search, CustomerId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCustomerActivity([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CustomerId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractCustomerActivityServices.CustomerActivity_ByCustomerId(pageParam,search,CustomerId);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult ReSendInvitation(int Id = 0, int CustomerId=0)
        {
            SuccessResult<AbstractCustomers> userData = abstractCustomersServices.Customers_ById(CustomerId);

            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            string var_sql = "select * from CustomerPlans where Id=" + Id;
            SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            int Code = 400;

            if (userData != null && userData.Item != null)
            {
                if (dt.Rows.Count > 0)
                {
                    bool rtr = SendInvitaionEmail(userData.Item.Email, userData.Item.FirstName+" "+ userData.Item.LastName, Convert.ToString(dt.Rows[0]["LiscenceKey"]), Convert.ToString(Convert.ToDateTime(dt.Rows[0]["StartDate"]).ToString("dd-MM-yyyy")), Convert.ToString(Convert.ToDateTime(dt.Rows[0]["EndDate"])));
                    if (rtr)
                    {
                        Code = 200;
                    }
                    else
                    {
                        Code = 400;
                    }
                }
                else
                {
                    Code = 400;                    
                }
            }
            else
            {
                Code = 400;                
            }

            return Json(Code, JsonRequestBehavior.AllowGet);
        }

        public bool SendInvitaionEmail(string toEmail, string username, string liscence, string From, string To)
        {
            string body = "";
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/EmailInvitation2.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@username", username);
            body = body.Replace("@LiscenceKey", liscence);
            body = body.Replace("@FromDate", From);
            body = body.Replace("@ToDate", To);
            
            return EmailHelper.Send(toEmail,"", "", "New Liscence Key", body);
            
        }

        [HttpPost]
        public JsonResult Upsert(int Id ,int CustomerId , DateTime StartDate, DateTime EndDate, string PlanName = null, string TransactionNumber = null)
        {
            AbstractCustomerPlans abstractCustomerPlans = new CustomerPlans();
            abstractCustomerPlans.Id = Id;
            abstractCustomerPlans.CustomerId = CustomerId;
            abstractCustomerPlans.TransactionNumber = TransactionNumber;
            abstractCustomerPlans.PlanName = PlanName;
            if (PlanName == "Individual - $59.99")
            {
                abstractCustomerPlans.Amount = "$59.99";
            }
            else if (PlanName == "Corporate Plan - $69.99")
            {
                abstractCustomerPlans.Amount = "$69.99";
            }
            abstractCustomerPlans.NoOfMonths = "12";
            abstractCustomerPlans.StartDate = StartDate;
            abstractCustomerPlans.EndDate = EndDate;
            //abstractCustomerPlans.RedeemedDate = null;

            var model = abstractCustomerPlansServices.CustomerPlans_Upsert(abstractCustomerPlans);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}