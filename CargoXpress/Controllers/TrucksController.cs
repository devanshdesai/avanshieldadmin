﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class TrucksController : BaseController
    {        
            #region Fields
            private readonly AbstractTrucksService trucksServices;
            private readonly AbstractTruckDocumentsService truckDocumentsService;
            private readonly AbstractTruckPrefferedDestinationsServices truckPrefferedDestinationsServices;
        private readonly AbstractTruckTypesServices truckTypesServices;
        #endregion

        #region Ctor
        public TrucksController(AbstractTrucksService trucksServices,
                AbstractTruckDocumentsService truckDocumentsService,
                AbstractTruckPrefferedDestinationsServices truckPrefferedDestinationsServices,
                AbstractTruckTypesServices truckTypesServices)
            {
                this.trucksServices = trucksServices;
                this.truckDocumentsService = truckDocumentsService;
                this.truckPrefferedDestinationsServices = truckPrefferedDestinationsServices;
            this.truckTypesServices = truckTypesServices;
            }
            #endregion

            #region Methods
            [ActionName(Actions.Index)]
            public ActionResult Index(int Id, string CarrierName)
            {
                ViewBag.CarrierId = Id;
               ProjectSession.CarrierName = CarrierName;
                return View();
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public JsonResult BindTrucksData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string PlateNumber = "", string CompanyName = "", int TruckTypeId = 0, int LoadCapacity = 0, bool? IsActive = true, int CarrierId = 0)
            {
                try
                {
                    int totalRecord = 0;
                    int filteredRecord = 0;

                    PageParam pageParam = new PageParam();
                    pageParam.Offset = requestModel.Start;
                    pageParam.Limit = requestModel.Length;

                    string search = Convert.ToString(requestModel.Search.Value);

                    var model = trucksServices.Trucks_All(pageParam, search, PlateNumber, CompanyName, TruckTypeId, LoadCapacity, CarrierId);
                    filteredRecord = (int)model.TotalRecords;
                    totalRecord = (int)model.TotalRecords;

                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    //ErrorLogHelper.Log(ex);
                    return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
                }
            }

            [ActionName(Actions.TruckDetails)]
            public ActionResult CarrierDetails(int Id)
            {

                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 20;

                var result = trucksServices.Trucks_ById(Id);

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    return View("TruckDetails", result.Item);
                }
                else
                {
                    ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

                }
                return View();
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public JsonResult BindTruckDocumentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TruckID)
            {
                try
                {
                    int totalRecord = 0;
                    int filteredRecord = 0;

                    PageParam pageParam = new PageParam();
                    pageParam.Offset = requestModel.Start;
                    pageParam.Limit = requestModel.Length;

                    string search = Convert.ToString(requestModel.Search.Value);

                    var model = truckDocumentsService.TruckDocuments_All(pageParam, TruckID);

                    filteredRecord = (int)model.TotalRecords;
                    totalRecord = (int)model.TotalRecords;

                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    //ErrorLogHelper.Log(ex);
                    return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
                }
            }
            
            [HttpPost]
            [ValidateAntiForgeryToken]
            public JsonResult BindTruckPreferredDestData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int TruckID)
            {
                try
                {
                    int totalRecord = 0;
                    int filteredRecord = 0;

                    PageParam pageParam = new PageParam();
                    pageParam.Offset = requestModel.Start;
                    pageParam.Limit = requestModel.Length;

                    string search = Convert.ToString(requestModel.Search.Value);

                    var model = truckPrefferedDestinationsServices.TruckPrefferedDestinations_All(pageParam, TruckID);
                    filteredRecord = (int)model.TotalRecords;
                    totalRecord = (int)model.TotalRecords;

                    return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    //ErrorLogHelper.Log(ex);
                    return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
                }
            }

            [HttpPost]
            public JsonResult ActInact(int TruckId)
            {
                trucksServices.Trucks_ActInact(TruckId);
                return Json("Truck's status updated successfully", JsonRequestBehavior.AllowGet);
            }
        [HttpGet]
        [ActionName(Actions.AddEditTruck)]
        public ActionResult AddEditTruck(int Id = 0, int CarrierId = 0)
        {
            ViewBag.TruckTypes = BindTruckTypeDropDown();
            if (Id == 0)
            {
                Trucks model = new Trucks();
                model.CarrierId = CarrierId;
                return View(model);
            }
            else
            {
                var result = trucksServices.Trucks_ById(Id);

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    return View(result.Item);
                }
                else
                {
                    ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

                }
                return View();
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddEditTruck)]
        public ActionResult AddEditTruck(Trucks objmodel)
        {
            
            var result = trucksServices.Trucks_Upsert(objmodel);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                return RedirectToAction("Index", new { Id = objmodel.CarrierId, CarrierName = ProjectSession.CarrierName });
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            ViewBag.TruckTypes = BindTruckTypeDropDown();
            return View(objmodel);
        }
        public IList<SelectListItem> BindTruckTypeDropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 1000;


                var model = truckTypesServices.TruckTypes_All(pageParam, "");
                foreach (var master in model.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }
        #endregion
    }
    }