﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class MasterPricingPlansController : Controller
    {
        #region Fields
        private readonly AbstractMasterPricingPlansServices abstractMasterPricingPlansServices;
        private readonly AbstractUserTypeServices abstractUserTypeServices;

        #endregion 

        #region Ctor
        public MasterPricingPlansController(AbstractMasterPricingPlansServices abstractMasterPricingPlansServices, AbstractUserTypeServices abstractUserTypeServices) //AbstractJobServices abstractJobServices
        {
            this.abstractMasterPricingPlansServices = abstractMasterPricingPlansServices;
            this.abstractUserTypeServices = abstractUserTypeServices;

        }
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index(int Id = 0)
        {
            ViewBag.UserTypeName = BindDropDowns("userTypeName");
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);


                var model = abstractMasterPricingPlansServices.MasterPricingPlans_All(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindDropDowns(string action = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

                if (action == "userTypeName")
                {
                    var model = abstractUserTypeServices.UserType_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        
                        if (master.Id == 2)
                        {
                            items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                        }
                        else if (master.Id == 3)
                        {
                            items.Add(new SelectListItem() { Text = master.Name.ToString(), Value = Convert.ToString(master.Id) });
                        }
                    }
                }

                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }

        [HttpPost]
        public JsonResult ActInact(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            abstractMasterPricingPlansServices.MasterPricingPlans_ActInact(Id);
            return Json("MasterPricingPlans status updated successfully", JsonRequestBehavior.AllowGet);
        }

        public ActionResult MasterPricingPlansDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            return View();
        }
        [HttpPost]
        public JsonResult GetById(int Id)
        {
            SuccessResult<AbstractMasterPricingPlans> successResult = abstractMasterPricingPlansServices.MasterPricingPlans_ById(Id);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Upsert(int Id, string PlanName, string Description, decimal Price, decimal TimeLimit, decimal AdvPayPerc, int TotalNoOfInterServices, decimal AdditionalServicePrice,int UserType)
        
        {
            AbstractMasterPricingPlans abstractMasterPricingPlans = new MasterPricingPlans();
            abstractMasterPricingPlans.Id = Id;
            abstractMasterPricingPlans.PlanName = PlanName;
            abstractMasterPricingPlans.Description = Description;
            abstractMasterPricingPlans.Price = Price;
            abstractMasterPricingPlans.TimeLimit = TimeLimit;
            abstractMasterPricingPlans.AdvPayPerc = AdvPayPerc;
            abstractMasterPricingPlans.TotalNoOfInterServices = TotalNoOfInterServices;
            abstractMasterPricingPlans.AdditionalServicePrice = AdditionalServicePrice;
            abstractMasterPricingPlans.UserType = UserType;


            SuccessResult<AbstractMasterPricingPlans> result = abstractMasterPricingPlansServices.MasterPricingPlans_Upsert(abstractMasterPricingPlans);
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        
        #endregion
    }
}
