﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Device.Location;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class JobsFinderController : BaseController
    {
        #region Fields
        
        private readonly AbstractJobBidsServices jobBidsServices;
        private readonly AbstractJobActivityServices jobActivityServices;
        private readonly AbstractJobServices jobServices;
        private readonly AbstractJobDriverCommentsServices jobDriverCommentsServices;
        private readonly AbstractJobCommentsServices jobCommentsServices;
        private readonly AbstractJobDocumentsServices jobDocumentsServices;
        private readonly AbstractJobDriverLocationServices abstractJobDriverLocationServices;
        private readonly AbstractJobRatingsServices abstractJobRatingsServices;
        private readonly AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices;
        #endregion

        #region Ctor

        public JobsFinderController(AbstractJobBidsServices jobBidsServices,
            AbstractJobServices jobServices,
            AbstractJobActivityServices jobActivityServices,
            AbstractJobDriverCommentsServices jobDriverCommentsServices,
            AbstractJobCommentsServices jobCommentsServices,
            AbstractJobDocumentsServices jobDocumentsServices,
            AbstractJobDriverLocationServices abstractJobDriverLocationServices,
            AbstractJobRatingsServices abstractJobRatingsServices,
            AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices)
        {
            this.jobBidsServices = jobBidsServices;
            this.jobActivityServices = jobActivityServices;           
            this.jobServices = jobServices;
            this.jobDriverCommentsServices = jobDriverCommentsServices;
            this.jobCommentsServices = jobCommentsServices;
            this.jobDocumentsServices = jobDocumentsServices;
            this.abstractJobDriverLocationServices = abstractJobDriverLocationServices;
            this.abstractJobRatingsServices = abstractJobRatingsServices;
            this.abstractJobBillOfLoadingServices = abstractJobBillOfLoadingServices;
        }
        #endregion

        #region Methods
        public ActionResult Index()
        {
            //PageParam pageParam = new PageParam();
            //var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, 0);
            //var result2 = jobServices.Job_ById(JobId);
            //if (result.Values.Count > 0)
            //{
            //    ViewBag.Latitude = result.Values[result.Values.Count-1].Latitude;
            //    ViewBag.Longitude = result.Values[result.Values.Count - 1].Longitude;
            //    ViewBag.CapturedDate = result.Values[result.Values.Count - 1].CapturedDate;
            //}
            //else
            //{
            //    ViewBag.Latitude = 0.0;
            //    ViewBag.Longitude = 0.0;
            //    ViewBag.CapturedDate = "";
            //}
            //ViewBag.JobId = JobId;
            //if (result2 != null && result2.Code == 200 && result2.Item != null)
            //{
            //    ViewBag.JobNo = result2.Item.JobNo;
            //}
            //else
            //{
            //    ViewBag.JobNo = "";
            //}
            return View();
        }


        public JsonResult TruckLocationCo(string JobNo, string lat, string lon, string dist)
        {
            List<JobDriverLocation> obj = new List<JobDriverLocation>();
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            DataTable dt = new DataTable();
            string var_sql = "select Id from Job where JobNo='" + JobNo + "'";
            SqlDataAdapter sda = new SqlDataAdapter(var_sql,con);
            sda.Fill(dt);
            int JobId = 0;
            if(dt.Rows.Count > 0)
            {
                JobId = Convert.ToInt32(dt.Rows[0]["Id"]);
            }
            else
            {
                JobId = -1;
            }
            PageParam pageParam = new PageParam();
            var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, 0);
            for(int i=0; i < result.Values.Count; i++)
            {
                var sCoord = new GeoCoordinate(ConvertTo.Double(result.Values[i].Latitude), ConvertTo.Double(result.Values[i].Longitude));
                var eCoord = new GeoCoordinate(Convert.ToDouble(lat), Convert.ToDouble(lon));
                double distance = sCoord.GetDistanceTo(eCoord);
                if(distance <= (Convert.ToDouble(dist)*1000))
                {
                    JobDriverLocation locObj = new JobDriverLocation();
                    locObj.Latitude = result.Values[i].Latitude;
                    locObj.Longitude = result.Values[i].Longitude;
                    obj.Add(locObj);
                }
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}