﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class CarriersController : BaseController
    {
        #region Fields
        private readonly AbstractCarriersServices carriersServices;
        private readonly AbstractCarrierDocumentsServices carrierDocumentsServices;
        private readonly AbstractCarrierTruckServicesServices carrierTruckServices;
        private readonly AbstractCarrierPreferedDestinationsServices carrierPreferedDestinationsServices;
        private readonly AbstractCountriesService countriesService;
        private readonly AbstractCarrierPricingPlanServices abstractCarrierPricingPlanServices;
        #endregion

        #region Ctor
        public CarriersController(AbstractCarriersServices carriersServices,
            AbstractCarrierDocumentsServices carrierDocumentsServices,
            AbstractCarrierTruckServicesServices carrierTruckServices,
            AbstractCarrierPreferedDestinationsServices carrierPreferedDestinationsServices,
            AbstractCountriesService countriesService, AbstractCarrierPricingPlanServices abstractCarrierPricingPlanServices)
        {
            this.carriersServices = carriersServices;
            this.carrierDocumentsServices = carrierDocumentsServices;
            this.carrierTruckServices = carrierTruckServices;
            this.carrierPreferedDestinationsServices = carrierPreferedDestinationsServices;
            this.countriesService = countriesService;
            this.abstractCarrierPricingPlanServices = abstractCarrierPricingPlanServices;
        }
        #endregion

        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCarriersData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Name = "", string Email = "", string Location = "", int CountryId = 0, string PhoneNumber = "", bool? IsActive = true)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = carriersServices.Carriers_All(pageParam, search, Name, Email, CountryId, PhoneNumber, IsActive);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.CarrierDetails)]
        public ActionResult CarrierDetails(int Id)
        {

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 20;

            var result = carriersServices.Carriers_ById(Id);
            
            if (result != null && result.Code == 200 && result.Item != null)
            {
                ProjectSession.CarrierName = result.Item.FullName;
                return View("CarrierDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCarrierDocumentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CarrierID)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = carrierDocumentsServices.CarrierDocuments_All(pageParam, CarrierID);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCarrierTrucksData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CarrierID)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = carrierTruckServices.CarrierTruckServices_All(pageParam, CarrierID, null);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCarrierPreferredDestData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CarrierID)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = carrierPreferedDestinationsServices.CarrierPreferedDestinations_All(pageParam, CarrierID);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ActInact(int CarrierId)
        {
            carriersServices.Carriers_ActInact(CarrierId);
            return Json("Carrier's status updated successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCarrierPricingPlan([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int CarrierID = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractCarrierPricingPlanServices.CarrierPricingPlan_ByCarrierId(pageParam, search, CarrierID, 2);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ApprovedCarrier(int CarrierId)
        {
            carriersServices.Carriers_Approve(CarrierId);
            return Json("Carrier approved successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AddEditCarrier)]
        public ActionResult AddEditCarrier(int Id = 0)
        {
            ViewBag.Countries = BindCountryDropDown();
            if (Id == 0)
            {
                return View();
            }
            else
            {
                var result = carriersServices.Carriers_ById(Id);

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    return View(result.Item);
                }
                else
                {
                    ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

                }
                return View();
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddEditCarrier)]
        public ActionResult AddEditCarrier(Carriers objmodel)
        {
            if (objmodel.Id == 0)
            {
                objmodel.Password = CommonHelper.RandomPassword();
            }
            var result = carriersServices.Carriers_Upsert(objmodel);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            ViewBag.Countries = BindCountryDropDown();
            return View(objmodel);
        }
        public IList<SelectListItem> BindCountryDropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;


                var model = countriesService.Countries_All(pageParam, "");
                foreach (var master in model.Values)
                {
                    items.Add(new SelectListItem() { Text = master.CountryCode.ToString(), Value = Convert.ToString(master.Id) });
                }


                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }
        #endregion
    }
}