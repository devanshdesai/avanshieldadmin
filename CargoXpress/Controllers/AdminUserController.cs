﻿using CargoXpress.Common;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
   
    public class AdminUserController : BaseController
    {
        #region Fields
        private readonly AbstractAdminUserServices adminUserService;
        #endregion

        #region Ctor
        public AdminUserController(AbstractAdminUserServices adminUserService)
        {
            this.adminUserService = adminUserService;
        }
        #endregion

        #region Methods
        [HttpGet]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ChangePassword)]
        public ActionResult ChangePassword(AdminUser objmodel)
        {

            objmodel.Id = ProjectSession.AdminUserID;
            var result = adminUserService.AdminUser_ChangePassword(objmodel);
            if (result == true)
            {
                return RedirectToAction(Actions.LogIn, Pages.Controllers.Account);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result);
            }
            return View();
        }      
       
        #endregion
    }
}