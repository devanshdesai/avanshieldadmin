﻿using CargoXpress.Common;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using System;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly AbstractAdminUserServices abstractAdminUserServices;
        #endregion

        #region Ctor
        public AccountController(AbstractAdminUserServices abstractAdminUserServices)
        {
            this.abstractAdminUserServices = abstractAdminUserServices;
        }
        #endregion

        #region Methods
        [HttpGet]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [ActionName(Actions.LogIn)]
        public JsonResult LogIn(string username, string password)
        {
            AdminUser tdd_Client = new AdminUser();
            tdd_Client.Username = username;
            tdd_Client.Password = password;

            SuccessResult<AbstractAdminUser> result = abstractAdminUserServices.AdminUser_LogIn(tdd_Client);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                Session.Clear();
                ProjectSession.AdminUserID = result.Item.Id;
                ProjectSession.UserName = result.Item.FirstName;


                HttpCookie cookie = new HttpCookie("UserLogin");
                cookie.Values.Add("Id", result.Item.Id.ToString());
                cookie.Values.Add("UserName", result.Item.FirstName.ToString());
                cookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie);
                result.Item = null;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        [AllowAnonymous]
        [ActionName(Actions.Logout)] 
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Request.Cookies["UserLogin"] != null)
            {
                int userid = 0;
                HttpCookie reqCokie = Request.Cookies["UserLogin"];
                userid = Convert.ToInt32(reqCokie["Id"].ToString());
                abstractAdminUserServices.AdminUser_LogOut(userid);

                var c = new HttpCookie("UserLogin");
                c.Expires = DateTime.Now.AddDays(0);
                Response.Cookies.Add(c);
            }

            return RedirectToAction(Actions.LogIn, CargoXpress.Pages.Controllers.Account);
        }

        #endregion
    }
}