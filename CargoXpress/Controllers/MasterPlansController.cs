﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using GoogleMaps.LocationServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;
using System.Device.Location;

namespace CargoXpress.Controllers
{
    public class MasterPlansController : Controller
    {
        #region Fields
        private readonly AbstractMasterPlansServices abstractMasterPlansServices;
        #endregion

        #region Ctor

        public MasterPlansController(AbstractMasterPlansServices abstractMasterPlansServices)
        {
            this.abstractMasterPlansServices = abstractMasterPlansServices;
        }
        #endregion
        
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index(int Id = 0)
        {
            ViewBag.Id = Id;            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractMasterPlansServices.MasterPlans_All(pageParam,search);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

      
        [HttpPost]
        public JsonResult GetById(int Id)
        {
            var model = abstractMasterPlansServices.MasterPlans_ById(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CustomersDetails(string ri = "MA==")
        {
            int Id = Convert.ToInt32(ConvertTo.Base64Decode(ri));
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public JsonResult Upsert(int Id = 0, string Name = null, int NoOfMonths = 0 , string Description = null, decimal Amount = 0)
        {
            AbstractMasterPlans abstractMasterPlans = new MasterPlans();
            abstractMasterPlans.Id = Id;
            abstractMasterPlans.Name = Name;
            abstractMasterPlans.NoOfMonths = NoOfMonths;
            abstractMasterPlans.Description = Description;
            abstractMasterPlans.Amount = Amount;

            var model = abstractMasterPlansServices.MasterPlans_Upsert(abstractMasterPlans);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

       
    

        #endregion
    }
}