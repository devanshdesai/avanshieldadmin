﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class ShippersController : BaseController
    {
        #region Fields
        private readonly AbstractShippersServices shippersServices;
        private readonly AbstractShipperAddressesService shipperAddressesServices;
        private readonly AbstractShiperDocumentsServices shiperDocumentsServices;
        private readonly AbstractCountriesService countriesService;
        private readonly AbstractCarrierPricingPlanServices abstractCarrierPricingPlanServices;
        #endregion

        #region Ctor
        public ShippersController(AbstractShippersServices shippersServices, 
            AbstractShipperAddressesService shipperAddressesServices,
            AbstractShiperDocumentsServices shiperDocumentsServices,
            AbstractCountriesService countriesService, AbstractCarrierPricingPlanServices abstractCarrierPricingPlanServices)
        {
            this.shippersServices = shippersServices;
            this.shipperAddressesServices = shipperAddressesServices;
            this.shiperDocumentsServices = shiperDocumentsServices;
            this.countriesService = countriesService;
            this.abstractCarrierPricingPlanServices = abstractCarrierPricingPlanServices;
        }
        #endregion

        #region Methods
        // GET: Shippers
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {   
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindShippersData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string Name = "", string Email = "", string Location = "", int CountryId = 0, string PhoneNumber = "", bool? IsActive=true)
       {
            //public override PagedList<AbstractShippers> Shippers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive)
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractShippers abstractShippers = new Shippers();

                var model = shippersServices.Shippers_All(pageParam, search, Name, Email, CountryId, PhoneNumber, IsActive);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCarrierPricingPlan([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ShipperID = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                var model = abstractCarrierPricingPlanServices.CarrierPricingPlan_ByCarrierId(pageParam, search, ShipperID, 3);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.ShipperDetails)]
        public ActionResult ShipperDetails(int Id)
        {   
            var result = shippersServices.Shippers_ById(Id);
            
            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("ShipperDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);
                
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindShipperDocumentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ShipperID)
        {
            //public override PagedList<AbstractShippers> Shippers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive)
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                //AbstractShippers abstractShippers = new Shippers();
                var model = shiperDocumentsServices.ShiperDocuments_All(pageParam, ShipperID, null);
                
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindShipperAddressesData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int ShipperID)
        {
            //public override PagedList<AbstractShippers> Shippers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive)
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                //AbstractShippers abstractShippers = new Shippers();
                var model = shipperAddressesServices.ShipperAddresses_All(pageParam, ShipperID, null);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ActInact(int ShipperId)
        {
            shippersServices.Shippers_ActInact(ShipperId);
            return Json("Shipper's status updated successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.AddEditShipper)]
        public ActionResult AddEditShipper(int Id = 0)
        {
            ViewBag.Countries = BindCountryDropDown();
            if (Id == 0)
            {
                return View();
            }
            else
            {
                var result = shippersServices.Shippers_ById(Id);

                if (result != null && result.Code == 200 && result.Item != null)
                {
                    return View(result.Item);
                }
                else
                {
                    ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

                }
                return View();
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddEditShipper)]
        public ActionResult AddEditShipper(Shippers objmodel)
        {    
            if(objmodel.Id == 0)
            {
                objmodel.Password = CommonHelper.RandomPassword();
            }
            var result = shippersServices.Shippers_Upsert(objmodel);
            if (result != null && result.Code == 200 && result.Item != null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            ViewBag.Countries = BindCountryDropDown();
            return View(objmodel);
        }
        public IList<SelectListItem> BindCountryDropDown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = 0;

               
                    var model = countriesService.Countries_All(pageParam, "");
                    foreach (var master in model.Values)
                    {
                        items.Add(new SelectListItem() { Text = master.CountryCode.ToString(), Value = Convert.ToString(master.Id) });
                    }
                
                
                return items;
            }
            catch (Exception ex)
            {
                return items;
            }
        }
        #endregion
    }
}
