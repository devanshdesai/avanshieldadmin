﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Mvc;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;

namespace TodoDashboard.Controllers
{
    public class HomeController : BaseController
    {
        #region Fields
        #endregion

        #region Ctor
        public HomeController()
        {
            
        }
        #endregion

        #region Methods

        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindLocationData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,string Email = "",string Location = "",int StateId = 0, int ServiceTypeId = 0,int BarTypeId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                //AbstractMasterLocations abstractMasterLocations = new MasterLocations();
                //abstractMasterLocations.LocationId = LocationId;
                //abstractMasterLocations.LocationName = Location;
                //abstractMasterLocations.StateId = StateId;
                //abstractMasterLocations.ServiceTypeId = ServiceTypeId;
                //abstractMasterLocations.BarTypeId = BarTypeId;

                //var model = abstractMasterLocationsService.MasterLocations_All(pageParam, search, abstractMasterLocations);
                //filteredRecord = (int)model.TotalRecords; 
                //totalRecord = (int)model.TotalRecords;


                return Json(new DataTablesResponse(requestModel.Draw, null, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public ActionResult SampleAdd()
        {
            return View();
        }
    }
}