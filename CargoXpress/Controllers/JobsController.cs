﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class JobsController : BaseController
    {
        #region Fields
        
        private readonly AbstractJobBidsServices jobBidsServices;
        private readonly AbstractJobActivityServices jobActivityServices;
        private readonly AbstractJobDriversServices JobDriversServices;
        private readonly AbstractJobServices jobServices;
        private readonly AbstractJobDriverCommentsServices jobDriverCommentsServices;
        private readonly AbstractJobCommentsServices jobCommentsServices;
        private readonly AbstractJobDocumentsServices jobDocumentsServices;
        private readonly AbstractJobDriverLocationServices abstractJobDriverLocationServices;
        private readonly AbstractJobRatingsServices abstractJobRatingsServices;
        private readonly AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices;
        #endregion

        #region Ctor

        public JobsController(AbstractJobBidsServices jobBidsServices,
            AbstractJobServices jobServices,
            AbstractJobActivityServices jobActivityServices,
            AbstractJobDriversServices jobDriversServices,
            AbstractJobDriverCommentsServices jobDriverCommentsServices,
            AbstractJobCommentsServices jobCommentsServices,
            AbstractJobDocumentsServices jobDocumentsServices,
            AbstractJobDriverLocationServices abstractJobDriverLocationServices,
            AbstractJobRatingsServices abstractJobRatingsServices,
            AbstractJobBillOfLoadingServices abstractJobBillOfLoadingServices)
        {
            this.jobBidsServices = jobBidsServices;
            this.jobActivityServices = jobActivityServices;           
            this.JobDriversServices = jobDriversServices;           
            this.jobServices = jobServices;
            this.jobDriverCommentsServices = jobDriverCommentsServices;
            this.jobCommentsServices = jobCommentsServices;
            this.jobDocumentsServices = jobDocumentsServices;
            this.abstractJobDriverLocationServices = abstractJobDriverLocationServices;
            this.abstractJobRatingsServices = abstractJobRatingsServices;
            this.abstractJobBillOfLoadingServices = abstractJobBillOfLoadingServices;
        }
        #endregion
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            int totalRecord = 0;

            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 1000;

            string search = "";
            var model = jobServices.Job_All(pageParam, search, null, 0, 0, 0, 0, 0, 0, DateTime.Now, DateTime.Now, DateTime.Now, "All");
            totalRecord = (int)model.TotalRecords;

            int[] NJSId = { 1, 2 };
            ProjectSession.NewJobs = model.Values.Where(w => NJSId.Contains(w.JobStatusId)).Count();
            int[] OJSId = {3, 4, 5, 6, 7, 8, 9};
            ProjectSession.OngoingJobs = model.Values.Where(w => OJSId.Contains(w.JobStatusId)).Count();
            ProjectSession.CompletedJobs = model.Values.Where(w => w.JobStatusId == 10).Count();
            ProjectSession.CancelledJobs = model.Values.Where(w => w.JobStatusId == 11).Count();


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindOngoingJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s=>s.Id).FirstOrDefault();
                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "OngoingJob");
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindNewJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                //  JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s => s.Id).FirstOrDefault();

                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "NewJob");
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCompletedJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                // var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s => s.Id).FirstOrDefault();
                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "CompletedJob");
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindCancelledJobsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string JobStatus = "", string JobNo = "", int ShipperId = 0, int CarrierId = 0, int DriverId = 0, int TruckId = 0, int JobStatusId = 0, int TruckTypeId = 0, DateTime? CarrierJobAssignedDate = null, DateTime? CarrierJobStartDate = null, DateTime? CarrierJobEndDate = null)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                AbstractJob job = new Job();
                //var result = jobStatusServices.JobStatus_All(pageParam, search);
                // JobStatusId = result.Values.Where(w => w.Name == JobStatus).Select(s => s.Id).FirstOrDefault();
                CarrierJobAssignedDate = DateTime.Now;
                CarrierJobStartDate = DateTime.Now;
                CarrierJobEndDate = DateTime.Now;

                var model = jobServices.Job_All(pageParam, search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, CarrierJobAssignedDate, CarrierJobStartDate, CarrierJobEndDate, "CancelledJob");
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [ActionName(Actions.OngoingJobDetails)]
        public ActionResult OngoingJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("OngoingJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }

        [ActionName(Actions.NewJobDetails)]
        public ActionResult NewJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("NewJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobBidsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {   
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                int CarrierId = 0;

                //AbstractShippers abstractShippers = new Shippers();
                var model = jobBidsServices.JobBids_ByJobId(pageParam, JobId, CarrierId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobActivityData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);
                int CarrierId = 0;

                //AbstractShippers abstractShippers = new Shippers();
                var model = jobActivityServices.JobActivity_ByJobId(pageParam, JobId, CarrierId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.CompletedJobDetails)]
        public ActionResult CompletedJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                PageParam page = new PageParam();
                var shipperratingResult = abstractJobRatingsServices.JobRatings_ByJobId(page, Id, 0, 0, 2);
                if(shipperratingResult.Values.Count > 0)
                {
                    result.Item.RatingToShipper = ConvertTo.Decimal(shipperratingResult.Values[0].RatingToShipper);
                    result.Item.CommentToShipper = shipperratingResult.Values[0].CommentToShipper;
                    result.Item.RatingToShipperDate = shipperratingResult.Values[0].RatingToShipperDateStr;
                }
                var carrierratingResult = abstractJobRatingsServices.JobRatings_ByJobId(page, Id, 0, 0, 3);
                if (shipperratingResult.Values.Count > 0)
                {
                    result.Item.RatingToCarrier = ConvertTo.Decimal(carrierratingResult.Values[0].RatingToShipper);
                    result.Item.CommentToCarrier = carrierratingResult.Values[0].CommentToShipper;
                    result.Item.RatingToCarrierDate= carrierratingResult.Values[0].RatingToShipperDateStr;
                }
                return View("CompletedJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }

        [ActionName(Actions.CancelledJobDetails)]
        public ActionResult CancelledJobDetails(int Id)
        {
            var result = jobServices.Job_ById(Id);

            if (result != null && result.Code == 200 && result.Item != null)
            {
                return View("CancelledJobDetails", result.Item);
            }
            else
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), result.Message);

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobDriverCommentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId = 0, int DriverId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = jobDriverCommentsServices.JobDriverComments_All(pageParam, JobId, DriverId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobCommentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = jobCommentsServices.JobComments_ByJobId(pageParam, JobId);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindBillOfLoading([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = abstractJobBillOfLoadingServices.JobBillOfLoading_All(pageParam, "", JobId, "");
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AddEditJobComment(string Comment, int JobId=0, int Id=0)
        {

            AbstractJobComments jobComments = new JobComments();
            jobComments.Id = Id;
            jobComments.JobId = JobId;
            jobComments.Comment = Comment;
            jobComments.CommentDate = DateTime.Now;
            if (Id == 0)
            {   
                jobComments.CreatedBy = ProjectSession.AdminUserID;
            }
            
            jobComments.UpdatedBy = ProjectSession.AdminUserID;
            
            SuccessResult<AbstractJobComments> result = jobCommentsServices.JobComments_Upsert(jobComments);
                       
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteJobComment(int JobCommentId)
        {
            if (JobCommentId > 0)
            {
                DeleteJobComments(JobCommentId);
                return Json("200", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("400", JsonRequestBehavior.AllowGet);
            }
        }
        public void DeleteJobComments(int JobCommentId)
        {
            string var_sql = "delete from JobComments where Id=" + JobCommentId;
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand(var_sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        [HttpPost]
        public JsonResult GetJobCommentById(int JobCommentId)
        {
            SuccessResult<AbstractJobComments> successResult = jobCommentsServices.JobComments_ById(JobCommentId);
            return Json(successResult, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobDocumentsData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = jobDocumentsServices.JobDocuments_ByJobId(pageParam, JobId, 0);
                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TruckLocation(int JobId)
        {
            PageParam pageParam = new PageParam();
            //var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, 0);
            var result2 = jobServices.Job_ById(JobId);
            //if (result.Values.Count > 0)
            //{
            //    ViewBag.Latitude = result.Values[result.Values.Count-1].Latitude;
            //    ViewBag.Longitude = result.Values[result.Values.Count - 1].Longitude;
            //    ViewBag.CapturedDate = result.Values[result.Values.Count - 1].CapturedDate;
            //}
            //else
            //{
            //    ViewBag.Latitude = 0.0;
            //    ViewBag.Longitude = 0.0;
            //    ViewBag.CapturedDate = "";
            //}
            ViewBag.JobId = JobId;
            if (result2 != null && result2.Code == 200 && result2.Item != null)
            {
                ViewBag.JobNo = result2.Item.JobNo;
            }
            else
            {
                ViewBag.JobNo = "";
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BindJobDriversData([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int JobId)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = Convert.ToString(requestModel.Search.Value);

                //AbstractShippers abstractShippers = new Shippers();
                var model = JobDriversServices.JobDrivers_ByJobId(pageParam, JobId);

                filteredRecord = (int)model.TotalRecords;
                totalRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ErrorLogHelper.Log(ex);
                return Json(new object[] { null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TruckLocationCo(int JobId)
        {
            PageParam pageParam = new PageParam();
            var result = abstractJobDriverLocationServices.JobDriverLocation_ByJobId(pageParam, JobId, 0);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}