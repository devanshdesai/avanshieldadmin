﻿using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
using CargoXpress.Infrastructure;
using CargoXpress.Pages;
using CargoXpress.Services.Contract;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CargoXpress.Infrastructure.Enums;

namespace CargoXpress.Controllers
{
    public class DashboardController : BaseController
    {
        #region Fields
        
        //private readonly AbstractJobBidsServices jobBidsServices;
        //private readonly AbstractJobActivityServices jobActivityServices;
        //private readonly AbstractJobCommentsServices jobCommentsServices;
        //private readonly AbstractJobDocumentsServices jobDocumentsServices;
        //private readonly AbstractJobDriverLocationServices jobDriverLocationServices;
        //private readonly AbstractJobRatingsServices jobRatingsServices;
        //private readonly AbstractJobServices jobServices;
        //private readonly AbstractJobStatusServices jobStatusServices;
        //private readonly AbstractMasterJobStatusServices masterJobStatusServices;
        #endregion

        #region Ctor
        //public DashboardController(
        //   AbstractJobCommentsServices jobCommentsServices,
        //    AbstractJobDocumentsServices jobDocumentsServices,
        //    AbstractJobDriverLocationServices jobDriverLocationServices,
        //    AbstractJobRatingsServices jobRatingsServices,
        //    AbstractJobServices jobServices,
        //    AbstractJobStatusServices jobStatusServices,
        //    AbstractMasterJobStatusServices masterJobStatusServices)
        public DashboardController()
        {
            //this.jobBidsServices = jobBidsServices;
            //this.jobActivityServices = jobActivityServices;
            //this.jobCommentsServices = jobCommentsServices;
            //this.jobDocumentsServices = jobDocumentsServices;
            //this.jobDriverLocationServices = jobDriverLocationServices;
            //this.jobRatingsServices = jobRatingsServices;
            //this.jobServices = jobServices;
            //this.jobStatusServices = jobStatusServices;
            //this.masterJobStatusServices = masterJobStatusServices;
        }
        #endregion
        #region Methods
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            //int totalRecord = 0;

            //PageParam pageParam = new PageParam();
            //pageParam.Offset = 0;
            //pageParam.Limit = 1000;

            //string search = "";
            //var model = jobServices.Job_All(pageParam, search, null, 0, 0, 0, 0, 0, 0, DateTime.Now, DateTime.Now, DateTime.Now, "All");
            //totalRecord = (int)model.TotalRecords;

            //int[] NJSId = { 1, 2 };
            //ProjectSession.NewJobs = model.Values.Where(w => NJSId.Contains(w.JobStatusId)).Count();
            //int[] OJSId = {3, 4, 5, 6, 7, 8, 9};
            //ProjectSession.OngoingJobs = model.Values.Where(w => OJSId.Contains(w.JobStatusId)).Count();
            //ProjectSession.CompletedJobs = model.Values.Where(w => w.JobStatusId == 10).Count();
            //ProjectSession.CancelledJobs = model.Values.Where(w => w.JobStatusId == 11).Count();


            return View();
        }
        
        #endregion
    }
}