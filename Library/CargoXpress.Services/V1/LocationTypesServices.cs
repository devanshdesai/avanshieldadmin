﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class LocationTypesServices : AbstractLocationTypesServices
    {
        private AbstractLocationTypesDao abstractLocationTypesDao;

        public LocationTypesServices(AbstractLocationTypesDao abstractLocationTypesDao)
        {
            this.abstractLocationTypesDao = abstractLocationTypesDao;
        }

        public override SuccessResult<AbstractLocationTypes> LocationTypes_Upsert(AbstractLocationTypes abstractLocationTypes)
        {
            return abstractLocationTypesDao.LocationTypes_Upsert(abstractLocationTypes);
        }

        public override SuccessResult<AbstractLocationTypes> LocationTypes_ById(int Id)
        {
            return abstractLocationTypesDao.LocationTypes_ById(Id);
        }

        public override PagedList<AbstractLocationTypes> LocationTypes_All(PageParam pageParam, String Search)
        {
            return this.abstractLocationTypesDao.LocationTypes_All(pageParam, Search);
        }

    }
}
