﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class TruckPrefferedDestinationsServices : AbstractTruckPrefferedDestinationsServices
    {
        private AbstractTruckPrefferedDestinationsDao abstractTruckPrefferedDestinationsDao;

        public TruckPrefferedDestinationsServices(AbstractTruckPrefferedDestinationsDao abstractTruckPrefferedDestinationsDao)
        {
            this.abstractTruckPrefferedDestinationsDao = abstractTruckPrefferedDestinationsDao;
        }

        public override SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_Upsert(AbstractTruckPrefferedDestinations abstractTruckPrefferedDestinations)
        {
            return abstractTruckPrefferedDestinationsDao.TruckPrefferedDestinations_Upsert(abstractTruckPrefferedDestinations);
        }

        public override SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_ById(int Id)
        {
            return abstractTruckPrefferedDestinationsDao.TruckPrefferedDestinations_ById(Id);
        }

        public override PagedList<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_All(PageParam pageParam, int TruckId)
        {
            return abstractTruckPrefferedDestinationsDao.TruckPrefferedDestinations_All(pageParam, TruckId);
        }

    }
}
