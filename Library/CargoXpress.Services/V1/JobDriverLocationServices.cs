﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobDriverLocationServices : AbstractJobDriverLocationServices
    {
        private AbstractJobDriverLocationDao abstractJobDriverLocationDao;

        public JobDriverLocationServices(AbstractJobDriverLocationDao abstractJobDriverLocationDao)
        {
            this.abstractJobDriverLocationDao = abstractJobDriverLocationDao;
        }

        public override SuccessResult<AbstractJobDriverLocation> JobDriverLocation_Upsert(AbstractJobDriverLocation abstractJobDriverLocation)
        {
            return abstractJobDriverLocationDao.JobDriverLocation_Upsert(abstractJobDriverLocation);
        }

        public override SuccessResult<AbstractJobDriverLocation> JobDriverLocations_ById(int Id)
        {
            return abstractJobDriverLocationDao.JobDriverLocations_ById(Id);
        }

        public override PagedList<AbstractJobDriverLocation> JobDriverLocation_ByJobId(PageParam pageParam, int JobId, int DriverId)
        {
            return this.abstractJobDriverLocationDao.JobDriverLocation_ByJobId(pageParam, JobId, DriverId);
        }

    }
}
