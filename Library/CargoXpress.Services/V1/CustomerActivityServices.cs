﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CustomerActivityServices : AbstractCustomerActivityServices
    {
        private AbstractCustomerActivityDao abstractCustomerActivityDao;

        public CustomerActivityServices(AbstractCustomerActivityDao abstractCustomerActivityDao)
        {
            this.abstractCustomerActivityDao = abstractCustomerActivityDao;
        }

        public override PagedList<AbstractCustomerActivity> CustomerActivity_ByCustomerId(PageParam pageParam, string search,int CustomerId)
        {
            return this.abstractCustomerActivityDao.CustomerActivity_ByCustomerId(pageParam,search, CustomerId);
        }

        
    }
}
