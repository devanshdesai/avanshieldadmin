﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class MasterPricingPlansServices : AbstractMasterPricingPlansServices
    {
        private AbstractMasterPricingPlansDao abstractMasterPricingPlansDao;

        public MasterPricingPlansServices(AbstractMasterPricingPlansDao abstractMasterPricingPlansDao)
        {
            this.abstractMasterPricingPlansDao = abstractMasterPricingPlansDao;
        }

        public override SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_Upsert(AbstractMasterPricingPlans abstractMasterPricingPlans)
        {
            return this.abstractMasterPricingPlansDao.MasterPricingPlans_Upsert(abstractMasterPricingPlans);
        }

        public override SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_ById(int Id)
        {
            return this.abstractMasterPricingPlansDao.MasterPricingPlans_ById(Id);
        }

        public override PagedList<AbstractMasterPricingPlans> MasterPricingPlans_All(PageParam pageParam, string search = "", int UserType = 0)
        {
            return this.abstractMasterPricingPlansDao.MasterPricingPlans_All(pageParam,search, UserType);
        }

        public override SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_ActInact(int Id)
        {
            return this.abstractMasterPricingPlansDao.MasterPricingPlans_ActInact(Id);
        }

        

       
    }
}
