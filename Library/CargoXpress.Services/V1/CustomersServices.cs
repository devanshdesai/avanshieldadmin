﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CustomersServices : AbstractCustomersServices
    {
        private AbstractCustomersDao abstractCustomersDao;

        public CustomersServices(AbstractCustomersDao abstractCustomersDao)
        {
            this.abstractCustomersDao = abstractCustomersDao;
        }

        public override SuccessResult<AbstractCustomers> Customers_Upsert(AbstractCustomers abstractCustomers)
        {
            return this.abstractCustomersDao.Customers_Upsert(abstractCustomers);
        }

        public override SuccessResult<AbstractCustomers> Customers_ById(int Id)
        {
            return this.abstractCustomersDao.Customers_ById(Id);
        }
        public override SuccessResult<AbstractCustomers> Customers_ActInAct(int Id)
        {
            return this.abstractCustomersDao.Customers_ActInAct(Id);
        }
        public override PagedList<AbstractCustomers> Customers_All(PageParam pageParam, string Search)
        {
            return this.abstractCustomersDao.Customers_All(pageParam, Search);
        }

    }
}
