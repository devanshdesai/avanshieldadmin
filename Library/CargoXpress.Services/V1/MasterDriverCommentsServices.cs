﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class MasterDriverCommentsServices : AbstractMasterDriverCommentsServices
    {
        private AbstractMasterDriverCommentsDao abstractMasterDriverCommentsDao;

        public MasterDriverCommentsServices(AbstractMasterDriverCommentsDao abstractMasterDriverCommentsDao)
        {
            this.abstractMasterDriverCommentsDao = abstractMasterDriverCommentsDao;
        }

        public override PagedList<AbstractMasterDriverComments> MasterDriverComments_All(PageParam pageParam)
        {
            return this.abstractMasterDriverCommentsDao.MasterDriverComments_All(pageParam);
        }

    }
}
