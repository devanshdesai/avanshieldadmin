﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class TruckTypesServices : AbstractTruckTypesServices
    {
        private AbstractTruckTypesDao abstractTruckTypesDao;

        public TruckTypesServices(AbstractTruckTypesDao abstractTruckTypesDao)
        {
            this.abstractTruckTypesDao = abstractTruckTypesDao;
        }

        public override SuccessResult<AbstractTruckTypes> TruckTypes_Upsert(AbstractTruckTypes abstractTruckTypes)
        {
            return abstractTruckTypesDao.TruckTypes_Upsert(abstractTruckTypes);
        }

        public override SuccessResult<AbstractTruckTypes> TruckTypes_ById(int Id)
        {
            return abstractTruckTypesDao.TruckTypes_ById(Id);
        }

        public override PagedList<AbstractTruckTypes> TruckTypes_All(PageParam pageParam, string Search)
        {
            return this.abstractTruckTypesDao.TruckTypes_All(pageParam, Search);
        }

    }
}
