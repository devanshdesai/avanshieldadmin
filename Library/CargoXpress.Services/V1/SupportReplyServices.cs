﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class SupportReplyServices : AbstractSupportReplyServices
    {
        private AbstractSupportReplyDao abstractSupportReplyDao;

        public SupportReplyServices(AbstractSupportReplyDao abstractSupportReplyDao)
        {
            this.abstractSupportReplyDao = abstractSupportReplyDao;
        }

        public override SuccessResult<AbstractSupportReply> SupportReply_Upsert(AbstractSupportReply abstractSupportReply)
        {
            return abstractSupportReplyDao.SupportReply_Upsert(abstractSupportReply);
        }
        public override PagedList<AbstractSupportReply> SupportReply_BySupportId(PageParam pageParam, int SupportId)
        {
            return abstractSupportReplyDao.SupportReply_BySupportId(pageParam,SupportId);
        }
    }
}
