﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CustomerPlansServices : AbstractCustomerPlansServices
    {
        private AbstractCustomerPlansDao abstractCustomerPlansDao;

        public CustomerPlansServices(AbstractCustomerPlansDao abstractCustomerPlansDao)
        {
            this.abstractCustomerPlansDao = abstractCustomerPlansDao;
        }

        public override PagedList<AbstractCustomerPlans> CustomerPlans_ByCustomerId(PageParam pageParam, string search,int CustomerId)
        {
            return this.abstractCustomerPlansDao.CustomerPlans_ByCustomerId(pageParam,search, CustomerId);
        }

        public override SuccessResult<AbstractCustomerPlans> CustomerPlans_Upsert(AbstractCustomerPlans abstractQuote)
        {
            SuccessResult<AbstractCustomerPlans> result = this.abstractCustomerPlansDao.CustomerPlans_Upsert(abstractQuote);
            return result;
        }


    }
}
