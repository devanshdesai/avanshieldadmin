﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobBillOfLoadingServices : AbstractJobBillOfLoadingServices
    {
        private AbstractJobBillOfLoadingDao abstractJobBillOfLoadingDao;

        public JobBillOfLoadingServices(AbstractJobBillOfLoadingDao abstractJobBillOfLoadingDao)
        {
            this.abstractJobBillOfLoadingDao = abstractJobBillOfLoadingDao;
        }

        public override SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading_Upsert(AbstractJobBillOfLoading abstractJobBillOfLoading)
        {
            return this.abstractJobBillOfLoadingDao.JobBillOfLoading_Upsert(abstractJobBillOfLoading);
        }

        

        public override PagedList<AbstractJobBillOfLoading> JobBillOfLoading_All(PageParam pageParam, string search, int JobId, string BillOfLoading)
        {
            return this.abstractJobBillOfLoadingDao.JobBillOfLoading_All(pageParam,search, JobId, BillOfLoading);
        }

        public override SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading_Delete(int Id)
        {
            return this.abstractJobBillOfLoadingDao.JobBillOfLoading_Delete(Id);
        }

        

       
    }
}
