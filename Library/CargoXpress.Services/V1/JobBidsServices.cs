﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobBidsServices : AbstractJobBidsServices
    {
        private AbstractJobBidsDao abstractJobBidsDao;

        public JobBidsServices(AbstractJobBidsDao abstractJobBidsDao)
        {
            this.abstractJobBidsDao = abstractJobBidsDao;
        }

        public override SuccessResult<AbstractJobBids> JobBids_Upsert(AbstractJobBids abstractJobBids)
        {
            return this.abstractJobBidsDao.JobBids_Upsert(abstractJobBids);
        }

        public override SuccessResult<AbstractJobBids> JobBids_ById(int Id)
        {
            return abstractJobBidsDao.JobBids_ById(Id);
        }

        public override PagedList<AbstractJobBids> JobBids_ByJobId(PageParam pageParam, int JobId, int CarrierId)
        {
            return this.abstractJobBidsDao.JobBids_ByJobId(pageParam, JobId, CarrierId);
        }

        public override SuccessResult<AbstractJobBids> JobBidStatus_Update(AbstractJobBids abstractJobBids)
        {
            return this.abstractJobBidsDao.JobBidStatus_Update(abstractJobBids);
        }
    }
}
