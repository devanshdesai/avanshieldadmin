﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CarrierPreferedDestinationsServices : AbstractCarrierPreferedDestinationsServices
    {
        private AbstractCarrierPreferedDestinationsDao abstractCarrierPreferedDestinationsDao;

        public CarrierPreferedDestinationsServices(AbstractCarrierPreferedDestinationsDao abstractCarrierPreferedDestinationsDao)
        {
            this.abstractCarrierPreferedDestinationsDao = abstractCarrierPreferedDestinationsDao;
        }

        public override SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_Upsert(AbstractCarrierPreferedDestinations abstractCarrierPreferedDestinations)
        {
            return abstractCarrierPreferedDestinationsDao.CarrierPreferedDestinations_Upsert(abstractCarrierPreferedDestinations);
        }

        public override SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_ById(int Id)
        {
            return abstractCarrierPreferedDestinationsDao.CarrierPreferedDestinations_ById(Id);
        }

        public override PagedList<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_All(PageParam pageParam, int CarrierId)
        {
            return abstractCarrierPreferedDestinationsDao.CarrierPreferedDestinations_All(pageParam,CarrierId);
        }


        public override bool CarrierPreferedDestinations_Delete(int CarrierId)
        {
            return this.abstractCarrierPreferedDestinationsDao.CarrierPreferedDestinations_Delete(CarrierId);
        }

    }
}
