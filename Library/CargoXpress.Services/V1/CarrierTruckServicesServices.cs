﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CarrierTruckServicesServices : AbstractCarrierTruckServicesServices
    {
        private AbstractCarrierTruckServicesDao abstractCarrierTruckServicesDao;

        public CarrierTruckServicesServices(AbstractCarrierTruckServicesDao abstractCarrierTruckServicesDao)
        {
            this.abstractCarrierTruckServicesDao = abstractCarrierTruckServicesDao;
        }

        public override SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices_Upsert(AbstractCarrierTruckServices abstractCarrierTruckServices)
        {
            return abstractCarrierTruckServicesDao.CarrierTruckServices_Upsert(abstractCarrierTruckServices);
        }

        public override SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices_ById(int Id)
        {
            return abstractCarrierTruckServicesDao.CarrierTruckServices_ById(Id);
        }

        public override PagedList<AbstractCarrierTruckServices> CarrierTruckServices_All(PageParam pageParam, int CarrierId, AbstractCarrierTruckServices abstractCarrierTruckServices = null)
        {
            return abstractCarrierTruckServicesDao.CarrierTruckServices_All(pageParam,CarrierId, abstractCarrierTruckServices);
        }

        public override bool CarrierTruckServices_Delete(int CarrierId)
        {
            return this.abstractCarrierTruckServicesDao.CarrierTruckServices_Delete(CarrierId);
        }
    }
}
