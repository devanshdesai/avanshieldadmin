﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class ShippersServices : AbstractShippersServices
    {
        private AbstractShippersDao abstractShippersDao;

        public ShippersServices(AbstractShippersDao abstractShippersDao)
        {
            this.abstractShippersDao = abstractShippersDao;
        }
        public override SuccessResult<AbstractShippers> Shippers_Upsert(AbstractShippers abstractShippers)
        {
            return this.abstractShippersDao.Shippers_Upsert(abstractShippers);
        }
        public override SuccessResult<AbstractShippers> Shippers_ById(int Id)
        {
            return this.abstractShippersDao.Shippers_ById(Id);
        }
        public override PagedList<AbstractShippers> Shippers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive)
        {
            return this.abstractShippersDao.Shippers_All(pageParam,search,Name,Email,CountryId,PhoneNumber,IsActive);
        }
        public override SuccessResult<AbstractShippers> Shippers_Login(AbstractShippers abstractShippers)
        {
            return this.abstractShippersDao.Shippers_Login(abstractShippers);
        }
        public override SuccessResult<AbstractShippers> Shippers_ActInact(int Id)
        {
            return this.abstractShippersDao.Shippers_ActInact(Id);
        }
        public override SuccessResult<AbstractShippers> Shippers_ChangePassword(AbstractShippers abstractShippers)
        {
            return this.abstractShippersDao.Shippers_ChangePassword(abstractShippers);
        }
        public override SuccessResult<AbstractShippers> Shippers_EmailVerified(int Id)
        {
            return this.abstractShippersDao.Shippers_EmailVerified(Id);
        }
        public override SuccessResult<AbstractShippers> Shippers_MobileVerified(int Id)
        {
            return this.abstractShippersDao.Shippers_MobileVerified(Id);
        }
        public override bool Shippers_Logout(int Id)    
        {
            return this.abstractShippersDao.Shippers_Logout(Id);
        }

        public override SuccessResult<AbstractShippers> Shippers_ProfileUpdate(AbstractShippers abstractShippers)
        {
            return this.abstractShippersDao.Shippers_ProfileUpdate(abstractShippers);
        }
    }
}
