﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class ShipperAddressesService : AbstractShipperAddressesService
    {
        private AbstractShipperAddressesDao abstractShipperAddressesDao;

        public ShipperAddressesService(AbstractShipperAddressesDao abstractShipperAddressesDao)
        {
            this.abstractShipperAddressesDao = abstractShipperAddressesDao;
        }

        public override SuccessResult<AbstractShipperAddresses> ShipperAddresses_Upsert(AbstractShipperAddresses abstractShipperAddresses)
        {
            return this.abstractShipperAddressesDao.ShipperAddresses_Upsert(abstractShipperAddresses);
        }

        public override PagedList<AbstractShipperAddresses> ShipperAddresses_All(PageParam pageParam, int ShipperId, AbstractShipperAddresses abstractShipperAddresses = null)
        {
            return this.abstractShipperAddressesDao.ShipperAddresses_All(pageParam, ShipperId , abstractShipperAddresses);
        }

        public override SuccessResult<AbstractShipperAddresses> ShipperAddresses_ById(int Id)
        {
            return this.abstractShipperAddressesDao.ShipperAddresses_ById(Id);
        }

    }
}
