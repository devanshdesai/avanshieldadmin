﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class SupportServices : AbstractSupportServices
    {
        private AbstractSupportDao abstractSupportDao;

        public SupportServices(AbstractSupportDao abstractSupportDao)
        {
            this.abstractSupportDao = abstractSupportDao;
        }

        public override SuccessResult<AbstractSupport> Support_Upsert(AbstractSupport abstractSupport)
        {
            return this.abstractSupportDao.Support_Upsert(abstractSupport);
        }

        public override SuccessResult<AbstractSupport> Support_Close(int Id)
        {
            return this.abstractSupportDao.Support_Close(Id);
        }

        public override SuccessResult<AbstractSupport> Support_ActInact(int Id)
        {
            return this.abstractSupportDao.Support_ActInact(Id);
        }

        public override PagedList<AbstractSupport> Support_All(PageParam pageParam, int CarrierId, int ShipperId, int JobId, int StatusId)
        {
            return this.abstractSupportDao.Support_All(pageParam, CarrierId, ShipperId, JobId, StatusId);
        }

        public override SuccessResult<AbstractSupport> Support_ById(int Id)
        {
            return this.abstractSupportDao.Support_ById(Id);
        }
    }
}
