﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class UserTypeServices : AbstractUserTypeServices
    {
        private AbstractUserTypeDao abstractUserTypeDao;

        public UserTypeServices(AbstractUserTypeDao abstractUserTypeDao)
        {
            this.abstractUserTypeDao = abstractUserTypeDao;
        }


        public override PagedList<AbstractUserType> UserType_All(PageParam pageParam, string search)
        {
            return this.abstractUserTypeDao.UserType_All(pageParam,search);
        }

        
    }
}
