﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobServices : AbstractJobServices
    {
        private AbstractJobDao abstractJobDao;

        public JobServices(AbstractJobDao abstractJobDao)
        {
            this.abstractJobDao = abstractJobDao;
        }

        public override SuccessResult<AbstractJob> Job_Upsert(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_Upsert(abstractJob);
        }

        public override SuccessResult<AbstractJob> Job_ById(int Id)
        {
            return abstractJobDao.Job_ById(Id);
        }

        public override PagedList<AbstractJob> Job_All(PageParam pageParam, string Search, string JobNo, int ShipperId, int CarrierId, int DriverId, int TruckId, int JobStatusId, int TruckTypeId, DateTime? CarrierJobAssignedDate, DateTime? CarrierJobStartDate , DateTime? CarrierJobEndDate, string JobStatus)
        {
            return this.abstractJobDao.Job_All(pageParam, Search, JobNo, ShipperId, CarrierId, DriverId, TruckId, JobStatusId, TruckTypeId, JobStatus);
        }

        public override SuccessResult<AbstractJob> Job_AssignDriver(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_AssignDriver(abstractJob);
        }

        public override SuccessResult<AbstractJob> Job_AssignTruck(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_AssignTruck(abstractJob);
        }

        public override SuccessResult<AbstractJob> Job_CarrierJobStartDate(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_CarrierJobStartDate(abstractJob);
        }

        public override SuccessResult<AbstractJob> Job_CarrierJobEndDate(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_CarrierJobEndDate(abstractJob);
        }

        public override SuccessResult<AbstractJob> Job_JobStatus(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_JobStatus(abstractJob);
        }

        public override SuccessResult<AbstractJob> Job_AcceptBid(AbstractJob abstractJob)
        {
            return abstractJobDao.Job_AcceptBid(abstractJob);
        }

    }
}
