﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CustomerTransactionsServices : AbstractCustomerTransactionsServices
    {
        private AbstractCustomerTransactionsDao abstractCustomerTransactionsDao;

        public CustomerTransactionsServices(AbstractCustomerTransactionsDao abstractCustomerTransactionsDao)
        {
            this.abstractCustomerTransactionsDao = abstractCustomerTransactionsDao;
        }

        public override SuccessResult<AbstractCustomerTransactions> CustomerTransactions_Upsert(AbstractCustomerTransactions abstractCustomerTransactions)
        {
            return this.abstractCustomerTransactionsDao.CustomerTransactions_Upsert(abstractCustomerTransactions);
        }

        public override SuccessResult<AbstractCustomerTransactions> CustomerTransactions_ById(int Id)
        {
            return this.abstractCustomerTransactionsDao.CustomerTransactions_ById(Id);
        }
        public override SuccessResult<AbstractCustomerTransactions> CustomerTransactions_IsReedemed(int Id)
        {
            return this.abstractCustomerTransactionsDao.CustomerTransactions_IsReedemed(Id);
        }
        public override PagedList<AbstractCustomerTransactions> CustomerTransactions_All(PageParam pageParam, string Search, int CustomerId, int PlanId)
        {
            return this.abstractCustomerTransactionsDao.CustomerTransactions_All(pageParam, Search, CustomerId, PlanId);
        }

    }
}
