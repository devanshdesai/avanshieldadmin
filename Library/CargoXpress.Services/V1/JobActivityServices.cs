﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobActivityServices : AbstractJobActivityServices
    {
        private AbstractJobActivityDao abstractJobActivityDao;

        public JobActivityServices(AbstractJobActivityDao abstractJobActivityDao)
        {
            this.abstractJobActivityDao = abstractJobActivityDao;
        }

        public override PagedList<AbstractJobActivity> JobActivity_ByJobId(PageParam pageParam, int JobId, int CarrierId)
        {
            return this.abstractJobActivityDao.JobActivity_ByJobId(pageParam, JobId, CarrierId);
        }

    }
}
