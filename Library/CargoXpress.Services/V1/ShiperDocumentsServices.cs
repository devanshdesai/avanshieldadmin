﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class ShiperDocumentsServices : AbstractShiperDocumentsServices
    {
        private AbstractShiperDocumentsDao abstractShiperDocumentsDao;

        public ShiperDocumentsServices(AbstractShiperDocumentsDao abstractShiperDocumentsDao)
        {
            this.abstractShiperDocumentsDao = abstractShiperDocumentsDao;
        }

        public override SuccessResult<AbstractShiperDocuments> ShipperDocuments_Upsert(AbstractShiperDocuments abstractShiperDocuments)
        {
            return this.abstractShiperDocumentsDao.ShipperDocuments_Upsert(abstractShiperDocuments);
        }

        public override PagedList<AbstractShiperDocuments> ShiperDocuments_All(PageParam pageParam, int ShipperId, AbstractShiperDocuments abstractShiperDocuments = null)
        {
            return this.abstractShiperDocumentsDao.ShiperDocuments_All(pageParam, ShipperId , abstractShiperDocuments);
        }

        public override SuccessResult<AbstractShiperDocuments> ShiperDocuments_ById(int Id)
        {
            return this.abstractShiperDocumentsDao.ShiperDocuments_ById(Id);
        }

    }
}
