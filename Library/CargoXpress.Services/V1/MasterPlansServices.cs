﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class MasterPlansServices : AbstractMasterPlansServices
    {
        private AbstractMasterPlansDao abstractMasterPlansDao;

        public MasterPlansServices(AbstractMasterPlansDao abstractMasterPlansDao)
        {
            this.abstractMasterPlansDao = abstractMasterPlansDao;
        }
        public override SuccessResult<AbstractMasterPlans> MasterPlans_Upsert(AbstractMasterPlans abstractMasterPlans)
        {
            return this.abstractMasterPlansDao.MasterPlans_Upsert(abstractMasterPlans);
        }

        public override SuccessResult<AbstractMasterPlans> MasterPlans_ById(int Id)
        {
            return this.abstractMasterPlansDao.MasterPlans_ById(Id);
        }
        public override PagedList<AbstractMasterPlans> MasterPlans_All(PageParam pageParam, string Search)
        {
            return this.abstractMasterPlansDao.MasterPlans_All(pageParam, Search);
        }

    }
}
