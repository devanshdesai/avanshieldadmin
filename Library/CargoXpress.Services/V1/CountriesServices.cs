﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CountriesServices : AbstractCountriesService
    {
        private AbstractCountriesDao abstractCountriesDao;

        public CountriesServices(AbstractCountriesDao abstractCountriesDao)
        {
            this.abstractCountriesDao = abstractCountriesDao;
        }

        public override PagedList<AbstractCountries> Countries_All(PageParam pageParam, string search)
        {
            return abstractCountriesDao.Countries_All(pageParam, search);
        }
        
    }
}
