﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobDriverCommentsServices : AbstractJobDriverCommentsServices
    {
        private AbstractJobDriverCommentsDao abstractJobDriverCommentsDao;

        public JobDriverCommentsServices(AbstractJobDriverCommentsDao abstractJobDriverCommentsDao)
        {
            this.abstractJobDriverCommentsDao = abstractJobDriverCommentsDao;
        }

        public override SuccessResult<AbstractJobDriverComments> JobDriverComments_Upsert(AbstractJobDriverComments abstractJobDriverComments)
        {
            return abstractJobDriverCommentsDao.JobDriverComments_Upsert(abstractJobDriverComments);
        }

        public override SuccessResult<AbstractJobDriverComments> JobDriverComments_ById(int Id)
        {
            return abstractJobDriverCommentsDao.JobDriverComments_ById(Id);
        }

        public override PagedList<AbstractJobDriverComments> JobDriverComments_All(PageParam pageParam, int JobId, int DriverId)
        {
            return abstractJobDriverCommentsDao.JobDriverComments_All(pageParam, JobId, DriverId);
        }

    }
}
