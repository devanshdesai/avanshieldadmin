﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobDriversServices : AbstractJobDriversServices
    {
        private AbstractJobDriversDao abstractJobDriversDao;

        public JobDriversServices(AbstractJobDriversDao abstractJobDriversDao)
        {
            this.abstractJobDriversDao = abstractJobDriversDao;
        }
        public override SuccessResult<AbstractJobDrivers> JobDrivers_Upsert(AbstractJobDrivers abstractJobDrivers)
        {
            return this.abstractJobDriversDao.JobDrivers_Upsert(abstractJobDrivers);
        }
        public override SuccessResult<AbstractJobDrivers> JobDrivers_ById(int Id)
        {
            return this.abstractJobDriversDao.JobDrivers_ById(Id);
        }
        public override PagedList<AbstractJobDrivers> JobDrivers_ByJobId(PageParam pageParam,int JobId)
        {
            return this.abstractJobDriversDao.JobDrivers_ByJobId(pageParam, JobId);
        }
        public override SuccessResult<AbstractJobDrivers> JobDrivers_IsJobCompleted(int Id)
        {
            return this.abstractJobDriversDao.JobDrivers_IsJobCompleted(Id);
        }
        public override bool JobDrivers_Delete(int Id)
        {
            return this.abstractJobDriversDao.JobDrivers_Delete(Id);
        }

    }
}
