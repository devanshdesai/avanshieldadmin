﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class AdminUserServices : AbstractAdminUserServices
    {
        private AbstractAdminUserDao abstractAdminUserDao;

        public AdminUserServices(AbstractAdminUserDao abstractAdminUserDao)
        {
            this.abstractAdminUserDao = abstractAdminUserDao;
        }
      
        public override SuccessResult<AbstractAdminUser> AdminUser_ById(int Id)
        {
            return this.abstractAdminUserDao.AdminUser_ById(Id);
        }
        public override bool AdminUser_ChangePassword(AbstractAdminUser abstractAdminUser)
        {
            return this.abstractAdminUserDao.AdminUser_ChangePassword(abstractAdminUser);
        }
        public override SuccessResult<AbstractAdminUser> AdminUser_LogIn(AbstractAdminUser abstractAdminUser)
        {
            return this.abstractAdminUserDao.AdminUser_LogIn(abstractAdminUser);
        }
        public override bool AdminUser_LogOut(int Id)
        {
            return this.abstractAdminUserDao.AdminUser_LogOut(Id);
        }

    }
}
