﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class DriversServices : AbstractDriversService
    {
        private AbstractDriversDao abstractDriversDao;

        public DriversServices(AbstractDriversDao abstractDriversDao)
        {
            this.abstractDriversDao = abstractDriversDao;
        }

        public override SuccessResult<AbstractDrivers> Drivers_Upsert(AbstractDrivers abstractDrivers)
        {
            return this.abstractDriversDao.Drivers_Upsert(abstractDrivers);
        }

        public override SuccessResult<AbstractDrivers> Drivers_ById(int Id)
        {
            return this.abstractDriversDao.Drivers_ById(Id);
        }
        // string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive, string DriverLicense, string DriverID,
        //Name,Email,CountryId,PhoneNumber,IsActive,DriverLicense,DriverID,
        public override PagedList<AbstractDrivers> Drivers_All(PageParam pageParam, string search, int CarrierId)
        {
            return this.abstractDriversDao.Drivers_All(pageParam,search,CarrierId);
        }

        public override SuccessResult<AbstractDrivers> Drivers_Login(AbstractDrivers abstractDrivers)
        {
            return this.abstractDriversDao.Drivers_Login(abstractDrivers);
        }

        public override SuccessResult<AbstractDrivers> Drivers_ActInact(int Id)
        {
            return this.abstractDriversDao.Drivers_ActInact(Id);
        }

        public override SuccessResult<AbstractDrivers> Drivers_ChangePassword(AbstractDrivers abstractDrivers)
        {
            return this.abstractDriversDao.Drivers_ChangePassword(abstractDrivers);
        }
        public override SuccessResult<AbstractDrivers> Drivers_EmailVerified(int Id)
        {
            return this.abstractDriversDao.Drivers_EmailVerified(Id);
        }

        public override SuccessResult<AbstractDrivers> Drivers_MobileVerify(int Id)
        {
            return this.abstractDriversDao.Drivers_MobileVerify(Id);
        }

        public override bool Drivers_Logout(int Id)    
        {
            return this.abstractDriversDao.Drivers_Logout(Id);
        }

        public override SuccessResult<AbstractDrivers> Drivers_ProfileUpdate(AbstractDrivers abstractDrivers)
        {
            return this.abstractDriversDao.Drivers_ProfileUpdate(abstractDrivers);
        }

    }
}
