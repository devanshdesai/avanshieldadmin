﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobTrucksServices : AbstractJobTrucksServices
    {
        private AbstractJobTrucksDao abstractJobTrucksDao;

        public JobTrucksServices(AbstractJobTrucksDao abstractJobTrucksDao)
        {
            this.abstractJobTrucksDao = abstractJobTrucksDao;
        }
        public override SuccessResult<AbstractJobTrucks> JobTrucks_Upsert(AbstractJobTrucks abstractJobTrucks)
        {
            return this.abstractJobTrucksDao.JobTrucks_Upsert(abstractJobTrucks);
        }
        public override SuccessResult<AbstractJobTrucks> JobTrucks_ById(int Id)
        {
            return this.abstractJobTrucksDao.JobTrucks_ById(Id);
        }
        public override PagedList<AbstractJobTrucks> JobTrucks_ByJobId(PageParam pageParam,int JobId)
        {
            return this.abstractJobTrucksDao.JobTrucks_ByJobId(pageParam, JobId);
        }
        public override SuccessResult<AbstractJobTrucks> JobTrucks_IsJobCompleted(int Id)
        {
            return this.abstractJobTrucksDao.JobTrucks_IsJobCompleted(Id);
        }
        public override bool JobTrucks_Delete(int Id)
        {
            return this.abstractJobTrucksDao.JobTrucks_Delete(Id);
        }

    }
}
