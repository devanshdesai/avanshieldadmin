﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class TrucksServices : AbstractTrucksService
    {
        private AbstractTrucksDao abstractTrucksDao;

        public TrucksServices(AbstractTrucksDao abstractTrucksDao)
        {
            this.abstractTrucksDao = abstractTrucksDao;
        }

        public override SuccessResult<AbstractTrucks> Trucks_Upsert(AbstractTrucks abstractTrucks)
        {
            return this.abstractTrucksDao.Trucks_Upsert(abstractTrucks);
        }


        public override SuccessResult<AbstractTrucks> Trucks_ById(int Id)
        {
            return this.abstractTrucksDao.Trucks_ById(Id);
        }

        public override PagedList<AbstractTrucks> Trucks_All(PageParam pageParam, string search, string PlateNumber, string CompanyName, int TruckTypeId, int LoadCapacity, int CarrierId)
        {
            return this.abstractTrucksDao.Trucks_All(pageParam, search, PlateNumber, CompanyName, TruckTypeId, LoadCapacity, CarrierId);
        }

        public override SuccessResult<AbstractTrucks> Trucks_ActInact(int Id)
        {
            return this.abstractTrucksDao.Trucks_ActInact(Id);
        }

        public override SuccessResult<AbstractTrucks> Trucks_JobActInact(int Id)
        {
            return this.abstractTrucksDao.Trucks_JobActInact(Id);
        }
    }
}
