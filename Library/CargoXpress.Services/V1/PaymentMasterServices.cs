﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class PaymentMasterServices : AbstractPaymentMasterServices
    {
        private AbstractPaymentMasterDao abstractPaymentMasterDao;

        public PaymentMasterServices(AbstractPaymentMasterDao abstractPaymentMasterDao)
        {
            this.abstractPaymentMasterDao = abstractPaymentMasterDao;
        }

        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_Upsert(AbstractPaymentMaster abstractPaymentMaster)
        {
            return this.abstractPaymentMasterDao.PaymentMaster_Upsert(abstractPaymentMaster);
        }
        

        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_ById(int Id)
        {
            return this.abstractPaymentMasterDao.PaymentMaster_ById(Id);
        }

        public override PagedList<AbstractPaymentMaster> PaymentMaster_All(PageParam pageParam, string search, int JobId, int PaidByUserType = 0, int PaidToUserType = 0)
        {
            return this.abstractPaymentMasterDao.PaymentMaster_All(pageParam,search,JobId, PaidByUserType, PaidToUserType);
        }

        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_Delete(int Id)
        {
            return this.abstractPaymentMasterDao.PaymentMaster_Delete(Id);
        }

        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_RecievedNotRecieved(int Id)
        {
            return this.abstractPaymentMasterDao.PaymentMaster_RecievedNotRecieved(Id);
        }
    }
}
