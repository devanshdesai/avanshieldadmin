﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class JobRatingsServices : AbstractJobRatingsServices
    {
        private AbstractJobRatingsDao abstractJobRatingsDao;

        public JobRatingsServices(AbstractJobRatingsDao abstractJobRatingsDao)
        {
            this.abstractJobRatingsDao = abstractJobRatingsDao;
        }

        public override SuccessResult<AbstractJobRatings> JobRatings_Upsert(AbstractJobRatings abstractJobRatings)
        {
            return abstractJobRatingsDao.JobRatings_Upsert(abstractJobRatings);
        }

        public override SuccessResult<AbstractJobRatings> JobRatings_ById(int Id)
        {
            return abstractJobRatingsDao.JobRatings_ById(Id);
        }

        public override PagedList<AbstractJobRatings> JobRatings_ByJobId(PageParam pageParam, int JobId, int ShipperId, int CarrierId,int UserType)
        {
            return this.abstractJobRatingsDao.JobRatings_ByJobId(pageParam, JobId , ShipperId, CarrierId,UserType);
        }

    }
}
