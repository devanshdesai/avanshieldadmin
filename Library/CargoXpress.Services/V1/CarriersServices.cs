﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Services.Contract;

namespace CargoXpress.Services.V1
{
    public class CarriersServices : AbstractCarriersServices
    {
        private AbstractCarriersDao abstractCarriersDao;

        public CarriersServices(AbstractCarriersDao abstractCarriersDao)
        {
            this.abstractCarriersDao = abstractCarriersDao;
        }

        public override SuccessResult<AbstractCarriers> Carriers_Upsert(AbstractCarriers abstractCarriers)
        {
            return this.abstractCarriersDao.Carriers_Upsert(abstractCarriers);
        }
        public override SuccessResult<AbstractCarriers> Carriers_ProfileUpdate(AbstractCarriers abstractCarriers)
        {
            return this.abstractCarriersDao.Carriers_ProfileUpdate(abstractCarriers);
        }

        public override SuccessResult<AbstractCarriers> Carriers_ById(int Id)
        {
            return this.abstractCarriersDao.Carriers_ById(Id);
        }

        public override PagedList<AbstractCarriers> Carriers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive)
        {
            return this.abstractCarriersDao.Carriers_All(pageParam,search, Name,Email,CountryId,PhoneNumber,IsActive);
        }

        public override SuccessResult<AbstractCarriers> Carriers_Login(AbstractCarriers abstractCarriers)
        {
            return this.abstractCarriersDao.Carriers_Login(abstractCarriers);
        }

        public override SuccessResult<AbstractCarriers> Carriers_ActInact(int Id)
        {
            return this.abstractCarriersDao.Carriers_ActInact(Id);
        }

        public override SuccessResult<AbstractCarriers> Carriers_Approve(int Id)
        {
            return this.abstractCarriersDao.Carriers_Approve(Id);
        }
        public override SuccessResult<AbstractCarriers> Carriers_ChangePassword(AbstractCarriers abstractCarriers)
        {
            return this.abstractCarriersDao.Carriers_ChangePassword(abstractCarriers);
        }
        public override SuccessResult<AbstractCarriers> Carriers_EmailVerify(int Id)
        {
            return this.abstractCarriersDao.Carriers_EmailVerify(Id);
        }
        public override SuccessResult<AbstractCarriers> Carriers_MobileVerify(int Id)
        {
            return this.abstractCarriersDao.Carriers_MobileVerify(Id);
        }

        public override bool Carriers_Logout(int Id)    
        {
            return this.abstractCarriersDao.Carriers_Logout(Id);
        }

        public override bool Carriers_ResetPassword(int Id, string Password)
        {
            return this.abstractCarriersDao.Carriers_ResetPassword(Id,Password);
        }
    }
}
