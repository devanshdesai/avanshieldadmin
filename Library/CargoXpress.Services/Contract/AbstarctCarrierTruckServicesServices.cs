﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractCarrierTruckServicesServices
    {
        public abstract SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices_Upsert(AbstractCarrierTruckServices abstractCarrierTruckServices);

        public abstract SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices_ById(int Id);

        public abstract PagedList<AbstractCarrierTruckServices> CarrierTruckServices_All(PageParam pageParam, int CarrierId,  AbstractCarrierTruckServices abstractCarrierTruckServices = null);

        public abstract bool CarrierTruckServices_Delete(int CarrierId);
    }
}
