﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractTruckDocumentsService
    {
        public abstract SuccessResult<AbstractTruckDocuments> TruckDocuments_Upsert(AbstractTruckDocuments abstractTruckDocuments);

        public abstract PagedList<AbstractTruckDocuments> TruckDocuments_All(PageParam pageParam, int TruckId);

        public abstract SuccessResult<AbstractTruckDocuments> TruckDocuments_ById(int Id);
    }
}
