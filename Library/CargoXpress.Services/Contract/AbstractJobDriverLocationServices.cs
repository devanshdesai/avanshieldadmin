﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobDriverLocationServices
    {
        public abstract SuccessResult<AbstractJobDriverLocation> JobDriverLocation_Upsert(AbstractJobDriverLocation abstractJobDriverLocation);

        public abstract SuccessResult<AbstractJobDriverLocation> JobDriverLocations_ById(int Id);

        public abstract PagedList<AbstractJobDriverLocation> JobDriverLocation_ByJobId(PageParam pageParam, int JobId, int DriverId);

    }
}
