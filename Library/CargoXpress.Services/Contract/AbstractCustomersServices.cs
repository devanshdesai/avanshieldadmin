﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractCustomersServices
    {
        public abstract SuccessResult<AbstractCustomers> Customers_Upsert(AbstractCustomers abstractCustomers);
        public abstract SuccessResult<AbstractCustomers> Customers_ById(int Id);
        public abstract PagedList<AbstractCustomers> Customers_All(PageParam pageParam, string Search);
        public abstract SuccessResult<AbstractCustomers> Customers_ActInAct(int Id);

    }
}
