﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobBidsServices
    {
        public abstract SuccessResult<AbstractJobBids> JobBids_Upsert(AbstractJobBids abstractJobBids);

        public abstract SuccessResult<AbstractJobBids> JobBids_ById(int Id);

        public abstract PagedList<AbstractJobBids> JobBids_ByJobId(PageParam pageParam, int JobId, int CarrierId);

        public abstract SuccessResult<AbstractJobBids> JobBidStatus_Update(AbstractJobBids abstractJobBids);
    }
}
