﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractShiperDocumentsServices
    {
        public abstract SuccessResult<AbstractShiperDocuments> ShipperDocuments_Upsert(AbstractShiperDocuments abstractShiperDocuments);

        public abstract PagedList<AbstractShiperDocuments> ShiperDocuments_All(PageParam pageParam, int ShipperId, AbstractShiperDocuments abstractShiperDocuments = null);

        public abstract SuccessResult<AbstractShiperDocuments> ShiperDocuments_ById(int Id);
    }
}
