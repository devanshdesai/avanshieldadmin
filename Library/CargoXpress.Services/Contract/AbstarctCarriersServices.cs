﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractCarriersServices
    {
        public abstract SuccessResult<AbstractCarriers> Carriers_Upsert(AbstractCarriers abstractCarriers);

        public abstract SuccessResult<AbstractCarriers> Carriers_ById(int Id);

        public abstract PagedList<AbstractCarriers> Carriers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive);
    
        public abstract SuccessResult<AbstractCarriers> Carriers_Login(AbstractCarriers abstractCarriers);

        public abstract SuccessResult<AbstractCarriers> Carriers_ActInact(int Id);

        public abstract SuccessResult<AbstractCarriers> Carriers_Approve(int Id);

        public abstract SuccessResult<AbstractCarriers> Carriers_ChangePassword(AbstractCarriers abstractCarriers);

        public abstract SuccessResult<AbstractCarriers> Carriers_EmailVerify(int Id);

        public abstract SuccessResult<AbstractCarriers> Carriers_MobileVerify(int Id);

        public abstract bool Carriers_Logout(int Id);

        public abstract bool Carriers_ResetPassword(int Id,string Password);

        public abstract SuccessResult<AbstractCarriers> Carriers_ProfileUpdate(AbstractCarriers abstractCarriers);
    }
}
