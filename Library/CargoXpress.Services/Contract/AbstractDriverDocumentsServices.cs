﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractDriverDocumentsServices
    {
        public abstract SuccessResult<AbstractDriverDocuments> DriverDocuments_Upsert(AbstractDriverDocuments abstractDriverDocuments);

        public abstract SuccessResult<AbstractDriverDocuments> DriverDocuments_ById(int Id);

        public abstract PagedList<AbstractDriverDocuments> DriverDocuments_All(PageParam pageParam, int DriverId);
    

    }
}
