﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractUserTypeServices
    {

        public abstract PagedList<AbstractUserType> UserType_All(PageParam pageParam, string search);
    
        
    }
}
