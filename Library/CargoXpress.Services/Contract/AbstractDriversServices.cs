﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractDriversService
    {
        public abstract SuccessResult<AbstractDrivers> Drivers_Upsert(AbstractDrivers abstractDrivers);

        public abstract SuccessResult<AbstractDrivers> Drivers_ById(int Id);

        public abstract PagedList<AbstractDrivers> Drivers_All(PageParam pageParam, string search, int CarrierId);
        // string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive, string DriverLicense, string DriverID, 
        public abstract SuccessResult<AbstractDrivers> Drivers_Login(AbstractDrivers abstractDrivers);

        public abstract SuccessResult<AbstractDrivers> Drivers_ActInact(int Id);

        public abstract SuccessResult<AbstractDrivers> Drivers_ChangePassword(AbstractDrivers abstractDrivers);

        public abstract SuccessResult<AbstractDrivers> Drivers_EmailVerified(int Id);

        public abstract SuccessResult<AbstractDrivers> Drivers_MobileVerify(int Id);

        public abstract bool Drivers_Logout(int Id);

        public abstract SuccessResult<AbstractDrivers> Drivers_ProfileUpdate(AbstractDrivers abstractDrivers);

    }
}
