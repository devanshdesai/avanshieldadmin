﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractCarrierPreferedDestinationsServices
    {
        public abstract SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_Upsert(AbstractCarrierPreferedDestinations abstractCarrierPreferedDestinations);

        public abstract SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_ById(int Id);

        public abstract PagedList<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_All(PageParam pageParam, int CarrierId);

        public abstract bool CarrierPreferedDestinations_Delete(int CarrierId);
    }
}
