﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractRouteChildServices
    {
        public abstract SuccessResult<AbstractRouteChild> RouteChild_Upsert(AbstractRouteChild abstractRouteChild);

        public abstract SuccessResult<AbstractRouteChild> RouteChild_ById(int Id);

        public abstract PagedList<AbstractRouteChild> RouteChild_ByRouteMasterId(PageParam pageParam, string search, int RouteMasterId);

        public abstract bool RouteChild_Delete(int Id);

    }
}
