﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractLocationTypesServices
    {
        public abstract SuccessResult<AbstractLocationTypes> LocationTypes_Upsert(AbstractLocationTypes abstractLocationTypes);

        public abstract SuccessResult<AbstractLocationTypes> LocationTypes_ById(int Id);

        public abstract PagedList<AbstractLocationTypes> LocationTypes_All(PageParam pageParam, String Search);

    }
}
