﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractTruckTypesServices
    {
        public abstract SuccessResult<AbstractTruckTypes> TruckTypes_Upsert(AbstractTruckTypes abstractTruckTypes);

        public abstract SuccessResult<AbstractTruckTypes> TruckTypes_ById(int Id);

        public abstract PagedList<AbstractTruckTypes> TruckTypes_All(PageParam pageParam, String Search);

    }
}
