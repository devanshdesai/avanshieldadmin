﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractRouteMasterServices
    {
        public abstract SuccessResult<AbstractRouteMaster> RouteMaster_Upsert(AbstractRouteMaster abstractRouteMaster);

        public abstract SuccessResult<AbstractRouteMaster> RouteMaster_ById(int Id);

        public abstract PagedList<AbstractRouteMaster> RouteMaster_ByJobId(PageParam pageParam, string search, int JobId);

        public abstract bool RouteMaster_Delete(int Id);

    }
}
