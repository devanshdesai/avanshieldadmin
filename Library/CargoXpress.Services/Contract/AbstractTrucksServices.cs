﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractTrucksService
    {
        public abstract SuccessResult<AbstractTrucks> Trucks_Upsert(AbstractTrucks abstractTrucks);

        public abstract PagedList<AbstractTrucks> Trucks_All(PageParam pageParam, string search, string PlateNumber, string CompanyName, int TruckTypeId, int LoadCapacity, int CarrierId);

        public abstract SuccessResult<AbstractTrucks> Trucks_ById(int Id);

        public abstract SuccessResult<AbstractTrucks> Trucks_ActInact(int Id);

        public abstract SuccessResult<AbstractTrucks> Trucks_JobActInact(int Id);
    }
}
