﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractCustomerPlansServices
    {
        public abstract PagedList<AbstractCustomerPlans> CustomerPlans_ByCustomerId(PageParam pageParam, string search,int CustomerId);

        public abstract SuccessResult<AbstractCustomerPlans> CustomerPlans_Upsert(AbstractCustomerPlans abstractCustomerPlans);
    }
}
