﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractNotificationServices
    {
        public abstract SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractNotification);

        public abstract PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search, int UserId, int UserType);

    }
}
