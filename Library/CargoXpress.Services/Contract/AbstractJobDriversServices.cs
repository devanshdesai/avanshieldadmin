﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobDriversServices
    {
        public abstract SuccessResult<AbstractJobDrivers> JobDrivers_Upsert(AbstractJobDrivers abstractJobDrivers);
        public abstract SuccessResult<AbstractJobDrivers> JobDrivers_ById(int Id);
        public abstract PagedList<AbstractJobDrivers> JobDrivers_ByJobId(PageParam pageParam, int JobId);
        public abstract bool JobDrivers_Delete(int Id);
        public abstract SuccessResult<AbstractJobDrivers> JobDrivers_IsJobCompleted(int Id);

    }
}
