﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Services.Contract
{
    public abstract class AbstractJobBillOfLoadingServices
    {
        public abstract SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading_Upsert(AbstractJobBillOfLoading abstractJobBillOfLoading);

        public abstract PagedList<AbstractJobBillOfLoading> JobBillOfLoading_All(PageParam pageParam, string search, int JobId, string BillOfLoading);

        public abstract SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading_Delete(int Id);

    }
}
