﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CargoXpress.Services
{
    using Autofac;    
    using Data;
    using CargoXpress.Services.Contract;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterModule<DataModule>();

            
            builder.RegisterType<V1.AdminServices>().As<AbstractAdminServices>().InstancePerDependency();
            builder.RegisterType<V1.AdminUserServices>().As<AbstractAdminUserServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomersServices>().As<AbstractCustomersServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterPlansServices>().As<AbstractMasterPlansServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerTransactionsServices>().As<AbstractCustomerTransactionsServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerPlansServices>().As<AbstractCustomerPlansServices>().InstancePerDependency();
            builder.RegisterType<V1.CustomerActivityServices>().As<AbstractCustomerActivityServices>().InstancePerDependency();
            base.Load(builder);
        }
    }
}
