﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractPaymentMasterDao
    {
        public abstract SuccessResult<AbstractPaymentMaster> PaymentMaster_Upsert(AbstractPaymentMaster abstractPaymentMaster);

        public abstract SuccessResult<AbstractPaymentMaster> PaymentMaster_ById(int Id);

        public abstract PagedList<AbstractPaymentMaster> PaymentMaster_All(PageParam pageParam, string search, int JobId, int PaidByUserType = 0, int PaidToUserType = 0);

        public abstract SuccessResult<AbstractPaymentMaster> PaymentMaster_Delete(int Id);

        public abstract SuccessResult<AbstractPaymentMaster> PaymentMaster_RecievedNotRecieved(int Id);

    }
}
