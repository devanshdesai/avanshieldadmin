﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractAdminUserDao
    {
        public abstract SuccessResult<AbstractAdminUser> AdminUser_ById(int Id);
        public abstract bool AdminUser_ChangePassword(AbstractAdminUser abstractAdminUser);
        public abstract SuccessResult<AbstractAdminUser> AdminUser_LogIn(AbstractAdminUser abstractAdminUser);
        public abstract bool AdminUser_LogOut(int Id);


    }
}
