﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractCarrierDocumentsDao
    {
        public abstract SuccessResult<AbstractCarrierDocuments> CarrierDocuments_Upsert(AbstractCarrierDocuments abstractCarrierDocuments);

        public abstract SuccessResult<AbstractCarrierDocuments> CarrierDocuments_ById(int Id);

        public abstract PagedList<AbstractCarrierDocuments> CarrierDocuments_All(PageParam pageParam, int CarrierId);


    }
}
