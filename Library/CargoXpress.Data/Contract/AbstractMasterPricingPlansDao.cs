﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractMasterPricingPlansDao
    {
        public abstract SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_Upsert(AbstractMasterPricingPlans abstractMasterPricingPlans);

        public abstract SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_ById(int Id);

        public abstract PagedList<AbstractMasterPricingPlans> MasterPricingPlans_All(PageParam pageParam, string search = "", int UserType = 0);

        public abstract SuccessResult<AbstractMasterPricingPlans> MasterPricingPlans_ActInact(int Id);

        
    }
}
