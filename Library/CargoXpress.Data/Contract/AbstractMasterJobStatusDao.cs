﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractMasterJobStatusDao
    {
         public abstract PagedList<AbstractMasterJobStatus> MasterJobStatus_All(PageParam pageParam, String search, int JobStatusTypeId);

    }
}
