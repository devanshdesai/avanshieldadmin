﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractShipperCardsDao
    {
        public abstract SuccessResult<AbstractShipperCards> ShipperCards_Upsert(AbstractShipperCards abstractShipperCards);

        public abstract SuccessResult<AbstractShipperCards> ShipperCards_ById(int Id);

        public abstract PagedList<AbstractShipperCards> ShipperCards_ByShipperId(PageParam pageParam, string search, int ShipperId);

        public abstract SuccessResult<AbstractShipperCards> ShipperCards_Delete(AbstractShipperCards abstractShipperCards);

        public abstract SuccessResult<AbstractShipperCards> ShipperCards_IsDefault(int Id);

    }
}
