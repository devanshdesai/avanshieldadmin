﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractShipperAddressesDao
    {
        public abstract SuccessResult<AbstractShipperAddresses> ShipperAddresses_Upsert(AbstractShipperAddresses abstractShipperAddresses);

        public abstract PagedList<AbstractShipperAddresses> ShipperAddresses_All(PageParam pageParam, int ShipperId, AbstractShipperAddresses abstractShipperAddresses = null);

        public abstract SuccessResult<AbstractShipperAddresses> ShipperAddresses_ById(int Id);
    }
}
