﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractCustomerTransactionsDao
    {
        public abstract SuccessResult<AbstractCustomerTransactions> CustomerTransactions_Upsert(AbstractCustomerTransactions abstractCustomerTransactions);
        public abstract SuccessResult<AbstractCustomerTransactions> CustomerTransactions_ById(int Id);
        public abstract PagedList<AbstractCustomerTransactions> CustomerTransactions_All(PageParam pageParam, string Search , int CustomerId, int PlanId);
        public abstract SuccessResult<AbstractCustomerTransactions> CustomerTransactions_IsReedemed(int Id);
    }
}
