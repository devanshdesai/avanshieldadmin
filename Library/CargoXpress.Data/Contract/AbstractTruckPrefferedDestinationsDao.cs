﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractTruckPrefferedDestinationsDao
    {
        public abstract SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_Upsert(AbstractTruckPrefferedDestinations abstractTruckPrefferedDestinations);

        public abstract SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_ById(int Id);

        public abstract PagedList<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_All(PageParam pageParam, int TruckId);


    }
}
