﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractCarrierPricingPlanDao
    {
        public abstract SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan_Upsert(AbstractCarrierPricingPlan abstractCarrierPricingPlan);

        public abstract PagedList<AbstractCarrierPricingPlan> CarrierPricingPlan_ByCarrierId(PageParam pageParam, string search, int CarrierId, int UserType);

        public abstract SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan_Delete(int Id);

    }
}
