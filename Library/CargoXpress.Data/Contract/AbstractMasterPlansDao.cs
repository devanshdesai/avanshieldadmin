﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractMasterPlansDao
    {
        public abstract SuccessResult<AbstractMasterPlans> MasterPlans_ById(int Id);
        public abstract SuccessResult<AbstractMasterPlans> MasterPlans_Upsert(AbstractMasterPlans abstractMasterPlans);
        public abstract PagedList<AbstractMasterPlans> MasterPlans_All(PageParam pageParam, string Search);

    }
}
