﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{   //add by krinal
    public abstract class AbstractMasterDriverCommentsDao
    {
         public abstract PagedList<AbstractMasterDriverComments> MasterDriverComments_All(PageParam pageParam);

    }
}
