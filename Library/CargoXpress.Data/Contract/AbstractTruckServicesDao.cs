﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractTruckServicesDao
    {
        public abstract SuccessResult<AbstractTruckServices> TruckServices_Upsert(AbstractTruckServices abstractTruckServices);

        public abstract SuccessResult<AbstractTruckServices> TruckServices_ById(int Id);

        public abstract PagedList<AbstractTruckServices> TruckServices_All(PageParam pageParam, String Search, int ParentId);

        public abstract PagedList<AbstractTruckServices> TruckServices_ByParentId(PageParam pageParam, String Search);

    }
}
