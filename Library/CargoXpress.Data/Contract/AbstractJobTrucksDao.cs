﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Entities.Contract;

namespace CargoXpress.Data.Contract
{
    public abstract class AbstractJobTrucksDao
    {
        public abstract SuccessResult<AbstractJobTrucks> JobTrucks_Upsert(AbstractJobTrucks abstractJobTrucks);
        public abstract SuccessResult<AbstractJobTrucks> JobTrucks_ById(int Id);
        public abstract PagedList<AbstractJobTrucks> JobTrucks_ByJobId(PageParam pageParam, int JobId);
        public abstract bool JobTrucks_Delete(int Id);
        public abstract SuccessResult<AbstractJobTrucks> JobTrucks_IsJobCompleted(int Id);

    }
}
