﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
//using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CustomerTransactionsDao : AbstractCustomerTransactionsDao
    {

        public override SuccessResult<AbstractCustomerTransactions> CustomerTransactions_Upsert(AbstractCustomerTransactions abstractCustomerTransactions)
        {
            SuccessResult<AbstractCustomerTransactions> CustomerTransactions = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCustomerTransactions.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractCustomerTransactions.CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlanId", abstractCustomerTransactions.PlanId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NoOfMonths", abstractCustomerTransactions.NoOfMonths, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Amount", abstractCustomerTransactions.Amount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@StartDate", abstractCustomerTransactions.StartDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@EndDate", abstractCustomerTransactions.EndDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@LicenceKey", abstractCustomerTransactions.LicenceKey, dbType: DbType.String, direction: ParameterDirection.Input);
            


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerTransactions_Upsert, param, commandType: CommandType.StoredProcedure);
                CustomerTransactions = task.Read<SuccessResult<AbstractCustomerTransactions>>().SingleOrDefault();
                CustomerTransactions.Item = task.Read<CustomerTransactions>().SingleOrDefault();
            }

            return CustomerTransactions;
        }

        public override SuccessResult<AbstractCustomerTransactions> CustomerTransactions_ById(int Id)
        {
            SuccessResult<AbstractCustomerTransactions> CustomerTransactions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerTransactions_ById, param, commandType: CommandType.StoredProcedure);
                CustomerTransactions = task.Read<SuccessResult<AbstractCustomerTransactions>>().SingleOrDefault();
                CustomerTransactions.Item = task.Read<CustomerTransactions>().SingleOrDefault();
            }

            return CustomerTransactions;
        }
        public override PagedList<AbstractCustomerTransactions> CustomerTransactions_All(PageParam pageParam, string search = null, int CustomerId = 0, int PlanId = 0)
        {
            PagedList<AbstractCustomerTransactions> CustomerTransactions = new PagedList<AbstractCustomerTransactions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlanId", PlanId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerTransactions_All, param, commandType: CommandType.StoredProcedure);
                CustomerTransactions.Values.AddRange(task.Read<CustomerTransactions>());
                CustomerTransactions.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CustomerTransactions;
        }
        public override SuccessResult<AbstractCustomerTransactions> CustomerTransactions_IsReedemed(int Id)
        {
            SuccessResult<AbstractCustomerTransactions> CustomerTransactions = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerTransactions_IsReedemed, param, commandType: CommandType.StoredProcedure);
                CustomerTransactions = task.Read<SuccessResult<AbstractCustomerTransactions>>().SingleOrDefault();
                CustomerTransactions.Item = task.Read<CustomerTransactions>().SingleOrDefault();
            }

            return CustomerTransactions;
        }


    }

}
