﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class PaymentMasterDao : AbstractPaymentMasterDao
    {
        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_Upsert(AbstractPaymentMaster abstractPaymentMaster)
        {
            SuccessResult<AbstractPaymentMaster> PaymentMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractPaymentMaster.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractPaymentMaster.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PaidByUserType", abstractPaymentMaster.PaidByUserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Amount", abstractPaymentMaster.Amount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Type", abstractPaymentMaster.Type, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaidToUserType", abstractPaymentMaster.PaidToUserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Note", abstractPaymentMaster.Note, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PaymentDate", abstractPaymentMaster.PaymentDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Url", abstractPaymentMaster.Url, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractPaymentMaster.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractPaymentMaster.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@IsRecieved", abstractPaymentMaster.IsRecieved, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                PaymentMaster = task.Read<SuccessResult<AbstractPaymentMaster>>().SingleOrDefault();
                PaymentMaster.Item = task.Read<PaymentMaster>().SingleOrDefault();
            }

            return PaymentMaster;
        }



        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_ById(int Id)
        {
            SuccessResult<AbstractPaymentMaster> PaymentMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentMaster_ById, param, commandType: CommandType.StoredProcedure);
                PaymentMaster = task.Read<SuccessResult<AbstractPaymentMaster>>().SingleOrDefault();
                PaymentMaster.Item = task.Read<PaymentMaster>().SingleOrDefault();
            }

            return PaymentMaster;
        }

        public override PagedList<AbstractPaymentMaster> PaymentMaster_All(PageParam pageParam, string search, int JobId, int PaidByUserType = 0, int PaidToUserType = 0)
        {
            PagedList<AbstractPaymentMaster> PaymentMaster = new PagedList<AbstractPaymentMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PaidByUserType", PaidByUserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PaidToUserType", PaidToUserType, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentMaster_All, param, commandType: CommandType.StoredProcedure);
                PaymentMaster.Values.AddRange(task.Read<PaymentMaster>());
                PaymentMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return PaymentMaster;
        }

        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_Delete(int Id)
        {
            SuccessResult<AbstractPaymentMaster> PaymentMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentMaster_Delete, param, commandType: CommandType.StoredProcedure);
                PaymentMaster = task.Read<SuccessResult<AbstractPaymentMaster>>().SingleOrDefault();
                //PaymentMaster.Item = task.Read<PaymentMaster>().SingleOrDefault();
            }
            return PaymentMaster;
        }

        public override SuccessResult<AbstractPaymentMaster> PaymentMaster_RecievedNotRecieved(int Id)
        {
            SuccessResult<AbstractPaymentMaster> PaymentMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.PaymentMaster_RecievedNotRecieved, param, commandType: CommandType.StoredProcedure);
                PaymentMaster = task.Read<SuccessResult<AbstractPaymentMaster>>().SingleOrDefault();
                PaymentMaster.Item = task.Read<PaymentMaster>().SingleOrDefault();
            }

            return PaymentMaster;
        }

    }

}
