﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class DriverDocumentsDao : AbstractDriverDocumentsDao
    {
        public override SuccessResult<AbstractDriverDocuments> DriverDocuments_Upsert(AbstractDriverDocuments abstractDriverDocuments)
        {
            SuccessResult<AbstractDriverDocuments> DriverDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDriverDocuments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractDriverDocuments.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DocumentType", abstractDriverDocuments.DocumentType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Url", abstractDriverDocuments.Url, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDriverDocuments.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractDriverDocuments.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DriverDocuments_Upsert, param, commandType: CommandType.StoredProcedure);
                DriverDocuments = task.Read<SuccessResult<AbstractDriverDocuments>>().SingleOrDefault();
                DriverDocuments.Item = task.Read<DriverDocuments>().SingleOrDefault();
            }

            return DriverDocuments;
        }

        public override SuccessResult<AbstractDriverDocuments> DriverDocuments_ById(int Id)
        {
            SuccessResult<AbstractDriverDocuments> DriverDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DriverDocuments_ById, param, commandType: CommandType.StoredProcedure);
                DriverDocuments = task.Read<SuccessResult<AbstractDriverDocuments>>().SingleOrDefault();
                DriverDocuments.Item = task.Read<DriverDocuments>().SingleOrDefault();
            }

            return DriverDocuments;
        }

        public override PagedList<AbstractDriverDocuments> DriverDocuments_All(PageParam pageParam, int DriverId)
        {
            PagedList<AbstractDriverDocuments> DriverDocuments = new PagedList<AbstractDriverDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DriverDocuments_All, param, commandType: CommandType.StoredProcedure);
                DriverDocuments.Values.AddRange(task.Read<DriverDocuments>());
                DriverDocuments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DriverDocuments;
            }

           

    }

    }
