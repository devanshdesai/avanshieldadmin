﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class RouteMasterDao : AbstractRouteMasterDao
    {
        public override SuccessResult<AbstractRouteMaster> RouteMaster_Upsert(AbstractRouteMaster abstractRouteMaster)
        {
            SuccessResult<AbstractRouteMaster> RouteMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractRouteMaster.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractRouteMaster.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PickUpLocationName", abstractRouteMaster.PickUpLocationName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PickUpLat", abstractRouteMaster.PickUpLat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@PickUpLong", abstractRouteMaster.PickUpLong, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DropOffLocationName", abstractRouteMaster.DropOffLocationName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DropOffLat", abstractRouteMaster.DropOffLat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DropOffLong", abstractRouteMaster.DropOffLong, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RouteMaster_Upsert, param, commandType: CommandType.StoredProcedure);
                RouteMaster = task.Read<SuccessResult<AbstractRouteMaster>>().SingleOrDefault();
                RouteMaster.Item = task.Read<RouteMaster>().SingleOrDefault();
            }

            return RouteMaster;
        }

        

        public override SuccessResult<AbstractRouteMaster> RouteMaster_ById(int Id)
        {
            SuccessResult<AbstractRouteMaster> RouteMaster = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RouteMaster_ById, param, commandType: CommandType.StoredProcedure);
                RouteMaster = task.Read<SuccessResult<AbstractRouteMaster>>().SingleOrDefault();
                RouteMaster.Item = task.Read<RouteMaster>().SingleOrDefault();
            }

            return RouteMaster;
        }

        public override PagedList<AbstractRouteMaster> RouteMaster_ByJobId(PageParam pageParam, string search,int JobId)
        {
            PagedList<AbstractRouteMaster> RouteMaster = new PagedList<AbstractRouteMaster>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RouteMaster_ByJobId, param, commandType: CommandType.StoredProcedure);
                RouteMaster.Values.AddRange(task.Read<RouteMaster>());
                RouteMaster.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return RouteMaster;
            }


        public override bool RouteMaster_Delete(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.RouteMaster_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        

    }

    }
