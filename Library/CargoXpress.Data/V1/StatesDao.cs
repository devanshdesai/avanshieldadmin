﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class StatesDao : AbstractStatesDao
    {
      
        public override PagedList<AbstractStates> States_ByCountryId(PageParam pageParam,String Search, int CountryId)
        {
            

            PagedList<AbstractStates> States = new PagedList<AbstractStates>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.States_ByCountryId, param, commandType: CommandType.StoredProcedure);
                States.Values.AddRange(task.Read<States>());
                States.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return States;
        }

    }

    }
