﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CustomerPlansDao : AbstractCustomerPlansDao
    {
        
        public override PagedList<AbstractCustomerPlans> CustomerPlans_ByCustomerId(PageParam pageParam, string search,int CustomerId)
        {
            PagedList<AbstractCustomerPlans> CustomerPlans = new PagedList<AbstractCustomerPlans>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerPlans_ByCustomerId, param, commandType: CommandType.StoredProcedure);
                CustomerPlans.Values.AddRange(task.Read<CustomerPlans>());
                CustomerPlans.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CustomerPlans;
        }

        public override SuccessResult<AbstractCustomerPlans> CustomerPlans_Upsert(AbstractCustomerPlans abstractCustomerPlans)
        {
            SuccessResult<AbstractCustomerPlans> CustomerPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCustomerPlans.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CustomerId", abstractCustomerPlans.CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlanId", abstractCustomerPlans.PlanId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@LiscenceKey", abstractCustomerPlans.LiscenceKey, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StartDate", abstractCustomerPlans.StartDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@EndDate", abstractCustomerPlans.EndDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            //param.Add("@RedeemedDate", abstractCustomerPlans.RedeemedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@PlanName", abstractCustomerPlans.PlanName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Amount", abstractCustomerPlans.Amount, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NoOfMonths", abstractCustomerPlans.NoOfMonths, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsPaymentDone", abstractCustomerPlans.IsPaymentDone, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            //param.Add("@TransactionDate", abstractCustomerPlans.TransactionDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@TransactionNumber", abstractCustomerPlans.TransactionNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Note", abstractCustomerPlans.Note, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerPlans_Upsert, param, commandType: CommandType.StoredProcedure);
                CustomerPlans = task.Read<SuccessResult<AbstractCustomerPlans>>().SingleOrDefault();
                CustomerPlans.Item = task.Read<CustomerPlans>().SingleOrDefault();
            }

            return CustomerPlans;
        }

    }
}
