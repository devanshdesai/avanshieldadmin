﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CustomerActivityDao : AbstractCustomerActivityDao
    {
        
        public override PagedList<AbstractCustomerActivity> CustomerActivity_ByCustomerId(PageParam pageParam, string search,int CustomerId)
        {
            PagedList<AbstractCustomerActivity> CustomerActivity = new PagedList<AbstractCustomerActivity>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CustomerActivity_ByCustomerId, param, commandType: CommandType.StoredProcedure);
                CustomerActivity.Values.AddRange(task.Read<CustomerActivity>());
                CustomerActivity.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CustomerActivity;
            }
        
    }
}
