﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class RouteChildDao : AbstractRouteChildDao
    {
        public override SuccessResult<AbstractRouteChild> RouteChild_Upsert(AbstractRouteChild abstractRouteChild)
        {
            SuccessResult<AbstractRouteChild> RouteChild = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractRouteChild.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RouteMasterId", abstractRouteChild.RouteMasterId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@LocationName", abstractRouteChild.LocationName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Lat", abstractRouteChild.Lat, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Long", abstractRouteChild.Long, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Distance", abstractRouteChild.Distance, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RouteChild_Upsert, param, commandType: CommandType.StoredProcedure);
                RouteChild = task.Read<SuccessResult<AbstractRouteChild>>().SingleOrDefault();
                RouteChild.Item = task.Read<RouteChild>().SingleOrDefault();
            }

            return RouteChild;
        }

        

        public override SuccessResult<AbstractRouteChild> RouteChild_ById(int Id)
        {
            SuccessResult<AbstractRouteChild> RouteChild = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RouteChild_ById, param, commandType: CommandType.StoredProcedure);
                RouteChild = task.Read<SuccessResult<AbstractRouteChild>>().SingleOrDefault();
                RouteChild.Item = task.Read<RouteChild>().SingleOrDefault();
            }

            return RouteChild;
        }

        public override PagedList<AbstractRouteChild> RouteChild_ByRouteMasterId(PageParam pageParam, string search,int RouteMasterId)
        {
            PagedList<AbstractRouteChild> RouteChild = new PagedList<AbstractRouteChild>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RouteMasterId", RouteMasterId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.RouteChild_ByRouteMasterId, param, commandType: CommandType.StoredProcedure);
                RouteChild.Values.AddRange(task.Read<RouteChild>());
                RouteChild.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return RouteChild;
            }


        public override bool RouteChild_Delete(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.RouteChild_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        

    }

    }
