﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
//using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CustomersDao : AbstractCustomersDao
    {

        public override SuccessResult<AbstractCustomers> Customers_Upsert(AbstractCustomers abstractCustomers)
        {
            SuccessResult<AbstractCustomers> Customers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCustomers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractCustomers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractCustomers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", abstractCustomers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Mobile", abstractCustomers.Mobile, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerKey", abstractCustomers.CustomerKey, dbType: DbType.String, direction: ParameterDirection.Input);          


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customers_Upsert, param, commandType: CommandType.StoredProcedure);
                Customers = task.Read<SuccessResult<AbstractCustomers>>().SingleOrDefault();
                Customers.Item = task.Read<Customers>().SingleOrDefault();
            }

            return Customers;
        }

        public override SuccessResult<AbstractCustomers> Customers_ById(int Id)
        {
            SuccessResult<AbstractCustomers> Customers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customers_ById, param, commandType: CommandType.StoredProcedure);
                Customers = task.Read<SuccessResult<AbstractCustomers>>().SingleOrDefault();
                Customers.Item = task.Read<Customers>().SingleOrDefault();
            }

            return Customers;
        }
        public override PagedList<AbstractCustomers> Customers_All(PageParam pageParam, string search)
        {
            PagedList<AbstractCustomers> Customers = new PagedList<AbstractCustomers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customers_All, param, commandType: CommandType.StoredProcedure);
                Customers.Values.AddRange(task.Read<Customers>());
                Customers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Customers;
        }
        public override SuccessResult<AbstractCustomers> Customers_ActInAct(int Id)
        {
            SuccessResult<AbstractCustomers> Customers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Customers_ActInAct, param, commandType: CommandType.StoredProcedure);
                Customers = task.Read<SuccessResult<AbstractCustomers>>().SingleOrDefault();
                Customers.Item = task.Read<Customers>().SingleOrDefault();
            }

            return Customers;
        }


    }

}
