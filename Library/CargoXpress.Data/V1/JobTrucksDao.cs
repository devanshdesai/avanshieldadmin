﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
//using GlodCleaning.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobTrucksDao : AbstractJobTrucksDao
    {


        public override SuccessResult<AbstractJobTrucks> JobTrucks_Upsert(AbstractJobTrucks abstractJobTrucks)
        {
            SuccessResult<AbstractJobTrucks> JobTrucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobTrucks.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobTrucks.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", abstractJobTrucks.TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTrucks_Upsert, param, commandType: CommandType.StoredProcedure);
                JobTrucks = task.Read<SuccessResult<AbstractJobTrucks>>().SingleOrDefault();
                JobTrucks.Item = task.Read<JobTrucks>().SingleOrDefault();
            }

            return JobTrucks;
        }

        public override PagedList<AbstractJobTrucks> JobTrucks_ByJobId(PageParam pageParam, int Job)
        {
            PagedList<AbstractJobTrucks> JobTrucks = new PagedList<AbstractJobTrucks>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Job", Job, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTrucks_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobTrucks.Values.AddRange(task.Read<JobTrucks>());
                JobTrucks.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobTrucks;
        }

        public override SuccessResult<AbstractJobTrucks> JobTrucks_ById(int Id)
        {
            SuccessResult<AbstractJobTrucks> JobTrucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTrucks_ById, param, commandType: CommandType.StoredProcedure);
                JobTrucks = task.Read<SuccessResult<AbstractJobTrucks>>().SingleOrDefault();
                JobTrucks.Item = task.Read<JobTrucks>().SingleOrDefault();
            }

            return JobTrucks;
        }

        public override bool JobTrucks_Delete(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@IdId", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.JobTrucks_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override SuccessResult<AbstractJobTrucks> JobTrucks_IsJobCompleted(int Id)
        {
            SuccessResult<AbstractJobTrucks> JobTrucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobTrucks_IsJobCompleted, param, commandType: CommandType.StoredProcedure);
                JobTrucks = task.Read<SuccessResult<AbstractJobTrucks>>().SingleOrDefault();
                JobTrucks.Item = task.Read<JobTrucks>().SingleOrDefault();
            }

            return JobTrucks;
        }




    }

}
