﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobCommentsDao : AbstractJobCommentsDao
    {

        public override SuccessResult<AbstractJobComments> JobComments_Upsert(AbstractJobComments abstractJobComments)
        {
            SuccessResult<AbstractJobComments> JobComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobComments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobComments.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Comment", abstractJobComments.Comment, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CommentDate", abstractJobComments.CommentDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractJobComments.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractJobComments.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobComments_Upsert, param, commandType: CommandType.StoredProcedure);
                JobComments = task.Read<SuccessResult<AbstractJobComments>>().SingleOrDefault();
                JobComments.Item = task.Read<JobComments>().SingleOrDefault();
            }

            return JobComments;
        }

        public override SuccessResult<AbstractJobComments> JobComments_ById(int Id)
        {
            SuccessResult<AbstractJobComments> JobComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobComments_ById, param, commandType: CommandType.StoredProcedure);
                JobComments = task.Read<SuccessResult<AbstractJobComments>>().SingleOrDefault();
                JobComments.Item = task.Read<JobComments>().SingleOrDefault();
            }

            return JobComments;
        }

        public override PagedList<AbstractJobComments> JobComments_ByJobId(PageParam pageParam,int JobId)
        {
            

            PagedList<AbstractJobComments> JobComments = new PagedList<AbstractJobComments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobComments_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobComments.Values.AddRange(task.Read<JobComments>());
                JobComments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobComments;
        }

    }

    }
