﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CarrierPreferedDestinationsDao : AbstractCarrierPreferedDestinationsDao
    {
        public override SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_Upsert(AbstractCarrierPreferedDestinations abstractCarrierPreferedDestinations)
        {
            SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarrierPreferedDestinations.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractCarrierPreferedDestinations.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractCarrierPreferedDestinations.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractCarrierPreferedDestinations.StateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@City", abstractCarrierPreferedDestinations.City, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@CreatedBy", abstractCarrierPreferedDestinations.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@UpdatedBy", abstractCarrierPreferedDestinations.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierPreferedDestinations_Upsert, param, commandType: CommandType.StoredProcedure);
                CarrierPreferedDestinations = task.Read<SuccessResult<AbstractCarrierPreferedDestinations>>().SingleOrDefault();
                CarrierPreferedDestinations.Item = task.Read<CarrierPreferedDestinations>().SingleOrDefault();
            }

            return CarrierPreferedDestinations;
        }

        public override SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_ById(int Id)
        {
            SuccessResult<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierPreferedDestinations_ById, param, commandType: CommandType.StoredProcedure);
                CarrierPreferedDestinations = task.Read<SuccessResult<AbstractCarrierPreferedDestinations>>().SingleOrDefault();
                CarrierPreferedDestinations.Item = task.Read<CarrierPreferedDestinations>().SingleOrDefault();
            }

            return CarrierPreferedDestinations;
        }

        public override PagedList<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations_All(PageParam pageParam, int CarrierId)
        {
            PagedList<AbstractCarrierPreferedDestinations> CarrierPreferedDestinations = new PagedList<AbstractCarrierPreferedDestinations>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierPreferedDestinations_All, param, commandType: CommandType.StoredProcedure);
                CarrierPreferedDestinations.Values.AddRange(task.Read<CarrierPreferedDestinations>());
                CarrierPreferedDestinations.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CarrierPreferedDestinations;
            }

        public override bool CarrierPreferedDestinations_Delete(int CarrierId)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.CarrierPreferedDestinations_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

    }

    }
