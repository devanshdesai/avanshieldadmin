﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class TrucksDao : AbstractTrucksDao
    {
        public override SuccessResult<AbstractTrucks> Trucks_Upsert(AbstractTrucks abstractTrucks)
        {
            SuccessResult<AbstractTrucks> Trucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTrucks.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PlateNumber", abstractTrucks.PlateNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractTrucks.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckTypeId", abstractTrucks.TruckTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Logbook", abstractTrucks.Logbook, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractTrucks.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Insurance", abstractTrucks.Insurance, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RfidTag", abstractTrucks.RfidTag, dbType: DbType.String, direction: ParameterDirection.Input); 
            param.Add("@LoadCapacity", abstractTrucks.LoadCapacity, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractTrucks.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTrucks.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Trucks_Upsert, param, commandType: CommandType.StoredProcedure);
                Trucks = task.Read<SuccessResult<AbstractTrucks>>().SingleOrDefault();
                Trucks.Item = task.Read<Trucks>().SingleOrDefault();
            }

            return Trucks;
        }

        public override SuccessResult<AbstractTrucks> Trucks_ById(int Id)
        {
            SuccessResult<AbstractTrucks> Trucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Trucks_ById, param, commandType: CommandType.StoredProcedure);
                Trucks = task.Read<SuccessResult<AbstractTrucks>>().SingleOrDefault();
                Trucks.Item = task.Read<Trucks>().SingleOrDefault();
            }

            return Trucks;
        }

        public override PagedList<AbstractTrucks> Trucks_All(PageParam pageParam, string search, string PlateNumber, string CompanyName, int TruckTypeId, int LoadCapacity, int CarrierId)
        {
            PagedList<AbstractTrucks> Carriers = new PagedList<AbstractTrucks>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@PlateNumber", PlateNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@CompanyName", CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);           
            //param.Add("@TruckTypeId", TruckTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@LoadCapacity", LoadCapacity, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Trucks_All, param, commandType: CommandType.StoredProcedure);
                Carriers.Values.AddRange(task.Read<Trucks>());
                Carriers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Carriers;
            }

        public override SuccessResult<AbstractTrucks> Trucks_ActInact(int Id)
           {
            SuccessResult<AbstractTrucks> Trucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Trucks_ActInact, param, commandType: CommandType.StoredProcedure);
                Trucks = task.Read<SuccessResult<AbstractTrucks>>().SingleOrDefault();
                Trucks.Item = task.Read<Trucks>().SingleOrDefault();
            }

            return Trucks;
        }

        public override SuccessResult<AbstractTrucks> Trucks_JobActInact(int Id)
        {
            SuccessResult<AbstractTrucks> Trucks = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Trucks_JobActInact, param, commandType: CommandType.StoredProcedure);
                Trucks = task.Read<SuccessResult<AbstractTrucks>>().SingleOrDefault();
                Trucks.Item = task.Read<Trucks>().SingleOrDefault();
            }

            return Trucks;
        }

    }

    }
