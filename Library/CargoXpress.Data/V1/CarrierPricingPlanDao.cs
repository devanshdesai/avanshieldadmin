﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CarrierPricingPlanDao : AbstractCarrierPricingPlanDao
    {
        public override SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan_Upsert(AbstractCarrierPricingPlan abstractCarrierPricingPlan)
        {
            SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarrierPricingPlan.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractCarrierPricingPlan.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@MasterPricingPlanId", abstractCarrierPricingPlan.MasterPricingPlanId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StartDate", abstractCarrierPricingPlan.StartDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EndDate", abstractCarrierPricingPlan.EndDate, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractCarrierPricingPlan.UserType, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierPricingPlan_Upsert, param, commandType: CommandType.StoredProcedure);
                CarrierPricingPlan = task.Read<SuccessResult<AbstractCarrierPricingPlan>>().SingleOrDefault();
                CarrierPricingPlan.Item = task.Read<CarrierPricingPlan>().SingleOrDefault();
            }

            return CarrierPricingPlan;
        }

        

        public override PagedList<AbstractCarrierPricingPlan> CarrierPricingPlan_ByCarrierId(PageParam pageParam, string search,int CarrierId, int UserType)
        {
            PagedList<AbstractCarrierPricingPlan> CarrierPricingPlan = new PagedList<AbstractCarrierPricingPlan>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierPricingPlan_ByCarrierId, param, commandType: CommandType.StoredProcedure);
                CarrierPricingPlan.Values.AddRange(task.Read<CarrierPricingPlan>());
                CarrierPricingPlan.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CarrierPricingPlan;
            }

            public override SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan_Delete(int Id)
            {
            SuccessResult<AbstractCarrierPricingPlan> CarrierPricingPlan = null;        
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierPricingPlan_Delete, param, commandType: CommandType.StoredProcedure);
                CarrierPricingPlan = task.Read<SuccessResult<AbstractCarrierPricingPlan>>().SingleOrDefault();
                CarrierPricingPlan.Item = task.Read<CarrierPricingPlan>().SingleOrDefault();
            }
            return CarrierPricingPlan;
            }

        
        

    }

    }
