﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CarrierBankDetailsDao : AbstractCarrierBankDetailsDao
    {
        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_Upsert(AbstractCarrierBankDetails abstractCarrierBankDetails)
        {
            SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarrierBankDetails.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractCarrierBankDetails.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AccountNumber", abstractCarrierBankDetails.AccountNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SwiftCode", abstractCarrierBankDetails.SwiftCode, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BankName", abstractCarrierBankDetails.BankName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AccountName", abstractCarrierBankDetails.AccountName, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierBankDetails_Upsert, param, commandType: CommandType.StoredProcedure);
                CarrierBankDetails = task.Read<SuccessResult<AbstractCarrierBankDetails>>().SingleOrDefault();
                CarrierBankDetails.Item = task.Read<CarrierBankDetails>().SingleOrDefault();
            }

            return CarrierBankDetails;
        }

        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_ById(int Id)
        {
            SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierBankDetails_ById, param, commandType: CommandType.StoredProcedure);
                CarrierBankDetails = task.Read<SuccessResult<AbstractCarrierBankDetails>>().SingleOrDefault();
                CarrierBankDetails.Item = task.Read<CarrierBankDetails>().SingleOrDefault();
            }

            return CarrierBankDetails;
        }

        public override PagedList<AbstractCarrierBankDetails> CarrierBankDetails_ByCarrierId(PageParam pageParam, string search,int CarrierId)
        {
            PagedList<AbstractCarrierBankDetails> CarrierBankDetails = new PagedList<AbstractCarrierBankDetails>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierBankDetails_ByCarrierId, param, commandType: CommandType.StoredProcedure);
                CarrierBankDetails.Values.AddRange(task.Read<CarrierBankDetails>());
                CarrierBankDetails.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CarrierBankDetails;
            }

            public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_Delete(AbstractCarrierBankDetails abstractCarrierBankDetails)
            {
            SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails = null;        
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarrierBankDetails.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractCarrierBankDetails.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierBankDetails_Delete, param, commandType: CommandType.StoredProcedure);
                CarrierBankDetails = task.Read<SuccessResult<AbstractCarrierBankDetails>>().SingleOrDefault();
                CarrierBankDetails.Item = task.Read<CarrierBankDetails>().SingleOrDefault();
            }
            return CarrierBankDetails;
            }

        public override SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails_IsDefault(int Id)
           {
            SuccessResult<AbstractCarrierBankDetails> CarrierBankDetails = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierBankDetails_IsDefault, param, commandType: CommandType.StoredProcedure);
                CarrierBankDetails = task.Read<SuccessResult<AbstractCarrierBankDetails>>().SingleOrDefault();
                CarrierBankDetails.Item = task.Read<CarrierBankDetails>().SingleOrDefault();
            }

            return CarrierBankDetails;
            }
        

    }

    }
