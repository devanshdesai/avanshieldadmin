﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
//using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class AdminUserDao : AbstractAdminUserDao
    {

        public override SuccessResult<AbstractAdminUser> AdminUser_ById(int Id)
        {
            SuccessResult<AbstractAdminUser> AdminUser = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminUser_ById, param, commandType: CommandType.StoredProcedure);
                AdminUser = task.Read<SuccessResult<AbstractAdminUser>>().SingleOrDefault();
                AdminUser.Item = task.Read<AdminUser>().SingleOrDefault();
            }

            return AdminUser;
        }


        public override bool AdminUser_ChangePassword(AbstractAdminUser abstractAdminUser)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAdminUser.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractAdminUser.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractAdminUser.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractAdminUser.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.AdminUser_ChangePassword, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

       
        public override bool AdminUser_LogOut(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.AdminUser_LogOut, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override SuccessResult<AbstractAdminUser> AdminUser_LogIn(AbstractAdminUser abstractAdminUser)
        {
            SuccessResult<AbstractAdminUser> AdminUser = null;
            var param = new DynamicParameters();
            
            param.Add("@Username", abstractAdminUser.Username, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractAdminUser.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminUser_LogIn, param, commandType: CommandType.StoredProcedure);
                AdminUser = task.Read<SuccessResult<AbstractAdminUser>>().SingleOrDefault();
                AdminUser.Item = task.Read<AdminUser>().SingleOrDefault();
            }
            return AdminUser;
        }



    }

}
