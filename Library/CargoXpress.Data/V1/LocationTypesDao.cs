﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class LocationTypesDao : AbstractLocationTypesDao
    {

        public override SuccessResult<AbstractLocationTypes> LocationTypes_Upsert(AbstractLocationTypes abstractLocationTypes)
        {
            SuccessResult<AbstractLocationTypes> LocationTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractLocationTypes.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractLocationTypes.Name, dbType: DbType.String, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LocationTypes_Upsert, param, commandType: CommandType.StoredProcedure);
                LocationTypes = task.Read<SuccessResult<AbstractLocationTypes>>().SingleOrDefault();
                LocationTypes.Item = task.Read<LocationTypes>().SingleOrDefault();
            }

            return LocationTypes;
        }

        public override SuccessResult<AbstractLocationTypes> LocationTypes_ById(int Id)
        {
            SuccessResult<AbstractLocationTypes> LocationTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LocationTypes_ById, param, commandType: CommandType.StoredProcedure);
                LocationTypes = task.Read<SuccessResult<AbstractLocationTypes>>().SingleOrDefault();
                LocationTypes.Item = task.Read<LocationTypes>().SingleOrDefault();
            }

            return LocationTypes;
        }

        public override PagedList<AbstractLocationTypes> LocationTypes_All(PageParam pageParam,String Search)
        {
            

            PagedList<AbstractLocationTypes> LocationTypes = new PagedList<AbstractLocationTypes>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.LocationTypes_All, param, commandType: CommandType.StoredProcedure);
                LocationTypes.Values.AddRange(task.Read<LocationTypes>());
                LocationTypes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return LocationTypes;
        }

    }

    }
