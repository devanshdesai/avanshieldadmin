﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobDocumentsDao : AbstractJobDocumentsDao
    {

        public override SuccessResult<AbstractJobDocuments> JobDocuments_Upsert(AbstractJobDocuments abstractJobDocuments)
        {
            SuccessResult<AbstractJobDocuments> JobDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobDocuments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobDocuments.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractJobDocuments.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DocumentTypeId", abstractJobDocuments.DocumentTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Url", abstractJobDocuments.Url, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@CreatedBy", abstractJobDocuments.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@UpdatedBy", abstractJobDocuments.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDocuments_Upsert, param, commandType: CommandType.StoredProcedure);
                JobDocuments = task.Read<SuccessResult<AbstractJobDocuments>>().SingleOrDefault();
                JobDocuments.Item = task.Read<JobDocuments>().SingleOrDefault();
            }

            return JobDocuments;
        }

        public override SuccessResult<AbstractJobDocuments> JobDocuments_ById(int Id)
        {
            SuccessResult<AbstractJobDocuments> JobDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDocuments_ById, param, commandType: CommandType.StoredProcedure);
                JobDocuments = task.Read<SuccessResult<AbstractJobDocuments>>().SingleOrDefault();
                JobDocuments.Item = task.Read<JobDocuments>().SingleOrDefault();
            }

            return JobDocuments;
        }

        public override PagedList<AbstractJobDocuments> JobDocuments_ByJobId(PageParam pageParam,int JobId, int UserType)
        {
            

            PagedList<AbstractJobDocuments> JobDocuments = new PagedList<AbstractJobDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDocuments_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobDocuments.Values.AddRange(task.Read<JobDocuments>());
                JobDocuments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobDocuments;
        }

    }

    }
