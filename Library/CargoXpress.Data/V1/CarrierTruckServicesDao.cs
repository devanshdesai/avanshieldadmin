﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CarrierTruckServicesDao : AbstractCarrierTruckServicesDao
    {
        public override SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices_Upsert(AbstractCarrierTruckServices abstractCarrierTruckServices)
        {
            SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarrierTruckServices.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractCarrierTruckServices.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckServiceId", abstractCarrierTruckServices.TruckServiceId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@SubTruckServiceId", abstractCarrierTruckServices.SubTruckServiceId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCarrierTruckServices.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCarrierTruckServices.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierTruckServices_Upsert, param, commandType: CommandType.StoredProcedure);
                CarrierTruckServices = task.Read<SuccessResult<AbstractCarrierTruckServices>>().SingleOrDefault();
                CarrierTruckServices.Item = task.Read<CarrierTruckServices>().SingleOrDefault();
            }

            return CarrierTruckServices;
        }

        public override SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices_ById(int Id)
        {
            SuccessResult<AbstractCarrierTruckServices> CarrierTruckServices = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierTruckServices_ById, param, commandType: CommandType.StoredProcedure);
                CarrierTruckServices = task.Read<SuccessResult<AbstractCarrierTruckServices>>().SingleOrDefault();
                CarrierTruckServices.Item = task.Read<CarrierTruckServices>().SingleOrDefault();
            }

            return CarrierTruckServices;
        }

        public override PagedList<AbstractCarrierTruckServices> CarrierTruckServices_All(PageParam pageParam, int CarrierId, AbstractCarrierTruckServices abstractCarrierTruckServices = null)
        {
            PagedList<AbstractCarrierTruckServices> CarrierTruckServices = new PagedList<AbstractCarrierTruckServices>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierTruckServices_All, param, commandType: CommandType.StoredProcedure);
                CarrierTruckServices.Values.AddRange(task.Read<CarrierTruckServices>());
                CarrierTruckServices.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CarrierTruckServices;
        }

        public override bool CarrierTruckServices_Delete(int CarrierId)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.CarrierTruckServices_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

    }

    }
