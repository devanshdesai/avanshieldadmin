﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class ShipperAddressesDao : AbstractShipperAddressesDao
    {
        public override SuccessResult<AbstractShipperAddresses> ShipperAddresses_Upsert(AbstractShipperAddresses abstractShipperAddresses)
        {
            SuccessResult<AbstractShipperAddresses> ShipperAddresses = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShipperAddresses.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", abstractShipperAddresses.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@LocationType", abstractShipperAddresses.LocationType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Latitude", abstractShipperAddresses.Latitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longitude", abstractShipperAddresses.Longitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Street", abstractShipperAddresses.Street, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractShipperAddresses.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractShipperAddresses.StateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@City", abstractShipperAddresses.City, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperAddresses_Upsert, param, commandType: CommandType.StoredProcedure);
                ShipperAddresses = task.Read<SuccessResult<AbstractShipperAddresses>>().SingleOrDefault();
                ShipperAddresses.Item = task.Read<ShipperAddresses>().SingleOrDefault();
            }

            return ShipperAddresses;
        }

        public override SuccessResult<AbstractShipperAddresses> ShipperAddresses_ById(int Id)
        {
            SuccessResult<AbstractShipperAddresses> ShipperAddresses = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperAddresses_ById, param, commandType: CommandType.StoredProcedure);
                ShipperAddresses = task.Read<SuccessResult<AbstractShipperAddresses>>().SingleOrDefault();
                ShipperAddresses.Item = task.Read<ShipperAddresses>().SingleOrDefault();
            }

            return ShipperAddresses;
        }

        public override PagedList<AbstractShipperAddresses> ShipperAddresses_All(PageParam pageParam, int ShipperId, AbstractShipperAddresses abstractShipperAddresses = null)
        {
            PagedList<AbstractShipperAddresses> ShipperAddresses = new PagedList<AbstractShipperAddresses>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperAddresses_All, param, commandType: CommandType.StoredProcedure);
                ShipperAddresses.Values.AddRange(task.Read<ShipperAddresses>());
                ShipperAddresses.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShipperAddresses;
            }
    }

    }
