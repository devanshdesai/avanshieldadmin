﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CarrierDocumentsDao : AbstractCarrierDocumentsDao
    {
        public override SuccessResult<AbstractCarrierDocuments> CarrierDocuments_Upsert(AbstractCarrierDocuments abstractCarrierDocuments)
        {
            SuccessResult<AbstractCarrierDocuments> CarrierDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractCarrierDocuments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractCarrierDocuments.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DocumentType", abstractCarrierDocuments.DocumentType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Url", abstractCarrierDocuments.Url, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractCarrierDocuments.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractCarrierDocuments.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierDocuments_Upsert, param, commandType: CommandType.StoredProcedure);
                CarrierDocuments = task.Read<SuccessResult<AbstractCarrierDocuments>>().SingleOrDefault();
                CarrierDocuments.Item = task.Read<CarrierDocuments>().SingleOrDefault();
            }

            return CarrierDocuments;
        }

        public override SuccessResult<AbstractCarrierDocuments> CarrierDocuments_ById(int Id)
        {
            SuccessResult<AbstractCarrierDocuments> CarrierDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierDocuments_ById, param, commandType: CommandType.StoredProcedure);
                CarrierDocuments = task.Read<SuccessResult<AbstractCarrierDocuments>>().SingleOrDefault();
                CarrierDocuments.Item = task.Read<CarrierDocuments>().SingleOrDefault();
            }

            return CarrierDocuments;
        }

        public override PagedList<AbstractCarrierDocuments> CarrierDocuments_All(PageParam pageParam, int CarrierId)
        {
            PagedList<AbstractCarrierDocuments> CarrierDocuments = new PagedList<AbstractCarrierDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CarrierDocuments_All, param, commandType: CommandType.StoredProcedure);
                CarrierDocuments.Values.AddRange(task.Read<CarrierDocuments>());
                CarrierDocuments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return CarrierDocuments;
            }

           

    }

    }
