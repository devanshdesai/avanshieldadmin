﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class TruckPrefferedDestinationsDao : AbstractTruckPrefferedDestinationsDao
    {
        public override SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_Upsert(AbstractTruckPrefferedDestinations abstractTruckPrefferedDestinations)
        {
            SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTruckPrefferedDestinations.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", abstractTruckPrefferedDestinations.TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractTruckPrefferedDestinations.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractTruckPrefferedDestinations.StateId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@City", abstractTruckPrefferedDestinations.City, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractTruckPrefferedDestinations.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTruckPrefferedDestinations.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckPrefferedDestinations_Upsert, param, commandType: CommandType.StoredProcedure);
                TruckPrefferedDestinations = task.Read<SuccessResult<AbstractTruckPrefferedDestinations>>().SingleOrDefault();
                TruckPrefferedDestinations.Item = task.Read<TruckPrefferedDestinations>().SingleOrDefault();
            }

            return TruckPrefferedDestinations;
        }

        public override SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_ById(int Id)
        {
            SuccessResult<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckPrefferedDestinations_ById, param, commandType: CommandType.StoredProcedure);
                TruckPrefferedDestinations = task.Read<SuccessResult<AbstractTruckPrefferedDestinations>>().SingleOrDefault();
                TruckPrefferedDestinations.Item = task.Read<TruckPrefferedDestinations>().SingleOrDefault();
            }

            return TruckPrefferedDestinations;
        }

        public override PagedList<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations_All(PageParam pageParam, int TruckId)
        {
            PagedList<AbstractTruckPrefferedDestinations> TruckPrefferedDestinations = new PagedList<AbstractTruckPrefferedDestinations>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckPrefferedDestinations_All, param, commandType: CommandType.StoredProcedure);
                TruckPrefferedDestinations.Values.AddRange(task.Read<TruckPrefferedDestinations>());
                TruckPrefferedDestinations.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TruckPrefferedDestinations;
            }

           

    }

    }
