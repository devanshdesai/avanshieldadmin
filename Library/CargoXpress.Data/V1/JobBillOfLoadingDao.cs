﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobBillOfLoadingDao : AbstractJobBillOfLoadingDao
    {
        public override SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading_Upsert(AbstractJobBillOfLoading abstractJobBillOfLoading)
        {
            SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobBillOfLoading.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobBillOfLoading.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@BillOfLoading", abstractJobBillOfLoading.BillOfLoading, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FileUrl", abstractJobBillOfLoading.FileUrl, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractJobBillOfLoading.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractJobBillOfLoading.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBillOfLoading_Upsert, param, commandType: CommandType.StoredProcedure);
                JobBillOfLoading = task.Read<SuccessResult<AbstractJobBillOfLoading>>().SingleOrDefault();
                JobBillOfLoading.Item = task.Read<JobBillOfLoading>().SingleOrDefault();
            }

            return JobBillOfLoading;
        }

        

        public override PagedList<AbstractJobBillOfLoading> JobBillOfLoading_All(PageParam pageParam, string search, int JobId, string BillOfLoading)
        {
            PagedList<AbstractJobBillOfLoading> JobBillOfLoading = new PagedList<AbstractJobBillOfLoading>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@BillOfLoading", BillOfLoading, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBillOfLoading_All, param, commandType: CommandType.StoredProcedure);
                JobBillOfLoading.Values.AddRange(task.Read<JobBillOfLoading>());
                JobBillOfLoading.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobBillOfLoading;
            }

            public override SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading_Delete(int Id)
            {
            SuccessResult<AbstractJobBillOfLoading> JobBillOfLoading = null;        
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobBillOfLoading_Delete, param, commandType: CommandType.StoredProcedure);
                JobBillOfLoading = task.Read<SuccessResult<AbstractJobBillOfLoading>>().SingleOrDefault();
                //JobBillOfLoading.Item = task.Read<JobBillOfLoading>().SingleOrDefault();
            }
            return JobBillOfLoading;
            }

        

    }

    }
