﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobDriverLocationDao : AbstractJobDriverLocationDao
    {

        public override SuccessResult<AbstractJobDriverLocation> JobDriverLocation_Upsert(AbstractJobDriverLocation abstractJobDriverLocation)
        {
            SuccessResult<AbstractJobDriverLocation> JobDriverLocation = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobDriverLocation.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobDriverLocation.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractJobDriverLocation.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Latitude", abstractJobDriverLocation.Latitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longitude", abstractJobDriverLocation.Longitude, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CapturedDate", abstractJobDriverLocation.CapturedDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDriverLocation_Upsert, param, commandType: CommandType.StoredProcedure);
                JobDriverLocation = task.Read<SuccessResult<AbstractJobDriverLocation>>().SingleOrDefault();
                JobDriverLocation.Item = task.Read<JobDriverLocation>().SingleOrDefault();
            }

            return JobDriverLocation;
        }

        public override SuccessResult<AbstractJobDriverLocation> JobDriverLocations_ById(int Id)
        {
            SuccessResult<AbstractJobDriverLocation> JobDriverLocation = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDriverLocations_ById, param, commandType: CommandType.StoredProcedure);
                JobDriverLocation = task.Read<SuccessResult<AbstractJobDriverLocation>>().SingleOrDefault();
                JobDriverLocation.Item = task.Read<JobDriverLocation>().SingleOrDefault();
            }

            return JobDriverLocation;
        }

        public override PagedList<AbstractJobDriverLocation> JobDriverLocation_ByJobId(PageParam pageParam, int JobId, int DriverId)
        {
            

            PagedList<AbstractJobDriverLocation> JobDriverLocation = new PagedList<AbstractJobDriverLocation>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDriverLocation_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobDriverLocation.Values.AddRange(task.Read<JobDriverLocation>());
                JobDriverLocation.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobDriverLocation;
        }

    }

    }
