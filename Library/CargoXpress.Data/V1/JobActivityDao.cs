﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobActivityDao : AbstractJobActivityDao
    {
        public override PagedList<AbstractJobActivity> JobActivity_ByJobId(PageParam pageParam, int JobId, int CarrierId)
        {
            

            PagedList<AbstractJobActivity> JobActivity = new PagedList<AbstractJobActivity>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobActivity_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobActivity.Values.AddRange(task.Read<JobActivity>());
                JobActivity.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobActivity;
        }

    }

    }
