﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class CountriesDao : AbstractCountriesDao
    {

        public override PagedList<AbstractCountries> Countries_All(PageParam pageParam,string search)
        {
            PagedList<AbstractCountries> Countries = new PagedList<AbstractCountries>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Countries_All, param, commandType: CommandType.StoredProcedure);
                Countries.Values.AddRange(task.Read<Countries>());
                Countries.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Countries;
        }
    }
}
