﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class ShippersDao : AbstractShippersDao
    {
        public override SuccessResult<AbstractShippers> Shippers_Upsert(AbstractShippers abstractShippers)
        {
            SuccessResult<AbstractShippers> Shippers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShippers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Email", abstractShippers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractShippers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractShippers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractShippers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CompanyName", abstractShippers.CompanyName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TradingLicense", abstractShippers.TradingLicense, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Tin", abstractShippers.Tin, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RegistrantId", abstractShippers.RegistrantId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsCompany", abstractShippers.IsCompany, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractShippers.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractShippers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CommodityGroups", abstractShippers.CommodityGroups, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractShippers.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShippers.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractShippers.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_Upsert, param, commandType: CommandType.StoredProcedure);
                Shippers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Shippers.Item = task.Read<Shippers>().SingleOrDefault();
            }

            return Shippers;
        }

        public override SuccessResult<AbstractShippers> Shippers_ById(int Id)
        {
            SuccessResult<AbstractShippers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_ById, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Carriers.Item = task.Read<Shippers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override PagedList<AbstractShippers> Shippers_All(PageParam pageParam, string search, string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive)
        {
            PagedList<AbstractShippers> Shippers = new PagedList<AbstractShippers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Name", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", IsActive != null ? IsActive.Value : IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_All, param, commandType: CommandType.StoredProcedure);
                Shippers.Values.AddRange(task.Read<Shippers>());
                Shippers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Shippers;
        }

        public override SuccessResult<AbstractShippers> Shippers_Login(AbstractShippers abstractShippers)
        {
            SuccessResult<AbstractShippers> Shippers = null;
            var param = new DynamicParameters();

            param.Add("@PhoneNumber", abstractShippers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractShippers.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Password", abstractShippers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LoginType", abstractShippers.LoginType, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractShippers.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_Login, param, commandType: CommandType.StoredProcedure);
                Shippers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Shippers.Item = task.Read<Shippers>().SingleOrDefault();
            }
            return Shippers;
        }

        public override SuccessResult<AbstractShippers> Shippers_ActInact(int Id)
        {

            SuccessResult<AbstractShippers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_ActInact, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Carriers.Item = task.Read<Shippers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override SuccessResult<AbstractShippers> Shippers_ChangePassword(AbstractShippers abstractShippers)
        {
            SuccessResult<AbstractShippers> Carriers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShippers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractShippers.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractShippers.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractShippers.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractShippers.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Carriers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Carriers.Item = task.Read<Shippers>().SingleOrDefault();
            }

            return Carriers;
        }

        public override SuccessResult<AbstractShippers> Shippers_EmailVerified(int Id)
        {
            SuccessResult<AbstractShippers> Shippers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_EmailVerified, param, commandType: CommandType.StoredProcedure);
                Shippers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Shippers.Item = task.Read<Shippers>().SingleOrDefault();
            }
            return Shippers;
        }

        public override SuccessResult<AbstractShippers> Shippers_MobileVerified(int Id)
        {
            SuccessResult<AbstractShippers> Shippers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_MobileVerified, param, commandType: CommandType.StoredProcedure);
                Shippers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Shippers.Item = task.Read<Shippers>().SingleOrDefault();
            }
            return Shippers;
        }

        public override bool Shippers_Logout(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Shippers_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override SuccessResult<AbstractShippers> Shippers_ProfileUpdate(AbstractShippers abstractShippers)
        {
            SuccessResult<AbstractShippers> Shippers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShippers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FileURL", abstractShippers.FileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractShippers.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Shippers_ProfileUpdate, param, commandType: CommandType.StoredProcedure);
                Shippers = task.Read<SuccessResult<AbstractShippers>>().SingleOrDefault();
                Shippers.Item = task.Read<Shippers>().SingleOrDefault();
            }

            return Shippers;
        }

    }

}
