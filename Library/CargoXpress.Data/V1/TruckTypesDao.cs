﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class TruckTypesDao : AbstractTruckTypesDao
    {

        public override SuccessResult<AbstractTruckTypes> TruckTypes_Upsert(AbstractTruckTypes abstractTruckTypes)
        {
            SuccessResult<AbstractTruckTypes> TruckTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTruckTypes.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractTruckTypes.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MaximumTonage", abstractTruckTypes.MaximumTonage, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckTypes_Upsert, param, commandType: CommandType.StoredProcedure);
                TruckTypes = task.Read<SuccessResult<AbstractTruckTypes>>().SingleOrDefault();
                TruckTypes.Item = task.Read<TruckTypes>().SingleOrDefault();
            }

            return TruckTypes;
        }

        public override SuccessResult<AbstractTruckTypes> TruckTypes_ById(int Id)
        {
            SuccessResult<AbstractTruckTypes> TruckTypes = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckTypes_ById, param, commandType: CommandType.StoredProcedure);
                TruckTypes = task.Read<SuccessResult<AbstractTruckTypes>>().SingleOrDefault();
                TruckTypes.Item = task.Read<TruckTypes>().SingleOrDefault();
            }

            return TruckTypes;
        }

        public override PagedList<AbstractTruckTypes> TruckTypes_All(PageParam pageParam,String Search)
        {
            

            PagedList<AbstractTruckTypes> TruckTypes = new PagedList<AbstractTruckTypes>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckTypes_All, param, commandType: CommandType.StoredProcedure);
                TruckTypes.Values.AddRange(task.Read<TruckTypes>());
                TruckTypes.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TruckTypes;
        }

    }

    }
