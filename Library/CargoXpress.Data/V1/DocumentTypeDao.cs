﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class DocumentTypeDao : AbstractDocumentTypeDao
    {
      
        public override PagedList<AbstractDocumentType> DocumentType_All(PageParam pageParam, String Search)
        {
            

            PagedList<AbstractDocumentType> DocumentType = new PagedList<AbstractDocumentType>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DocumentType_All, param, commandType: CommandType.StoredProcedure);
                DocumentType.Values.AddRange(task.Read<DocumentType>());
                DocumentType.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return DocumentType;
        }

    }

    }
