﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class TruckDocumentsDao : AbstractTruckDocumentsDao
    {
        public override SuccessResult<AbstractTruckDocuments> TruckDocuments_Upsert(AbstractTruckDocuments abstractTruckDocuments)
        {
            SuccessResult<AbstractTruckDocuments> TruckDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractTruckDocuments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", abstractTruckDocuments.TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DocumentType", abstractTruckDocuments.DocumentType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Url", abstractTruckDocuments.Url, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractTruckDocuments.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractTruckDocuments.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckDocuments_Upsert, param, commandType: CommandType.StoredProcedure);
                TruckDocuments = task.Read<SuccessResult<AbstractTruckDocuments>>().SingleOrDefault();
                TruckDocuments.Item = task.Read<TruckDocuments>().SingleOrDefault();
            }

            return TruckDocuments;
        }

        public override SuccessResult<AbstractTruckDocuments> TruckDocuments_ById(int Id)
        {
            SuccessResult<AbstractTruckDocuments> TruckDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckDocuments_ById, param, commandType: CommandType.StoredProcedure);
                TruckDocuments = task.Read<SuccessResult<AbstractTruckDocuments>>().SingleOrDefault();
                TruckDocuments.Item = task.Read<TruckDocuments>().SingleOrDefault();
            }

            return TruckDocuments;
        }

        public override PagedList<AbstractTruckDocuments> TruckDocuments_All(PageParam pageParam, int TruckId)
        {
            PagedList<AbstractTruckDocuments> TruckDocuments = new PagedList<AbstractTruckDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TruckId", TruckId, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.TruckDocuments_All, param, commandType: CommandType.StoredProcedure);
                TruckDocuments.Values.AddRange(task.Read<TruckDocuments>());
                TruckDocuments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return TruckDocuments;
            }
    }

    }
