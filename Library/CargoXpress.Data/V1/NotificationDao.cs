﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class NotificationDao : AbstractNotificationDao
    {
        public override SuccessResult<AbstractNotification> Notification_Upsert(AbstractNotification abstractNotification)
        {
            SuccessResult<AbstractNotification> Notification = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractNotification.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractNotification.UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractNotification.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Text", abstractNotification.Text, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_Upsert, param, commandType: CommandType.StoredProcedure);
                Notification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                Notification.Item = task.Read<Notification>().SingleOrDefault();
            }

            return Notification;
        }

        

        public override PagedList<AbstractNotification> Notification_All(PageParam pageParam, string search, int UserId, int UserType)
        {
            PagedList<AbstractNotification> Notification = new PagedList<AbstractNotification>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Notification_All, param, commandType: CommandType.StoredProcedure);
                Notification.Values.AddRange(task.Read<Notification>());
                Notification.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Notification;
            }

            
    }

    }
