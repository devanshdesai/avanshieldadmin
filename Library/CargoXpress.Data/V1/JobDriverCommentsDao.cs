﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobDriverCommentsDao : AbstractJobDriverCommentsDao
    {
        public override SuccessResult<AbstractJobDriverComments> JobDriverComments_Upsert(AbstractJobDriverComments abstractJobDriverComments)
        {
            SuccessResult<AbstractJobDriverComments> JobDriverComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobDriverComments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobDriverComments.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserType", abstractJobDriverComments.UserType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractJobDriverComments.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Comment", abstractJobDriverComments.Comment, dbType: DbType.String, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDriverComments_Upsert, param, commandType: CommandType.StoredProcedure);
                JobDriverComments = task.Read<SuccessResult<AbstractJobDriverComments>>().SingleOrDefault();
                JobDriverComments.Item = task.Read<JobDriverComments>().SingleOrDefault();
            }

            return JobDriverComments;
        }

        public override SuccessResult<AbstractJobDriverComments> JobDriverComments_ById(int Id)
        {
            SuccessResult<AbstractJobDriverComments> JobDriverComments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDriverComments_ById, param, commandType: CommandType.StoredProcedure);
                JobDriverComments = task.Read<SuccessResult<AbstractJobDriverComments>>().SingleOrDefault();
                JobDriverComments.Item = task.Read<JobDriverComments>().SingleOrDefault();
            }

            return JobDriverComments;
        }

        public override PagedList<AbstractJobDriverComments> JobDriverComments_All(PageParam pageParam, int JobId, int DriverId)
        {
            PagedList<AbstractJobDriverComments> JobDriverComments = new PagedList<AbstractJobDriverComments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDriverComments_All, param, commandType: CommandType.StoredProcedure);
                JobDriverComments.Values.AddRange(task.Read<JobDriverComments>());
                JobDriverComments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobDriverComments;
            }

           

    }

    }
