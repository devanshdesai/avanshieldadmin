﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class DriversDao : AbstractDriversDao
    {
        public override SuccessResult<AbstractDrivers> Drivers_Upsert(AbstractDrivers abstractDrivers)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDrivers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Email", abstractDrivers.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractDrivers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@FirstName", abstractDrivers.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractDrivers.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@EmployeerName", abstractDrivers.EmployeerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DriverLicense", abstractDrivers.DriverLicense, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DriverID", abstractDrivers.DriverID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractDrivers.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CarrierId", abstractDrivers.CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractDrivers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractDrivers.CreatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractDrivers.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_Upsert, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }

            return Drivers;
        }

        public override SuccessResult<AbstractDrivers> Drivers_ById(int Id)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_ById, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }

            return Drivers;
        }
        //#string Name, string Email, int CountryId, string PhoneNumber, bool? IsActive, string DriverLicense, string DriverID, 
        public override PagedList<AbstractDrivers> Drivers_All(PageParam pageParam, string search, int CarrierId)
        {
            PagedList<AbstractDrivers> Drivers = new PagedList<AbstractDrivers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@Name", Name, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@CountryId", CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            //param.Add("@PhoneNumber", PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@IsActive", IsActive != null ? IsActive.Value : IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            //param.Add("@DriverLicense", DriverLicense, dbType: DbType.String, direction: ParameterDirection.Input);
            //param.Add("@DriverID", DriverID, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CarrierId", CarrierId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_All, param, commandType: CommandType.StoredProcedure);
                Drivers.Values.AddRange(task.Read<Drivers>());
                Drivers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Drivers;
        }

        public override SuccessResult<AbstractDrivers> Drivers_Login(AbstractDrivers abstractDrivers)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@CountryId", abstractDrivers.CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@PhoneNumber", abstractDrivers.PhoneNumber, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractDrivers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@LoginType", abstractDrivers.LoginType, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractDrivers.DeviceToken, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_Login, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }
            return Drivers;
        }

        public override SuccessResult<AbstractDrivers> Drivers_ActInact(int Id)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_ActInact, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }
            return Drivers;
        }

        public override SuccessResult<AbstractDrivers> Drivers_ChangePassword(AbstractDrivers abstractDrivers)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDrivers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractDrivers.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractDrivers.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractDrivers.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Type", abstractDrivers.Type, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_ChangePassword, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }

            return Drivers;
        }

        public override SuccessResult<AbstractDrivers> Drivers_EmailVerified(int Id)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_EmailVerified, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }
            return Drivers;
        }

        public override SuccessResult<AbstractDrivers> Drivers_MobileVerify(int Id)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_MobileVerify, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }
            return Drivers;
        }
        public override bool Drivers_Logout(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Drivers_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override SuccessResult<AbstractDrivers> Drivers_ProfileUpdate(AbstractDrivers abstractDrivers)
        {
            SuccessResult<AbstractDrivers> Drivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractDrivers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@FileURL", abstractDrivers.FileURL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractDrivers.UpdatedBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Drivers_ProfileUpdate, param, commandType: CommandType.StoredProcedure);
                Drivers = task.Read<SuccessResult<AbstractDrivers>>().SingleOrDefault();
                Drivers.Item = task.Read<Drivers>().SingleOrDefault();
            }

            return Drivers;
        }

    }

}
