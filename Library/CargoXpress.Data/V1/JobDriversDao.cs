﻿using Dapper;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
//using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class JobDriversDao : AbstractJobDriversDao
    {


        public override SuccessResult<AbstractJobDrivers> JobDrivers_Upsert(AbstractJobDrivers abstractJobDrivers)
        {
            SuccessResult<AbstractJobDrivers> JobDrivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractJobDrivers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", abstractJobDrivers.JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DriverId", abstractJobDrivers.DriverId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDrivers_Upsert, param, commandType: CommandType.StoredProcedure);
                JobDrivers = task.Read<SuccessResult<AbstractJobDrivers>>().SingleOrDefault();
                JobDrivers.Item = task.Read<JobDrivers>().SingleOrDefault();
            }

            return JobDrivers;
        }

        public override PagedList<AbstractJobDrivers> JobDrivers_ByJobId(PageParam pageParam, int JobId)
        {
            PagedList<AbstractJobDrivers> JobDrivers = new PagedList<AbstractJobDrivers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@JobId", JobId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDrivers_ByJobId, param, commandType: CommandType.StoredProcedure);
                JobDrivers.Values.AddRange(task.Read<JobDrivers>());
                JobDrivers.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return JobDrivers;
        }

        public override SuccessResult<AbstractJobDrivers> JobDrivers_ById(int Id)
        {
            SuccessResult<AbstractJobDrivers> JobDrivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDrivers_ById, param, commandType: CommandType.StoredProcedure);
                JobDrivers = task.Read<SuccessResult<AbstractJobDrivers>>().SingleOrDefault();
                JobDrivers.Item = task.Read<JobDrivers>().SingleOrDefault();
            }

            return JobDrivers;
        }

        public override bool JobDrivers_Delete(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@IdId", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.JobDrivers_Delete, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override SuccessResult<AbstractJobDrivers> JobDrivers_IsJobCompleted(int Id)
        {
            SuccessResult<AbstractJobDrivers> JobDrivers = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.JobDrivers_IsJobCompleted, param, commandType: CommandType.StoredProcedure);
                JobDrivers = task.Read<SuccessResult<AbstractJobDrivers>>().SingleOrDefault();
                JobDrivers.Item = task.Read<JobDrivers>().SingleOrDefault();
            }

            return JobDrivers;
        }




    }

}
