﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class SupportReplyDao : AbstractSupportReplyDao
    {
        public override SuccessResult<AbstractSupportReply> SupportReply_Upsert(AbstractSupportReply abstractSupport)
        {
            SuccessResult<AbstractSupportReply> Support = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractSupport.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@SupportId", abstractSupport.SupportId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ReplyText ", abstractSupport.ReplyText, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@UserTypeId", abstractSupport.UserTypeId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SupportReply_Upsert, param, commandType: CommandType.StoredProcedure);
                Support = task.Read<SuccessResult<AbstractSupportReply>>().SingleOrDefault();
                Support.Item = task.Read<SupportReply>().SingleOrDefault();
            }
            return Support;
        }

        public override PagedList<AbstractSupportReply> SupportReply_BySupportId(PageParam pageParam, int SupportId)
        {
            PagedList<AbstractSupportReply> Support = new PagedList<AbstractSupportReply>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@SupportId", SupportId, dbType: DbType.Int32, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SupportReply_BySupportId, param, commandType: CommandType.StoredProcedure);
                Support.Values.AddRange(task.Read<SupportReply>());
                Support.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Support;
            }

    }
}
