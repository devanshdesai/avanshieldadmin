﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class ShiperDocumentsDao : AbstractShiperDocumentsDao
    {
        public override SuccessResult<AbstractShiperDocuments> ShipperDocuments_Upsert(AbstractShiperDocuments abstractShiperDocuments)
        {
            SuccessResult<AbstractShiperDocuments> ShiperDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractShiperDocuments.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", abstractShiperDocuments.ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DocumentType", abstractShiperDocuments.DocumentType, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Url", abstractShiperDocuments.Url, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShipperDocuments_Upsert, param, commandType: CommandType.StoredProcedure);
                ShiperDocuments = task.Read<SuccessResult<AbstractShiperDocuments>>().SingleOrDefault();
                ShiperDocuments.Item = task.Read<ShiperDocuments>().SingleOrDefault();
            }

            return ShiperDocuments;
        }

        public override SuccessResult<AbstractShiperDocuments> ShiperDocuments_ById(int Id)
        {
            SuccessResult<AbstractShiperDocuments> ShiperDocuments = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShiperDocuments_ById, param, commandType: CommandType.StoredProcedure);
                ShiperDocuments = task.Read<SuccessResult<AbstractShiperDocuments>>().SingleOrDefault();
                ShiperDocuments.Item = task.Read<ShiperDocuments>().SingleOrDefault();
            }

            return ShiperDocuments;
        }

        public override PagedList<AbstractShiperDocuments> ShiperDocuments_All(PageParam pageParam, int ShipperId, AbstractShiperDocuments abstractShiperDocuments = null)
        {
            PagedList<AbstractShiperDocuments> ShiperDocuments = new PagedList<AbstractShiperDocuments>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@ShipperId", ShipperId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.ShiperDocuments_All, param, commandType: CommandType.StoredProcedure);
                ShiperDocuments.Values.AddRange(task.Read<ShiperDocuments>());
                ShiperDocuments.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return ShiperDocuments;
        }
    }

}
