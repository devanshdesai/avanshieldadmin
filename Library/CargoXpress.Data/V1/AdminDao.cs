﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class AdminDao : AbstractAdminDao
    {
        public override SuccessResult<AbstractAdmin> Admin_ById(int Id)
        {
            SuccessResult<AbstractAdmin> Admin = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_ById, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }

            return Admin;
        }

        public override SuccessResult<AbstractAdmin> Admin_Login(AbstractAdmin abstractAdmin)
            {
            SuccessResult<AbstractAdmin> Admin = null;        
            var param = new DynamicParameters();

            param.Add("@Email", abstractAdmin.Email, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractAdmin.Password, dbType: DbType.String, direction: ParameterDirection.Input);
           
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Admin_Login, param, commandType: CommandType.StoredProcedure);
                Admin = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                Admin.Item = task.Read<Admin>().SingleOrDefault();
            }
            return Admin;
            }

        public override bool Admin_ChangePassword(AbstractAdmin abstractAdmin)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", abstractAdmin.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@OldPassword", abstractAdmin.OldPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NewPassword", abstractAdmin.NewPassword, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ConfirmPassword", abstractAdmin.ConfirmPassword, dbType: DbType.String, direction: ParameterDirection.Input);
          
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Admin_ChangePassword, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

        public override bool Admin_Logout(int Id)
        {
            bool result = false;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.Admin_Logout, param, commandType: CommandType.StoredProcedure);
                result = task.SingleOrDefault<bool>();
            }

            return result;
        }

      

    }

    }
