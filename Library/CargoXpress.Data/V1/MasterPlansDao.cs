﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoXpress.Common;
using CargoXpress.Common.Paging;
using CargoXpress.Data.Contract;
using CargoXpress.Entities.Contract;
using CargoXpress.Entities.V1;
//using CargoXpress.Entities.V1;

namespace CargoXpress.Data.V1
{
    public class MasterPlansDao : AbstractMasterPlansDao
    {

        public override SuccessResult<AbstractMasterPlans> MasterPlans_Upsert(AbstractMasterPlans abstractMasterPlans)
        {
            SuccessResult<AbstractMasterPlans> MasterPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractMasterPlans.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Name", abstractMasterPlans.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Description", abstractMasterPlans.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Amount", abstractMasterPlans.Amount, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@NoOfMonths", abstractMasterPlans.NoOfMonths, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPlans_Upsert, param, commandType: CommandType.StoredProcedure);
                MasterPlans = task.Read<SuccessResult<AbstractMasterPlans>>().SingleOrDefault();
                MasterPlans.Item = task.Read<MasterPlans>().SingleOrDefault();
            }

            return MasterPlans;
        }

        public override SuccessResult<AbstractMasterPlans> MasterPlans_ById(int Id)
        {
            SuccessResult<AbstractMasterPlans> MasterPlans = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPlans_ById, param, commandType: CommandType.StoredProcedure);
                MasterPlans = task.Read<SuccessResult<AbstractMasterPlans>>().SingleOrDefault();
                MasterPlans.Item = task.Read<MasterPlans>().SingleOrDefault();
            }

            return MasterPlans;
        }
        public override PagedList<AbstractMasterPlans> MasterPlans_All(PageParam pageParam, string Search)
        {
            PagedList<AbstractMasterPlans> MasterPlans = new PagedList<AbstractMasterPlans>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Search", Search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterPlans_All, param, commandType: CommandType.StoredProcedure);
                MasterPlans.Values.AddRange(task.Read<MasterPlans>());
                MasterPlans.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return MasterPlans;
        }


    }

}
