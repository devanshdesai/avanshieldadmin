﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Rushkar">
//     Copyright Rushkar Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CargoXpress.Data
{
    using Autofac;
    using CargoXpress.Data.Contract;
    using CargoXpress.Data.V1;

    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
           
            builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            builder.RegisterType<V1.AdminUserDao>().As<AbstractAdminUserDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomersDao>().As<AbstractCustomersDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterPlansDao>().As<AbstractMasterPlansDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomerTransactionsDao>().As<AbstractCustomerTransactionsDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomerPlansDao>().As<AbstractCustomerPlansDao>().InstancePerDependency();
            builder.RegisterType<V1.CustomerActivityDao>().As<AbstractCustomerActivityDao>().InstancePerDependency();
            
        }
    }
}
