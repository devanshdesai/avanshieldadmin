﻿using System.Collections.Generic;

namespace CargoXpress.Common.Paging
{
    /// <summary>
    /// Paged list interface
    /// </summary>
    public interface IPagedList<T> : IList<T>
    {
        
    }
}
