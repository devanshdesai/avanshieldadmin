﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractCustomerTransactions
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string PlanDescription { get; set; }
        public int PlanNoOfMonths { get; set; }
        public decimal PlanAmount { get; set; }
        public int NoOfMonths { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LicenceKey { get; set; }

        [NotMapped]
        public string StartDateStr => StartDate != null ? StartDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string EndDateStr => EndDate != null ? EndDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

    }
}
