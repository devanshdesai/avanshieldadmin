﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractDrivers
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsMobileVerified { get; set; }
        public bool IsResetPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FileURL { get; set; }
        public string EmployeerName { get; set; }
        public string DriverLicense { get; set; }
        public string DriverID { get; set; }
        public int CountryId { get; set; }
        public int CarrierId { get; set; }
        public string DeviceToken { get; set; }
        public string PhoneNumber { get; set; }
        public string LastLogin { get; set; }
        public string Otp { get; set; }
        public int CountryCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public bool LoginType { get; set; }
        public int Type { get; set; }
        public string FullName { get; set; }
        public bool IsAvailable { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string LastLoginStr => LastLogin != null ? Convert.ToDateTime(LastLogin).ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string PhoneNumberStr => PhoneNumber != null ? "+" + CountryCode + " " + PhoneNumber : "-";
        [NotMapped]
        public string FileUrlStr => Configurations.BaseUrl + FileURL;

    }
}
