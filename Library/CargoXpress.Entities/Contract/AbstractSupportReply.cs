﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractSupportReply
    {
        public int Id { get; set; }
        public int SupportId { get; set; }
        public string ReplyText { get; set; }
        public int UserTypeId { get; set; }
        public DateTime ReplyDate { get; set; }
        
        [NotMapped]
        public string ReplyDateStr => ReplyDate != null ? ReplyDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
    }
}
