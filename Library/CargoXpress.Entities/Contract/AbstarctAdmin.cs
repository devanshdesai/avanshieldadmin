﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractAdmin
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AdminRole { get; set; }
        public string LastLogin { get; set; }
        public string OldPassword { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("MM/dd/yyyy") : "-";

    }
}
