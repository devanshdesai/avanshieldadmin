﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractJobActivity
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int UserType { get; set; }
        public int UserId { get; set; }
        public int CleanerId { get; set; }
        public int PreviousJobStatusId { get; set; }
        public int CurrentJobStatusId { get; set; }
        public string Note { get; set; }
        public string SystemNote { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        public string PreviousJobStatus { get; set; }
        public string CurrentJobStatus { get; set; }

    }
}
