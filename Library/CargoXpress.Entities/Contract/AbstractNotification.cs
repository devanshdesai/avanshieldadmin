﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractNotification
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int UserType { get; set; }
        public string Text { get; set; }
        public string UserTypeName { get; set; }

        public DateTime CreatedDate { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("mm/dd/yyyy") : "-";

    }
}
