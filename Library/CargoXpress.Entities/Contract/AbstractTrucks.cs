﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractTrucks
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsJobActive { get; set; }
        public string PlateNumber { get; set; }
        public int CarrierId { get; set; }
        public int TruckTypeId { get; set; }
        public string Logbook { get; set; }
        public string CompanyName { get; set; }
        public string Insurance { get; set; }
        public string RfidTag { get; set; }
        public decimal LoadCapacity { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public string TruckTypeName { get; set; }
        public bool IsAvailable { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

    }
}
