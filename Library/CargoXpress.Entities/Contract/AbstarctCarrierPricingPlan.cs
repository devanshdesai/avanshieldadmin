﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractCarrierPricingPlan
    {
        public int Id { get; set; }
        public int CarrierId { get; set; }
        public int MasterPricingPlanId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string PlanName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal TimeLimit { get; set; }
        public decimal AdvPayPerc { get; set; }
        public int TotalNoOfInterServices { get; set; }
        public decimal AdditionalServicePrice { get; set; }
        public bool IsActive { get; set; }
        public int UserType { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";        
        

    }
}
