﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractMasterJobStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserType { get; set; }
        public int JobStatusTypeId { get; set; }
               
    }
}
