﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractCustomerPlans
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int PlanId { get; set; }
        public string LiscenceKey { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RedeemedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PlanName { get; set; }
        public string Amount { get; set; }
        public string NoOfMonths { get; set; }
        public bool IsPaymentDone { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionNumber { get; set; }
        public string Note { get; set; }
        public bool IsRedeemed {get;set;}
        public bool IsExpired { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string EndDateStr => EndDate != null ? EndDate.ToString("dd-MM-yyyy") : "-";
        [NotMapped]
        public string StartDateStr => StartDate != null ? StartDate.ToString("dd-MM-yyyy") : "-";
        [NotMapped]
        public string RedeemedDateStr => RedeemedDate != null ? RedeemedDate.ToString("dd-MM-yyyy hh:mm tt") : "-";
        [NotMapped]
        public string TransactionDateStr => TransactionDate != null ? TransactionDate.ToString("dd-MM-yyyy hh:mm tt") : "-";

    }
}
