﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractShipperAddresses
    {
        public int Id { get; set; }
        public int ShipperId { get; set; }
        public int LocationType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Street { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        [NotMapped]
        public string FullAddress
        {
            get
            {
                string fullAddress = "";
                if (!string.IsNullOrEmpty(Street))
                    fullAddress += Street;
                if (!string.IsNullOrEmpty(City))
                    fullAddress += ", <br/> " + City;
                if (!string.IsNullOrEmpty(State))
                    fullAddress += ", " + State;
                if (!string.IsNullOrEmpty(Country))
                    fullAddress += ", " + Country;

                return fullAddress;
            }
        }
        /*[NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("MM/dd/yyyy") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedDate != null ? UpdatedDate.ToString("MM/dd/yyyy") : "-";*/

    }
}
