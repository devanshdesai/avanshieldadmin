﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractTruckTypes
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MaximumTonage { get; set; }
        public string Search { get; set; }

    }
}
