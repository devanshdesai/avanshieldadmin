﻿using CargoXpress.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoXpress.Entities.Contract
{
    public abstract class AbstractSupport
    {
        public int Id { get; set; }
        public int CarrierId { get; set; }
        public int ShipperId { get; set; }
        public int JobId { get; set; }
        public int StatusId { get; set; }
        public string SupportNo { get; set; }
        public string Issue { get; set; }
        public string CarrierName { get; set; }
        public string CarrierEmail { get; set; }
        public string CarrierPhoneNumber { get; set; }
        public string CarrierProfile { get; set; }
        public string ShipperName { get; set; }
        public string ShipperEmail { get; set; }
        public string ShipperPhoneNumber { get; set; }
        public string ShipperProfile { get; set; }
        public string JobNumber { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime CloseDate { get; set; }
        public int JobStatusId { get; set; }
        public int UserTypeId { get; set; }
        public string UserType { get; set; }


        [NotMapped]
        public string CreatedDateStr => CreatedDate != null ? CreatedDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

        [NotMapped]
        public string CloseDateStr => CloseDate != null ? CloseDate.ToString("dd-MMM-yyyy hh:mm tt") : "-";

    }
}
